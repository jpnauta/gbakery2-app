FROM python:3.7-slim

ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    # General
    apt-get install -y wget curl && \
    # OpenSSL
    apt-get install -y openssl && \
    # PostgreSQL
    apt-get install -y libpq-dev && \
    # bash
    apt-get install -y bash && \
    # Gevent
    apt-get install -y libevent-dev && \
    # Pillow
    apt-get install -y gcc musl-dev python-dev libffi-dev python3-pil && \
    # Git
    apt-get install -y git && \
    # Debugging
    apt-get install -y nano htop postgresql-client && \
    # lxml
    apt-get install -y libxslt-dev libxml2-dev && \
    # pyyaml
    apt-get install -y libyaml-dev && \
    # m2crypto
    apt-get install -y swig libssl-dev

# Install google cloud SDK
RUN curl https://sdk.cloud.google.com | bash

ENV LIBRARY_PATH /lib:/usr/lib:$LIBRARY_PATH  # Pillow

RUN mkdir /code/
WORKDIR /code/

# Install pip requirements
RUN pip install --upgrade pip
ADD requirements.txt /code/
RUN pip install -r requirements.txt

# Add files
ADD apps/ ./apps/
ADD manage.py .
ADD settings.py .
ADD urls.py .
ADD testing/ ./testing/
ADD asgi.py .
