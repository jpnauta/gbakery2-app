# gbakery2-app

[![CircleCI](https://circleci.com/bb/jpnauta/gbakery2-app.svg?style=svg&circle-token=ab5b320e8278dd2dfff77a1dadc902ef1a7f0361)](https://circleci.com/bb/jpnauta/gbakery2-app)

*Application tier for Glamorgan Bakery system.*

## Getting Started

### Pre-requisites
This project uses [docker-compose](https://docs.docker.com/compose/) for development. 
Please set up your machine to to run the `docker-compose` command before proceeding.

### Configuration

You will need to set up your environment file. To do this, simply copy the default
constants file into your directory folder with this command:

```
cp TEMPLATE.env .env
```

### Data Storage

You will also need to create the volumes where your data will be stored.

```
docker volume create --name=gbakery2_postgres
docker volume create --name=gbakery2_storage
```

You can delete these volumes with these commands

```
docker volume rm gbakery2_postgres
docker volume rm gbakery2_storage
```

You may want to restore a copy of some existing data so you don't need to start 
from scratch. 


### Start Server
You can now start your server for the first time! To do this run this command:

```
make start
```

*Note: It will take several minutes to build your environment for the first time* 

## Development

### Running
To start developing this project, simply start the project with the `start` command.

```
make start
```

As desired, run the `shell` in a separate window to run a bash shell on the web server.

```
make shell
```

## Knowledge Base

All passwords, keys, account details, etc. are stored on Glamorgan Bakery's 
[Slack account](https://glamorganbakery.slack.com/).


## Continuous Integration

To simplify deployment, continuous integration has been configured using [Circle CI][circleci]. Any
changes pushed to `master` branch will be automatically deployed to the live server.

[circleci]: https://circleci.com/bb/jpnauta/gbakery2-app

## Data Management

This project has four commands for managing database snapshots.

- `db_download`: Downloads the latest snapshot from S3 into your local machine
- `db_dump`: Creates a snapshot from the current database
- `db_restore`: Restores the snapshot on your local machine
- `db_upload`: Uploads the snapshot on your local machine to S3

Before you can run these commands, you must configure your gcloud service account by creating `credentials.json`.

## Testing with Stripe Webhooks

If you want to various actions on Stripe (e.g. refund, void), you can enable Stripe's 
[webhook proxy](https://stripe.com/docs/cli/listen) by running the `stripe_listen` command

```
make stripe_listen
```
