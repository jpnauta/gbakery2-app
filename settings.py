# Global settings for gbakery2 project.
import datetime
import logging
import os
import sys
from urllib.parse import urlparse

import environ
from celery import Celery
from celery.schedules import crontab
from google.oauth2 import service_account

PROJECT_DIR = os.path.dirname(__file__)
BLANK = object()
IS_SHELL = len(sys.argv) >= 2 and sys.argv[1] == "shell"

# Import project environment
env = environ.Env()
ENV_FILE_PATH = env.str(
    "ENV_FILE_PATH", os.path.join(PROJECT_DIR, ".env")
)  # Path to .env file
environ.Env.read_env(ENV_FILE_PATH)  # read .env file

# Basic settings
SITE_URL = env.str("SITE_URL")
MEDIA_URL = env.str("MEDIA_URL")
STATIC_URL = env.str("STATIC_URL")
SECRET_KEY = env.str("SECRET_KEY")
STORAGE_TYPE = env.str("STORAGE_TYPE")
DASH_SITE_URL = env.str("DASH_SITE_URL")

# Optional settings
SITE_ID = env.int("SITE_ID", 1)
TEST_MODE = env.bool("TEST_MODE", sys.argv[1:2] == ["test"])
DEBUG = env.bool("DEBUG", False)
ALLOW_CELERY_BYPASS = env.bool("ALLOW_CELERY_BYPASS", False)
SITE_PROTOCOL = env.str("SITE_PROTOCOL", "http://")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", str, ["*"])
SEND_ACTUAL_EMAILS = env.bool("SEND_ACTUAL_EMAILS", False)
PRINT_ALL_ERRORS = env.bool("PRINT_ALL_ERRORS", False)

ADMINS = (("Jeremy Nauta", "jeremypnauta@gmail.com"),)
MANAGERS = ADMINS

TIME_ZONE = "US/Mountain"  # No need to worry about other timezones
LANGUAGE_CODE = "en-us"
USE_I18N = True
USE_L10N = True
USE_TZ = True
ROOT_URLCONF = "urls"
FULL_SITE_URL = SITE_PROTOCOL + SITE_URL
SCHEDULE_FOLDER_URL = (
    "https://drive.google.com/folderview?id=0BxIS5b966gn7YnJNTmF2VGJJcGM&usp=sharing"
)
FULL_DASH_SITE_URL = SITE_PROTOCOL + DASH_SITE_URL

# Paths
sys.path.insert(0, os.path.join(PROJECT_DIR, "apps"))

SESSION_SERIALIZER = "django.contrib.sessions.serializers.PickleSerializer"

# User uploaded files
ADMIN_MEDIA_PREFIX = "/static/admin/"
FILE_UPLOAD_PERMISSIONS = 774

# Static
STATIC_ROOT = os.path.join(PROJECT_DIR, "static/dist")
FRONTEND_ROOT = os.path.join(PROJECT_DIR, "frontend")
STATICFILES_DIRS = ()
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

MIDDLEWARE = (
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    # 'django.contrib.sessions.middleware.SessionMiddleware',
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
)

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            # insert your TEMPLATE_DIRS here
            os.path.join(PROJECT_DIR, "apps/main/templates/"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "main.context_processors.load_general_constants",
            ],
        },
    },
]


AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)

BASE_APPS = (
    "django.contrib.contenttypes",
    "django.contrib.admin",
    "django.contrib.admindocs",
    "django.contrib.auth",
    # 'django.contrib.sessions',
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    # core user application included before the rest for authentication and FK purposes
    "users",
)

SUPPORT_APPS = (
    "allauth",
    "allauth.account",
    "taggit",
    "annoying",
    "celery",
    "django_celery_beat",
    "mathfilters",
    "reversion",
    "multiselectfield",
    "taggit_autosuggest",
    "stdimage",
    "bootstrapform",
    "storages",
    "rest_framework",
    "rest_framework.authtoken",
    "corsheaders",
    "channels",
    "django_extensions",
    "dj_rest_auth",
    "rest_framework_filters",
    "django_filters",
    "tinymce",
    "import_export",
)

CORE_APPS = (
    "images",
    "main",
    "orders",
    "products",
    "cakes",
    "reports",
    "schedule",
)

TEST_APPS = ()

INSTALLED_APPS = BASE_APPS + SUPPORT_APPS + CORE_APPS

# Database
for k in ["DATABASE_HOST", "DATABASE_NAME", "DATABASE_USER", "DATABASE_PASSWORD"]:
    if env.str(k, None) is not None:
        raise RuntimeError("Deprecated constant %s; use DATABASE_URL instead" % k)
DATABASES = {
    "default": env.db_url("DATABASE_URL"),
}
DEBUG_DATABASE_PATH = os.path.join(PROJECT_DIR, "conf/fixtures/debug_data.json")

# Caches
CACHES = {
    "default": {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": env.str("CACHE_LOCATION_URL"),
    }
}

# Allauth
AUTH_USER_MODEL = "users.User"
LOGIN_REDIRECT_URL = "/dashboard/"  # TODO set
ACCOUNT_LOGOUT_ON_GET = True
SOCIALACCOUNT_QUERY_EMAIL = True
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_USER_MODEL_EMAIL_FIELD = "email"
ACCOUNT_EMAIL_VERIFICATION = "none"  # Don't send verification email
ACCOUNT_EMAIL_SUBJECT_PREFIX = "Glamorgan Bakery - "  # TODO use
ACCOUNT_CONFIRM_EMAIL_ON_GET = True
SOCIAL_LOGIN_ENABLED = False

# Emails
DEFAULT_FROM_EMAIL = "noreply@glamorganbakery.com"
SUPPORT_EMAIL = "glamorganbakery@gmail.com"
ORDER_CONFIRMATION_DASHBOARD_SENDGRID_ID = "d-f0a6bbbc33654b9bbe9657961029262d"
if SEND_ACTUAL_EMAILS:
    EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"
    SENDGRID_API_KEY = env.str("SENDGRID_API_KEY")
else:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

# Django REST Framework
REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
    ),
    "DEFAULT_PARSER_CLASSES": ("rest_framework.parsers.JSONParser",),
    "DEFAULT_RENDERER_CLASSES": (
        "rest_framework.renderers.JSONRenderer",
        "rest_framework.renderers.BrowsableAPIRenderer",
    ),
    "DEFAULT_FILTER_BACKENDS": (
        "rest_framework.filters.SearchFilter",
        "rest_framework_filters.backends.RestFrameworkFilterBackend",
    ),
    "SEARCH_PARAM": "q",
    "DEFAULT_PAGINATION_CLASS": "main.serializers.pagination.AppPageNumberPagination",
    "PAGE_SIZE": 20,
    "EXCEPTION_HANDLER": "main.api.views.utils.api_exception_handler",
}

# DRF auth
REST_USE_JWT = True  # Use JWT for django-rest-auth
REST_AUTH_SERIALIZERS = {
    "USER_DETAILS_SERIALIZER": "users.api.auth.serializers.AppUserDetailsSerializer",
}
SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": datetime.timedelta(days=30),
    "REFRESH_TOKEN_LIFETIME": datetime.timedelta(days=300),
}

# Confirmation middleware
CONFIRMATION_COOLDOWN_MINUTES = (
    30  # If failure occurs, x minutes are applied before next failure sent
)
CONFIRMATION_ERROR_EMAIL_CONTACTS = ["jeremypnauta@gmail.com"]
CONFIRMATION_TASK_FREQ_MINUTES = env.int(
    "CONFIRMATION_TASK_FREQ_MINUTES", 2
)  # Performs checks every x minute

# Celery confirmation middleware
CELERY_CONFIRMATION_CACHE_KEY = "celery_confirmation"
CELERY_CONFIRMATION_ENABLED = env.bool("CELERY_CONFIRMATION_ENABLED", False)
CELERY_CONFIRMATION_CHECK_FREQ_MINUTES = env.int(
    "CELERY_CONFIRMATION_CHECK_FREQ_MINUTES", 4
)  # Should be > above

# Enable celery confirmation, if specified
if CELERY_CONFIRMATION_ENABLED:
    MIDDLEWARE += ("main.middleware.CeleryTaskConfirmation",)

# Celery
CELERY_BROKER_TRANSPORT = "redis"
CELERY_RESULT_BACKEND = "redis"
if env.str("REDIS_BROKER_URL", "") is None:
    raise RuntimeError("REDIS_BROKER_URL is deprecated - use CELERY_BROKER_* instead")

if env.get_value("BROKER_URL", default=None):
    raise RuntimeError("Renamed to CELERY_BROKER_URL")
CELERY_BROKER_URL = env.str("CELERY_BROKER_URL")
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers.DatabaseScheduler"
CELERY_BEAT_BROKER_URL = env.str("CELERY_BEAT_BROKER_URL", CELERY_BROKER_URL)
# This shouldn't be necessary, but appears to be
# https://github.com/celery/django-celery/issues/143
CELERY_REDIS_HOST = urlparse(CELERY_BROKER_URL).hostname

# Logging
LOGGING_HANDLERS = ["console"]
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s"
        },
        "simple": {"format": "%(levelname)s %(message)s"},
        "clear": {"format": "%(message)s",},
    },
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse",},
        "require_debug_true": {"()": "django.utils.log.RequireDebugTrue",},
        "require_sentry_enabled": {"()": "main.logging.filters.RequireSentryEnabled",},
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "clear",
            "filters": ["require_debug_true",],
        },
        # 'sentry': {
        #     'level': 'WARNING',
        #     'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        #     'filters': ['require_sentry_enabled', ],
        # },
    },
    "loggers": {
        "django.request": {
            "handlers": LOGGING_HANDLERS,
            "level": "ERROR",
            "propagate": True,
        },
        "log_to_console": {"level": "DEBUG", "handlers": ["console",],},
        "app": {"handlers": LOGGING_HANDLERS, "level": "DEBUG", "propagate": True,},
        # 'log_to_sentry': {
        #     'level': 'WARNING',
        #     'handlers': ['sentry', ],
        # },
    },
}

if PRINT_ALL_ERRORS:
    logging.basicConfig(
        level=logging.DEBUG, format="%(asctime)s %(levelname)s %(message)s",
    )

# Testing
TEST_RUNNER = env.str(
    "TEST_RUNNER", "main.tests.test_runners.BaseDjangoTestSuiteRunner"
)
TEST_OUTPUT_FILE_NAME = env.str(
    "TEST_OUTPUT_FILE_NAME", None
)  # For continuous integration reporting
if TEST_MODE:
    INSTALLED_APPS += TEST_APPS
    BROKER_BACKEND = "memory"  # Run celery tasks in memory

    STORAGE_TYPE = "local"
    MEDIA_ROOT = os.path.join(PROJECT_DIR, "testing/media/")
    SYNC_NOTIFIER_CLIENT_CLASS = (
        "main.sync.clients.NullSyncClient"  # Fake calls to sync server
    )

    # Disable migrations (makes tests a lot faster)
    # See http://stackoverflow.com/questions/25161425/disable-migrations-when-running-unit-tests-in-django-1-7
    class DisableMigrations(object):
        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return None

    MIGRATION_MODULES = DisableMigrations()

# Devserver
DEVSERVER_MODULES = (
    "devserver.modules.sql.SQLSummaryModule",
    "devserver.modules.profile.ProfileSummaryModule",
)

ENABLE_DEVSERVER = env.bool("ENABLE_DEVSERVER", False)
if ENABLE_DEVSERVER:
    INSTALLED_APPS += ("devserver",)
    MIDDLEWARE += ("devserver.middleware.DevServerMiddleware",)

# Warnings
import warnings
from main.exceptions.warning import (
    AppDeprecationWarning,
    AppPendingDeprecationWarning,
    AppRuntimeWarning,
)

warnings.filterwarnings("always", category=AppPendingDeprecationWarning)
warnings.filterwarnings("always", category=AppDeprecationWarning)

FILTER_DEPRECATION_WARNINGS = env.str("FILTER_DEPRECATION_WARNINGS", False)
if FILTER_DEPRECATION_WARNINGS:  # Filter deprecation warnings, if specified
    warnings.filterwarnings("error", category=AppDeprecationWarning)
    if not TEST_MODE:
        warnings.filterwarnings("error", category=AppRuntimeWarning)

# Storage
if STORAGE_TYPE == "sftp":
    STATICFILES_STORAGE = "main.storages.StaticSFTPStorage"
    DEFAULT_FILE_STORAGE = "main.storages.MediaSFTPStorage"
    SFTP_STORAGE_HOST = env.str("SFTP_STORAGE_HOST")
    SFTP_STORAGE_PARAMS = {
        "username": env.str("SFTP_STORAGE_USER"),
        "password": env.str("SFTP_STORAGE_PASSWORD"),
    }
elif STORAGE_TYPE == "minio":
    AWS_S3_ENDPOINT_URL = f'http://{env.str("MINIO_STORAGE_ENDPOINT")}'
    AWS_ACCESS_KEY_ID = env.str("MINIO_STORAGE_ACCESS_KEY")
    AWS_SECRET_ACCESS_KEY = env.str("MINIO_STORAGE_SECRET_KEY")
    AWS_AUTO_CREATE_BUCKET = True

    STATICFILES_STORAGE = "main.storages.AppMinioStaticStorage"
    DEFAULT_FILE_STORAGE = "main.storages.AppMinioMediaStorage"
    MINIO_STORAGE_AUTO_CREATE_STATIC_BUCKET = True
    MINIO_STORAGE_AUTO_CREATE_STATIC_POLICY = True
elif STORAGE_TYPE == "gs":
    DEFAULT_FILE_STORAGE = "main.storages.GoogleCloudMediaStorage"
    STATICFILES_STORAGE = "main.storages.GoogleCloudStaticStorage"
    GS_PROJECT_ID = env.str("GS_PROJECT_ID")
    GS_MEDIA_BUCKET_NAME = env.str("GS_MEDIA_BUCKET_NAME")
    GS_STATIC_BUCKET_NAME = env.str("GS_STATIC_BUCKET_NAME")
    GS_AUTO_CREATE_BUCKET = True
elif STORAGE_TYPE == "local":  # WARNING - should only be used for testing purposes
    DEFAULT_FILE_STORAGE = "django.core.files.storage.FileSystemStorage"
else:
    raise RuntimeError("Invalid storage type specified: '%s'" % STORAGE_TYPE)


# CORS
if env.list("CORS_ORIGIN_WHITELIST", str, None):
    raise RuntimeError(
        "CORS_ORIGIN_WHITELIST has been renamed to CORS_ALLOWED_ORIGINS and requires a scheme (e.g. https://)"
    )
CORS_ALLOWED_ORIGINS = env.list(
    "CORS_ALLOWED_ORIGINS", str, ["http://localhost:8080", "http://localhost:3000"]
)

# CHANNELS
CHANNELS_REDIS_HOST = env.str("CHANNELS_REDIS_HOST")
CHANNELS_CAPACITY_HTTP_REQUEST = env.int("CHANNELS_CAPACITY_HTTP_REQUEST", 100)
CHANNELS_CAPACITY_HTTP_RESPONSE = env.int("CHANNELS_CAPACITY_HTTP_RESPONSE", 100)
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [CHANNELS_REDIS_HOST],
            "channel_capacity": {
                "http.request": CHANNELS_CAPACITY_HTTP_REQUEST,
                "http.response*": CHANNELS_CAPACITY_HTTP_RESPONSE,
            },
        },
    },
}

ASGI_APPLICATION = "main.sync.routing.application"

# DB backups
DB_BACKUPS_PATH = "./backups/backup.sql"
DB_BACKUPS_AUTO_CREATE_BUCKET = True
DB_BACKUPS_BUCKET_NAME = env.str("DB_BACKUPS_BUCKET_NAME", "gbakery2-backups")
DB_BACKUPS_PROJECT_ID = env.str("DB_BACKUPS_PROJECT_ID", "gbakery2-153922")
DB_BACKUPS_CREDENTIALS_PATH = env.str("DB_BACKUPS_CREDENTIALS_PATH", None)
if DB_BACKUPS_CREDENTIALS_PATH is not None:
    DB_BACKUPS_CREDENTIALS = service_account.Credentials.from_service_account_file(
        DB_BACKUPS_CREDENTIALS_PATH
    )
DB_BACKUPS_TASK_ENABLED = env.bool("DB_BACKUPS_TASK_ENABLED", False)


# Collectfast
ENABLE_COLLECTFAST = env.bool("ENABLE_COLLECTFAST", False)
if ENABLE_COLLECTFAST:
    INSTALLED_APPS = (
        "collectfast",
    ) + INSTALLED_APPS  # Must go before `django.contrib.staticfiles`
    CACHES["collectfast"] = CACHES["default"]
    COLLECTFAST_CACHE = "collectfast"
    COLLECTFAST_THREADS = 50
    COLLECTFAST_DEBUG = True
    MAX_ENTRIES = 9999999

# Stripe
STRIPE_SECRET_KEY = env.str(
    "STRIPE_SECRET_KEY",
    "sk_test_51HbAtyFJw834zxH6YjCEMof21wgmFTIXjp3QzmbA4i6fHN0YshpRJDWaGBKeJLuUN9HjXxQWvW1du2R4GnFHh2hC00KnC9eZz0",
)
if STRIPE_SECRET_KEY:
    import stripe

    stripe.api_key = STRIPE_SECRET_KEY
# This variable can be found in webhook settings in the Stripe Developer Dashboard
STRIPE_ENDPOINT_SECRET = env.str("STRIPE_ENDPOINT_SECRET", None)
if TEST_MODE:
    STRIPE_ENDPOINT_SECRET = "test_endpoint_secret"

# Rollbar
ROLLBAR_ACCESS_TOKEN = env.str("ROLLBAR_ACCESS_TOKEN", None)
if ROLLBAR_ACCESS_TOKEN and not IS_SHELL:
    import rollbar

    # Connect access token
    rollbar.init(ROLLBAR_ACCESS_TOKEN)

    # Report all exceptions
    def rollbar_except_hook(exc_type, exc_value, traceback):
        rollbar.report_exc_info((exc_type, exc_value, traceback))
        sys.__excepthook__(exc_type, exc_value, traceback)

    sys.excepthook = rollbar_except_hook
