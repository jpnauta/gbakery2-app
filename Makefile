build:
	docker-compose build
start:
	docker-compose up --build
start_local:
	docker-compose up --build data_postgres data_redis minio
test:
	docker-compose -f docker-compose.test.yml up --build --force-recreate --abort-on-container-exit
clean:
	docker-compose down -v
	docker-compose -f docker-compose.test.yml down -v
shell:
	docker-compose exec web bash
test_structure:
	docker-compose -f docker-compose.yml build
	docker-compose -f docker-compose.test.structure.yml build
	docker-compose -f docker-compose.test.structure.yml up --abort-on-container-exit
stripe_listen:
	stripe listen --forward-to localhost:8000/api/public/stripe/webhooks/handler/