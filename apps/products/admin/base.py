from django.utils.safestring import mark_safe
from import_export.admin import ImportExportModelAdmin
from import_export.formats.base_formats import CSV

from main.admin import BaseModelAdmin, BaseTabularInline
from main.admin.actions import make_public, make_private
from products.admin.actions import star_products, unstar_products
from products.admin.forms import (
    FrontProductFPBPInlineFormset,
    FrontProductPriceInlineFormset,
    FrontProductForm,
)
from products.resources import FrontProductResource
from products.models import (
    ProductCategory,
    BakerProduct,
    FrontProduct,
    FrontProductBakerProduct,
    FrontProductPrice,
    FrontProductNotReady,
)


class ProductCategoryAdmin(BaseModelAdmin):
    model = ProductCategory

    list_display = (
        "image_tag",
        "id",
        "name",
        "is_public",
        "quantity_type",
        "parent",
        "created",
        "updated",
    )
    list_filter = (
        "is_public",
        "parent",
        "created",
        "quantity_type",
    )
    search_fields = ("name",)
    actions = (make_public, make_private)

    def image_tag(self, obj):
        return mark_safe(
            '<a href="%s"><img src="%s"></a>' % (obj.id, obj.image_url("xs_square"))
        )

    image_tag.__name__ = "image"
    image_tag.allow_tags = True


class BakerProductAdmin(BaseModelAdmin):
    model = BakerProduct

    list_display = (
        "name",
        "base_category",
        "category",
        "quantity_type_name",
        "created",
        "updated",
        "is_starred",
    )
    list_filter = (
        "category",
        "created",
        "is_starred",
    )
    search_fields = ("name",)
    actions = (star_products, unstar_products)


class FrontProductFPBPInline(BaseTabularInline):
    formset = FrontProductFPBPInlineFormset
    model = FrontProductBakerProduct


class FrontProductPriceInline(BaseTabularInline):
    formset = FrontProductPriceInlineFormset
    model = FrontProductPrice


class FrontProductAdmin(ImportExportModelAdmin, BaseModelAdmin):
    model = FrontProduct
    form = FrontProductForm

    resource_class = FrontProductResource
    formats = [CSV]
    skip_admin_log = True

    ordering = (
        "base_category__name",
        "category__name",
        "name",
    )
    list_display = (
        "name",
        "id",
        "base_category",
        "category",
        "image_tag",
        "is_starred",
        "quantity_type_name",
        "created",
        "updated",
        "rating",
    )
    list_filter = (
        "category",
        "created",
        "is_starred",
    )
    search_fields = ("name",)
    exclude = ("base_category",)
    inlines = (
        FrontProductFPBPInline,
        FrontProductPriceInline,
    )
    actions = (make_public, make_private, star_products, unstar_products)

    def image_tag(self, obj):
        return mark_safe(
            '<a href="%s"><img src="%s"></a>' % (obj.id, obj.image_url("xs_square"))
        )

    image_tag.__name__ = "image"
    image_tag.allow_tags = True

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related("prices",)
            .select_related("base_category", "category",)
        )


class FrontProductNotReadyAdmin(BaseModelAdmin):
    model = FrontProductNotReady


ProductCategoryAdmin.register()
BakerProductAdmin.register()
FrontProductAdmin.register()
FrontProductNotReadyAdmin.register()
