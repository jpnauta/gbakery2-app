from main.forms import BaseInlineFormSet, BaseModelForm
from products.models import FrontProduct


class FrontProductFPBPInlineFormset(BaseInlineFormSet):
    def clean(self):
        super().clean()
        self.clean_at_least_one_exists()
        self.clean_unique("baker_product")


class FrontProductPriceInlineFormset(BaseInlineFormSet):
    def clean(self):
        super().clean()
        self.clean_unique("quantity_type")


class FrontProductForm(BaseModelForm):
    class Meta:
        model = FrontProduct
        exclude = ()
