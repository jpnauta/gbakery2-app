from django.contrib import messages


def star_products(modeladmin, request, queryset):
    queryset.update(is_starred=True)

    messages.success(request, "Success! Selected items are now starred")


star_products.short_description = "Star products"


def unstar_products(modeladmin, request, queryset):
    queryset.update(is_starred=False)

    messages.success(request, "Success! Selected items are now un-starred")


unstar_products.short_description = "Un-star products"
