from django.db.models import Sum, Prefetch
from main.managers.querysets import BaseQuerySet, PublicPrivateQuerySet, RatedQuerySet


class ProductCategoryQuerySet(BaseQuerySet, PublicPrivateQuerySet):
    def primary(self):
        """
        Retrieves all primary (non-subcategory) categories
        """
        return self.filter(parent=None)


class FrontProductQuerySet(BaseQuerySet, PublicPrivateQuerySet, RatedQuerySet):
    def with_images(self):
        return self.exclude(image=None)

    def public(self):
        return super().public().filter(category__is_public=True)

    def for_order_date(self, d):
        return self.filter(orders__date=d).distinct()


class BakerProductQuerySet(BaseQuerySet):
    def for_order_date(self, d):
        return self.filter(front_products__order_products__order__date=d).distinct()


class FrontProductPriceQuerySet(PublicPrivateQuerySet, BaseQuerySet):
    pass
