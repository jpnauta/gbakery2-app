from django import forms
from main.constants import DATE_FORMAT
from main.forms import BaseForm


class BaseDateForm(BaseForm):
    date = forms.DateField(input_formats=[DATE_FORMAT])
