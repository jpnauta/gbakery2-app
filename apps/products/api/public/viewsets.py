from main.serializers.pagination import OptionalPageNumberPagination
from main.serializers.viewsets import AppReadOnlyModelViewSet, AppPublicViewSetMixin
from products.api.public.filters import PublicProductCategoryFilterSet
from products.api.public.serializers import PublicProductCategorySerializer
from products.models import ProductCategory


class PublicProductCategoryViewSet(AppPublicViewSetMixin, AppReadOnlyModelViewSet):
    lookup_field = "slug"
    model = ProductCategory
    queryset = model.objects.public()
    serializer_class = PublicProductCategorySerializer
    filter_class = PublicProductCategoryFilterSet
    pagination_class = OptionalPageNumberPagination

    def get_queryset(self):
        qs = super().get_queryset()

        return qs.prefetch_related(
            "front_products", "front_products__prices", "front_products__image"
        ).select_related("image")
