from main.serializers.filters import AppFilterSet
from products.models import ProductCategory


class PublicProductCategoryFilterSet(AppFilterSet):
    class Meta:
        model = ProductCategory
        fields = {
            "parent": ["exact", "isnull"],
        }
