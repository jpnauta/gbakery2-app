from rest_framework import serializers

from main.serializers import AppModelSerializer
from products.models import FrontProduct, FrontProductPrice, ProductCategory


class PublicProductCategorySerializer(AppModelSerializer):
    class FrontProductSerializer(AppModelSerializer):
        class FrontProductPriceSerializer(AppModelSerializer):
            class Meta:
                model = FrontProductPrice
                fields = (
                    "id",
                    "quantity_type",
                    "quantity_type_name",
                    "price",
                )

        prices = serializers.SerializerMethodField()
        detail_url = serializers.SerializerMethodField()
        img_sm_square = serializers.SerializerMethodField()
        img_med = serializers.SerializerMethodField()

        def prefetch_queryset(self, qs):
            return qs.prefetch_related("prices").select_related("image")

        def get_prices(self, product):
            qs = [p for p in product.prices.all() if p.is_public]
            return self.FrontProductPriceSerializer(instance=qs, many=True).data

        def get_detail_url(self, product):
            return None

        def get_img_sm_square(self, product):
            return product.image_url("sm_square")

        def get_img_med(self, product):
            return product.image_url("med")

        class Meta:
            model = FrontProduct
            fields = (
                "id",
                "name",
                "slug",
                "prices",
                "detail_url",
                "img_sm_square",
                "img_med",
                "description",
                "rating",
            )

    front_products = serializers.SerializerMethodField()

    def get_front_products(self, category):
        qs = category.front_products.all()
        return self.FrontProductSerializer(instance=qs, many=True).data

    class Meta:
        model = ProductCategory
        fields = (
            # TODO remove front products
            "id",
            "name",
            "slug",
            "parent",
            "img",
            "front_products",
        )
