from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError

from main.serializers.pagination import OptionalPageNumberPagination
from products.api.dashboard.filters import DashboardFrontProductNotReadyFilterSet
from products.api.forms import BaseDateForm
from products.api.dashboard.serializers import (
    DashFrontProductSerializer,
    DashBakerProductSerializer,
    DashProductCategorySerializer,
    DashFrontProductNotReadySerializer,
)
from reports.api.dashboard.serializers import DashProductCategoryReportSerializer
from main.serializers.viewsets import (
    ShowDetailsViewSetMixin,
    AppModelViewSet,
    AppReadOnlyModelViewSet,
    AppGenericViewSet,
)
from products.models import (
    FrontProduct,
    BakerProduct,
    ProductCategory,
    FrontProductNotReady,
)
from rest_framework.response import Response


class ReportViewSet(AppGenericViewSet):
    date_form_class = BaseDateForm

    def _report(self, report_serializer_class, qs, request, *args, **kwargs):

        form = self.date_form_class(data=request.query_params)

        if form.is_valid():
            d = form.cleaned_data["date"]
            serializer = report_serializer_class(date=d, instance=qs, many=True)
            return Response(serializer.data)
        else:
            raise ValidationError(form.errors)


class DashFrontProductViewSet(AppReadOnlyModelViewSet):
    lookup_field = "slug"
    model = FrontProduct
    queryset = model.objects.all().prefetch_related("prices").select_related("image")
    serializer_class = DashFrontProductSerializer
    pagination_class = OptionalPageNumberPagination


class DashBakerProductViewSet(AppReadOnlyModelViewSet):
    lookup_field = "slug"
    model = BakerProduct
    queryset = model.objects.all()
    serializer_class = DashBakerProductSerializer


class DashProductCategoryViewSet(AppReadOnlyModelViewSet, ReportViewSet):
    lookup_field = "slug"
    model = ProductCategory
    queryset = model.objects.all()
    serializer_class = DashProductCategorySerializer

    @action(detail=False, methods=["GET"])
    def report(self, *args, **kwargs):
        qs = self.get_queryset().primary()  # Only show base categories
        return self._report(DashProductCategoryReportSerializer, qs, *args, **kwargs)


class DashFrontProductNotReadyViewSet(ShowDetailsViewSetMixin, AppModelViewSet):
    model = FrontProductNotReady
    queryset = model.objects.all()
    serializer_class = DashFrontProductNotReadySerializer
    pagination_class = OptionalPageNumberPagination
    filter_class = DashboardFrontProductNotReadyFilterSet
