from main.serializers.filters import AppFilterSet
from products.models import FrontProductNotReady


class DashboardFrontProductNotReadyFilterSet(AppFilterSet):
    class Meta:
        model = FrontProductNotReady
        fields = {
            "date": ["exact"],
        }
