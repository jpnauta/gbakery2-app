from rest_framework import serializers

from main.serializers import AppModelSerializer, DetailsModelSerializer
from products.models import (
    FrontProduct,
    ProductCategory,
    FrontProductPrice,
    BakerProduct,
    FrontProductNotReady,
)


class DashFrontProductSerializer(AppModelSerializer):
    class FrontProductPriceSerializerForFP(AppModelSerializer):
        class Meta:
            model = FrontProductPrice
            fields = (
                "id",
                "quantity_type",
                "price",
            )

    prices = serializers.SerializerMethodField()
    img_sm_square = serializers.SerializerMethodField()
    img_med = serializers.SerializerMethodField()

    def get_prices(self, product):
        qs = product.prices.all()
        return self.FrontProductPriceSerializerForFP(instance=qs, many=True).data

    def get_img_sm_square(self, product):
        return product.image_url("sm_square")

    def get_img_med(self, product):
        return product.image_url("med")

    class Meta:
        model = FrontProduct
        fields = (
            "id",
            "name",
            "slug",
            "prices",
            "img_sm_square",
            "img_med",
            "description",
            "quantity_type",
            "rating",
            "is_starred",
        )


class DashBakerProductSerializer(AppModelSerializer):
    class Meta:
        model = BakerProduct
        fields = (
            "id",
            "name",
            "slug",
            "is_starred",
        )


class DashProductCategorySerializer(AppModelSerializer):
    class Meta:
        model = ProductCategory
        fields = (
            "id",
            "name",
            "slug",
        )


class DashNestFrontProductSerializerForFrontProductNotReady(AppModelSerializer):
    class Meta:
        model = FrontProduct
        fields = (
            "id",
            "slug",
            "name",
        )


class DashFrontProductNotReadySerializer(DetailsModelSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Send full product info if set to detail mode
        if self.details:
            serializer = DashNestFrontProductSerializerForFrontProductNotReady(
                many=False, read_only=True
            )
            self.fields["front_product"] = serializer

    class Meta:
        model = FrontProductNotReady
        fields = (
            "id",
            "front_product",
            "date",
        )
