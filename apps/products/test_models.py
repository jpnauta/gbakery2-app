from django.core.exceptions import ValidationError
from django.db import IntegrityError

from main.tests import BaseTestCase
from products.factories import (
    FrontProductFactory,
    ProductCategoryFactory,
    BakerProductFactory,
)
from products.models import QUANTITY_HALF_DOZEN, QUANTITY_INDIVIDUAL


class ProductTestCase(BaseTestCase):
    def test_it_capitalizes_category_name(self):
        category = ProductCategoryFactory(name=u"test category")

        self.assertEqual(category.name, u"Test Category")

    def test_it_avoids_category_name_collisions(self):
        ProductCategoryFactory(name=u"test oNe")

        try:
            ProductCategoryFactory(name=u"  TEST     one   ")
            self.fail(u"Name collision not prevented")
        except IntegrityError:
            pass

    def test_it_capitalizes_front_product_name(self):
        product = FrontProductFactory(name=u"test front product")

        self.assertEqual(product.name, u"Test Front Product")

    def test_it_capitalizes_baker_product_name(self):
        product = BakerProductFactory(name=u"test baker product")

        self.assertEqual(product.name, u"Test Baker Product")

    def test_it_sets_quantity_type_from_category(self):
        category = ProductCategoryFactory(quantity_type=QUANTITY_HALF_DOZEN)
        product = BakerProductFactory(category=category)

        self.assertEqual(product.quantity_type, QUANTITY_HALF_DOZEN)

    def test_it_updates_product_quantity_type_when_category_quantity_type_is_changed(
        self,
    ):
        category = ProductCategoryFactory(quantity_type=QUANTITY_HALF_DOZEN)
        product = BakerProductFactory(category=category)

        self.assertEqual(product.quantity_type, QUANTITY_HALF_DOZEN)
        category.quantity_type = QUANTITY_INDIVIDUAL
        category.save()

        product = product.reload()

        self.assertEqual(product.quantity_type, QUANTITY_INDIVIDUAL)


class ProductCategoryTestCase(BaseTestCase):
    def test_grandchildren_not_permitted(self):
        c1 = ProductCategoryFactory()
        c2 = ProductCategoryFactory(parent=c1)

        with self.assertRaises(ValidationError) as e:
            ProductCategoryFactory.build(parent=c2).clean()

        self.assertIn(
            "Cannot make a subcategory from another subcategory", str(e.exception)
        )
