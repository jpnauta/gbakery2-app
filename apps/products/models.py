# coding=utf-8
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import PROTECT, CASCADE, SET_NULL
from django.utils.translation import ugettext as _
from taggit.managers import TaggableManager
from tinymce.models import HTMLField

from images.models import Image
from main.ext.lists.utils import get_first
from main.managers import AppManager
from main.models import (
    BaseModel,
    Slugged,
    PublicPrivate,
    Rated,
    UniqueProperName,
    ModelDiff,
)
from main.models.fields import CurrencyField
from main.utils.generic import django_choice_options
from main.wrappers.signals import call_after_create, call_after_delete
from products.managers import (
    ProductCategoryQuerySet,
    FrontProductQuerySet,
    BakerProductQuerySet,
    FrontProductPriceQuerySet,
)

(QUANTITY_INDIVIDUAL, QUANTITY_HALF_DOZEN, QUANTITY_DOZEN) = (1, 2, 3)
QUANTITY_TYPE_OPTIONS = {
    QUANTITY_INDIVIDUAL: {
        "name": _(u"Individual"),
        "short_name": _(u"ea."),
        "short_name_2": _(u""),
        "quantity": 1,
    },
    QUANTITY_HALF_DOZEN: {
        "name": _(u"Half Dozen"),
        "short_name": _(u"½ doz."),
        "short_name_2": _(u"half doz."),
        "quantity": 6,
    },
    QUANTITY_DOZEN: {
        "name": _(u"Dozen"),
        "short_name": _(u"doz."),
        "short_name_2": _(u"doz."),
        "quantity": 12,
    },
}

QUANTITY_TYPES = django_choice_options(QUANTITY_TYPE_OPTIONS, "name")

(REPORT_DISPLAY_QUANTITY_TYPE, REPORT_DISPLAY_ORDER) = (1, 2)
REPORT_DISPLAY_TYPE_OPTIONS = {
    REPORT_DISPLAY_QUANTITY_TYPE: {"name": _(u"By Quantity Type"),},
    REPORT_DISPLAY_ORDER: {"name": _(u"By Order")},
}

REPORT_DISPLAY_TYPES = django_choice_options(REPORT_DISPLAY_TYPE_OPTIONS, "name")


class RelatedImageModel(models.Model):
    def image_url(self, *args, **kwargs):
        return Image.get_image_url(self.image, *args, **kwargs)

    @property
    def img(self):
        return self.image_url()

    class Meta:
        abstract = True


class ProductCategory(
    ModelDiff, RelatedImageModel, BaseModel, Slugged, UniqueProperName, PublicPrivate
):
    quantity_type = models.IntegerField(
        choices=QUANTITY_TYPES,
        default=QUANTITY_DOZEN,
        help_text=_(
            "The most frequent display type for the products of " "this category"
        ),
    )
    parent = models.ForeignKey(
        "self",
        related_name="children",
        on_delete=SET_NULL,
        null=True,
        blank=True,
        help_text=_("If set, this category is a subcategory of the specified category"),
    )
    image = models.ForeignKey(
        Image, related_name="categories", on_delete=PROTECT, null=True, blank=True
    )

    # Frontend meta
    fm_title = models.CharField(
        _("Frontend Title"), max_length=128, blank=True, null=True
    )
    fm_meta_description = models.TextField(
        _("Frontend Meta Description"), blank=True, null=True
    )
    fm_meta_keywords = models.CharField(
        _("Frontend Meta Keywords"), max_length=128, blank=True, null=True
    )
    fm_h1_tag = models.CharField(
        _("Frontend H1 Tag"), max_length=128, blank=True, null=True
    )
    fm_img_tag = models.TextField(_("Frontend Alt Img Tag"), blank=True, null=True)
    product_supplier_message = models.TextField(null=True, blank=True)

    objects = AppManager.from_queryset(ProductCategoryQuerySet)()

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.name

    @property
    def img(self):
        return self.image_url()

    def _update_product_quantity_types(self):
        # If category.quantity_type is changed, quantity_type for each product must be updated
        if self.field_has_changed("quantity_type"):
            for fp in self.front_products.all():
                fp.save()

            for fp in self.baker_products.all():
                fp.save()

    def save(self, *args, **kwargs):
        self._update_product_quantity_types()
        return super().save(*args, **kwargs)

    def clean(self):
        super().clean()

        if self.has_foreign_key("parent") and self.parent.parent_id:
            raise ValidationError("Cannot make a subcategory from another subcategory")

    class Meta:
        ordering = ("name",)
        verbose_name_plural = _("product categories")


class QuantityTypeCustom(models.Model):
    quantity_type_custom = models.IntegerField(
        _("Custom quantity type"),
        choices=QUANTITY_TYPES,
        null=True,
        blank=True,
        help_text=_(
            "Only use this if you wish to customize this product! The product category's "
            "quantity type will be used by default."
        ),
    )
    quantity_type = models.IntegerField(
        choices=QUANTITY_TYPES, null=True, editable=False
    )

    @property
    def quantity_type_name(self):
        obj = QUANTITY_TYPE_OPTIONS.get(self.quantity_type)

        if obj:
            return obj["name"]

    def _set_quantity_type(self):
        self.quantity_type = self.quantity_type_custom or self.category.quantity_type

    def save(self, *args, **kwargs):
        self._set_quantity_type()
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True


def base_category_factory(related_name, all_related_name):
    class BaseCategory(QuantityTypeCustom):
        category = models.ForeignKey(
            ProductCategory, related_name=related_name, on_delete=CASCADE
        )
        base_category = models.ForeignKey(
            ProductCategory,
            related_name=all_related_name,
            on_delete=CASCADE,
            blank=True,
            editable=False,
        )

        def _set_base_category(self):
            if self.has_foreign_key("category"):
                category = self.category

                while category.parent is not None:
                    category = category.parent

                self.base_category = category

        def save(self, *args, **kwargs):
            self._set_base_category()
            super().save(*args, **kwargs)

        class Meta:
            abstract = True

    return BaseCategory


class BakerProduct(
    BaseModel,
    Slugged,
    UniqueProperName,
    base_category_factory("baker_products", "all_baker_products"),
):
    is_starred = models.BooleanField(
        default=False, help_text=_("Product deserves special attention to make"),
    )

    objects = AppManager.from_queryset(BakerProductQuerySet)()

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.name

    class Meta:
        ordering = ("name",)


class FrontProduct(
    RelatedImageModel,
    BaseModel,
    Slugged,
    Rated,
    UniqueProperName,
    base_category_factory("front_products", "all_front_products"),
):
    description = HTMLField(null=True, blank=True)
    image = models.ForeignKey(
        Image, related_name="front_products", on_delete=PROTECT, null=True, blank=True
    )
    tags = TaggableManager(blank=True)

    baker_products = models.ManyToManyField(
        BakerProduct,
        through="products.FrontProductBakerProduct",
        related_name="front_products",
    )
    is_starred = models.BooleanField(
        default=False, help_text=_("Product deserves special attention to make"),
    )

    objects = AppManager.from_queryset(FrontProductQuerySet)()

    _price_info_map = None

    def calculate_rating(self):
        self.rating = self.order_products.count()

    def _get_fpp(self, quantity_type: int):
        return get_first(
            fpp for fpp in self.prices.all() if fpp.quantity_type == quantity_type
        )

    @property
    def price_info_map(self):
        # Used by `FrontProductResource` to export price info to CSVs
        if self._price_info_map is None:
            self._price_info_map = dict()
            for quantity_type in QUANTITY_TYPE_OPTIONS.keys():
                fpp = self._get_fpp(quantity_type)
                self._price_info_map[quantity_type] = (
                    dict(price=fpp.price, is_public=fpp.is_public,)
                    if fpp is not None
                    else None
                )
        return self._price_info_map

    def update_price_info_map(self, value: dict):
        # Used by `FrontProductResource` to import price info from CSVs
        for quantity_type in QUANTITY_TYPE_OPTIONS.keys():
            price_info = value.get(quantity_type)
            if price_info:
                fpp = self._get_fpp(quantity_type)
                if fpp is None:
                    fpp = FrontProductPrice()

                new_price = price_info["price"]
                new_is_public = price_info.get("is_public", False)
                if fpp.price != new_price or fpp.is_public != new_is_public:
                    fpp.front_product = self
                    fpp.quantity_type = quantity_type
                    fpp.price = new_price
                    fpp.is_public = new_is_public
                    fpp.save()
        self._price_info_map = value

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.name

    class Meta:
        ordering = ("name",)


class FrontProductBakerProduct(BaseModel):
    front_product = models.ForeignKey(
        FrontProduct, related_name="front_product_baker_products", on_delete=CASCADE,
    )
    baker_product = models.ForeignKey(
        BakerProduct, related_name="front_product_baker_products", on_delete=CASCADE,
    )

    # [num_items] front product objects == [goes_to] baker product objects
    num_items = models.PositiveIntegerField(_("Number of items"), default=1)
    goes_to = models.PositiveIntegerField(default=1)
    ratio = models.FloatField(
        default=1, editable=False
    )  # Auto calculated during save, useful for querying

    def _calculate_ratio(self):
        self.ratio = self.goes_to / float(self.num_items)

    def save(self, *args, **kwargs):
        self._calculate_ratio()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return u"%s %s → %s %s" % (
            self.num_items,
            self.front_product,
            self.goes_to,
            self.baker_product,
        )

    class Meta:
        unique_together = (("front_product", "baker_product",),)
        verbose_name = _("product relation")


class FrontProductPrice(PublicPrivate, BaseModel):
    front_product = models.ForeignKey(
        FrontProduct, related_name="prices", on_delete=CASCADE
    )
    quantity_type = models.IntegerField(choices=QUANTITY_TYPES)
    price = CurrencyField(validators=[MinValueValidator(Decimal("0"))])

    objects = AppManager.from_queryset(FrontProductPriceQuerySet)()

    class Meta:
        unique_together = (("front_product", "quantity_type",),)
        verbose_name = _("front product price")
        ordering = ("quantity_type",)  # Show individual first, then 1/2 doz, etc.

    @property
    def quantity_type_name(self):
        return QUANTITY_TYPE_OPTIONS[self.quantity_type]["name"]


class FrontProductNotReady(BaseModel):
    DATE_STR_FORMAT = "%Y-%m-%d"

    front_product = models.ForeignKey(
        FrontProduct, related_name="not_ready_dates", on_delete=CASCADE
    )
    date = models.DateField()

    # Derived data
    _date_str = models.CharField(
        max_length=256, editable=False
    )  # String representation of date

    def _all_orders(self):
        return self.front_product.orders.filter(date=self.date)

    def force_mark_orders_as_cannot_be_filled(self, *args, **kwargs):
        for order in self._all_orders():
            order.save(force_cannot_be_filled=True)

    def calculate_order_cannot_be_filled(self, *args, **kwargs):
        for order in self._all_orders():
            order.save()

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return u"%s not ready for %s" % (self.front_product, self.date)

    def _set_date_str(self):
        if self.date:
            self._date_str = self.date.strftime(self.DATE_STR_FORMAT)

    def save(self, *args, **kwargs):
        self._set_date_str()
        super().save(*args, **kwargs)

    class Meta:
        unique_together = (("front_product", "date",),)
        verbose_name_plural = _("front products not ready")


call_after_create(
    FrontProductNotReady.force_mark_orders_as_cannot_be_filled, FrontProductNotReady
)
call_after_delete(
    FrontProductNotReady.calculate_order_cannot_be_filled, FrontProductNotReady
)
