# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_date_str_for_existing_front_products_not_ready(apps, schema_editor):
    FrontProductNotReady = apps.get_model("products", "FrontProductNotReady")

    for order in FrontProductNotReady.objects.all():
        order._date_str = order.date.strftime("%Y-%m-%d")
        order.save()


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0015_auto_20151124_1929"),
    ]

    operations = [
        migrations.AddField(
            model_name="frontproductnotready",
            name="_date_str",
            field=models.CharField(default="TEMP", max_length=256, editable=False),
            preserve_default=False,
        ),
        migrations.RunPython(set_date_str_for_existing_front_products_not_ready),
    ]
