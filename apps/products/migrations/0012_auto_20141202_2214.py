# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0011_auto_20141202_1847"),
    ]

    operations = [
        migrations.CreateModel(
            name="FrontProductNotReady",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("date", models.DateField()),
                (
                    "front_product",
                    models.ForeignKey(
                        related_name="not_ready_dates",
                        to="products.FrontProduct",
                        on_delete=PROTECT,
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name="frontproductnotready",
            unique_together=set([("front_product", "date")]),
        ),
    ]
