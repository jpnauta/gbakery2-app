# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_quantity_type_for_existing_front_products(apps, schema):
    FrontProduct = apps.get_model("products", "FrontProduct")

    for fp in FrontProduct.objects.all():
        fp.quantity_type = fp.quantity_type_custom or fp.category.quantity_type
        fp.save()


def set_quantity_type_for_existing_baker_products(apps, schema):
    BakerProduct = apps.get_model("products", "BakerProduct")

    for bp in BakerProduct.objects.all():
        bp.quantity_type = bp.quantity_type_custom or bp.category.quantity_type
        bp.save()


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0017_auto_20151208_1709"),
    ]

    operations = [
        migrations.RunPython(set_quantity_type_for_existing_front_products),
        migrations.RunPython(set_quantity_type_for_existing_baker_products),
    ]
