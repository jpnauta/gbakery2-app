# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations


def set_quantity_type_for_existing_front_products(apps, schema):
    FrontProduct = apps.get_model("products", "FrontProduct")

    for fp in FrontProduct.objects.all():
        fp.quantity_type = fp.quantity_type_custom or fp.category.quantity_type


def set_quantity_type_for_existing_baker_products(apps, schema):
    BakerProduct = apps.get_model("products", "BakerProduct")

    for bp in BakerProduct.objects.all():
        bp.quantity_type = bp.quantity_type_custom or bp.category.quantity_type


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0016_frontproductnotready__date_str"),
    ]

    operations = [
        migrations.AddField(
            model_name="bakerproduct",
            name="quantity_type",
            field=models.IntegerField(
                null=True,
                editable=False,
                choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")],
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="frontproduct",
            name="quantity_type",
            field=models.IntegerField(
                null=True,
                editable=False,
                choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")],
            ),
            preserve_default=True,
        ),
        migrations.RunPython(set_quantity_type_for_existing_front_products),
        migrations.RunPython(set_quantity_type_for_existing_baker_products),
    ]
