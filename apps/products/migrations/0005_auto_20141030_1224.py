# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0004_auto_20141030_1011"),
    ]

    operations = [
        migrations.AlterField(
            model_name="frontproduct",
            name="rating",
            field=models.DecimalField(
                default=0, editable=False, max_digits=19, decimal_places=5
            ),
        ),
    ]
