# Generated by Django 2.2.16 on 2020-09-20 19:46

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0021_bakerproduct_is_starred"),
    ]

    operations = [
        migrations.AlterField(
            model_name="frontproduct",
            name="description",
            field=tinymce.models.HTMLField(blank=True, null=True),
        ),
    ]
