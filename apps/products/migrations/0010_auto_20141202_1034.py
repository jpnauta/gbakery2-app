# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def _set_base_category(product):
    category = product.category

    while category.parent is not None:
        category = category.parent

    product.base_category = category


def set_baker_product_base_category(apps, schema_editor):
    BakerProduct = apps.get_model("products", "BakerProduct")

    for product in BakerProduct.objects.all():
        _set_base_category(product)
        product.save()
        print(product.base_category.name)


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0009_bakerproduct_base_category"),
    ]

    operations = [
        migrations.RunPython(set_baker_product_base_category),
    ]
