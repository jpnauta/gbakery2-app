# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def _set_base_category(product):
    category = product.category

    while category.parent is not None:
        category = category.parent

    product.base_category = category


def reverse_fpbp_ratio(apps, schema_editor):
    FrontProductBakerProduct = apps.get_model("products", "FrontProductBakerProduct")

    for fpbp in FrontProductBakerProduct.objects.all():
        t_num_items = fpbp.num_items
        fpbp.num_items = fpbp.goes_to
        fpbp.goes_to = t_num_items
        fpbp.ratio = fpbp.goes_to / float(fpbp.num_items)

        fpbp.save()


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0012_auto_20141202_2214"),
    ]

    operations = [
        migrations.RunPython(reverse_fpbp_ratio),
    ]
