# -*- coding: utf-8 -*-

from django.db import models, migrations
from django.db.models import PROTECT

import main.models.fields
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ("taggit", "0001_initial"),
        ("images", "0002_auto_20141025_2254"),
    ]

    operations = [
        migrations.CreateModel(
            name="BakerProduct",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("slug", models.SlugField(unique=True, blank=True)),
                ("name", models.CharField(unique=True, max_length=128)),
                (
                    "quantity_type",
                    models.IntegerField(
                        choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")]
                    ),
                ),
            ],
            options={"ordering": ("name",),},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="FrontProduct",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("slug", models.SlugField(unique=True, blank=True)),
                (
                    "is_public",
                    models.BooleanField(
                        default=True, help_text="Shows/hides item from customers"
                    ),
                ),
                ("name", models.CharField(unique=True, max_length=128)),
                ("description", models.TextField(null=True, blank=True)),
                (
                    "quantity_type",
                    models.IntegerField(
                        choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")]
                    ),
                ),
                ("rating", models.IntegerField(default=0)),
            ],
            options={"ordering": ("name",),},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="FrontProductBakerProduct",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                (
                    "num_items",
                    models.PositiveIntegerField(
                        default=1, verbose_name="Number of items"
                    ),
                ),
                ("goes_to", models.PositiveIntegerField(default=1)),
                ("ratio", models.FloatField(default=1)),
                (
                    "baker_product",
                    models.ForeignKey(
                        related_name="front_product_baker_products",
                        to="products.BakerProduct",
                        on_delete=PROTECT,
                    ),
                ),
                (
                    "front_product",
                    models.ForeignKey(
                        related_name="front_product_baker_products",
                        to="products.FrontProduct",
                        on_delete=PROTECT,
                    ),
                ),
            ],
            options={"verbose_name": "product relation",},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="FrontProductPrice",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                (
                    "quantity_type",
                    models.IntegerField(
                        choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")]
                    ),
                ),
                (
                    "price",
                    main.models.fields.CurrencyField(max_digits=8, decimal_places=2),
                ),
                (
                    "front_product",
                    models.ForeignKey(
                        related_name="prices",
                        to="products.FrontProduct",
                        on_delete=PROTECT,
                    ),
                ),
            ],
            options={"ordering": ("quantity_type",), "verbose_name": "product price",},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="ProductCategory",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("slug", models.SlugField(unique=True, blank=True)),
                (
                    "is_public",
                    models.BooleanField(
                        default=True, help_text="Shows/hides item from customers"
                    ),
                ),
                ("name", models.CharField(unique=True, max_length=128)),
                (
                    "quantity_type",
                    models.IntegerField(
                        default=3,
                        help_text="The most frequent display type for the products of this category",
                        choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")],
                    ),
                ),
                (
                    "image",
                    models.ForeignKey(
                        related_name="categories",
                        blank=True,
                        to="images.Image",
                        null=True,
                        on_delete=PROTECT,
                    ),
                ),
                (
                    "parent",
                    models.ForeignKey(
                        related_name="children",
                        blank=True,
                        to="products.ProductCategory",
                        help_text="If set, this category is a subcategory of the specified category",
                        null=True,
                        on_delete=PROTECT,
                    ),
                ),
            ],
            options={
                "ordering": ("name",),
                "verbose_name_plural": "product categories",
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name="frontproductprice",
            unique_together=set([("front_product", "quantity_type")]),
        ),
        migrations.AlterUniqueTogether(
            name="frontproductbakerproduct",
            unique_together=set([("front_product", "baker_product")]),
        ),
        migrations.AddField(
            model_name="frontproduct",
            name="baker_products",
            field=models.ManyToManyField(
                related_name="front_products",
                through="products.FrontProductBakerProduct",
                to="products.BakerProduct",
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="frontproduct",
            name="category",
            field=models.ForeignKey(
                related_name="front_products",
                to="products.ProductCategory",
                on_delete=PROTECT,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="frontproduct",
            name="image",
            field=models.ForeignKey(
                related_name="front_products",
                blank=True,
                to="images.Image",
                null=True,
                on_delete=PROTECT,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="frontproduct",
            name="tags",
            field=taggit.managers.TaggableManager(
                to="taggit.Tag",
                through="taggit.TaggedItem",
                blank=True,
                help_text="A comma-separated list of tags.",
                verbose_name="Tags",
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="bakerproduct",
            name="category",
            field=models.ForeignKey(
                related_name="baker_products",
                to="products.ProductCategory",
                on_delete=PROTECT,
            ),
            preserve_default=True,
        ),
    ]
