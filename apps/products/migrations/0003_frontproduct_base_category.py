# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0002_auto_20141027_2102"),
    ]

    operations = [
        migrations.AddField(
            model_name="frontproduct",
            name="base_category",
            field=models.ForeignKey(
                related_name="all_front_products",
                blank=True,
                to="products.ProductCategory",
                null=True,
                on_delete=PROTECT,
            ),
            preserve_default=True,
        ),
    ]
