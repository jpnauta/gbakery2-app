# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0010_auto_20141202_1034"),
    ]

    operations = [
        migrations.AlterField(
            model_name="bakerproduct",
            name="quantity_type_custom",
            field=models.IntegerField(
                blank=True,
                help_text="Only use this if you wish to customize this product! The product category's quantity type will be used by default.",
                null=True,
                verbose_name="Custom quantity type",
                choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")],
            ),
        ),
        migrations.AlterField(
            model_name="frontproduct",
            name="quantity_type_custom",
            field=models.IntegerField(
                blank=True,
                help_text="Only use this if you wish to customize this product! The product category's quantity type will be used by default.",
                null=True,
                verbose_name="Custom quantity type",
                choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")],
            ),
        ),
    ]
