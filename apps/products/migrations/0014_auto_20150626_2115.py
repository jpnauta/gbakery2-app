# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0013_auto_20141212_1939"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="frontproductnotready",
            options={"verbose_name_plural": "front products not ready"},
        ),
        migrations.AlterField(
            model_name="bakerproduct",
            name="base_category",
            field=models.ForeignKey(
                related_name="all_baker_products",
                blank=True,
                editable=False,
                to="products.ProductCategory",
                on_delete=PROTECT,
            ),
        ),
        migrations.AlterField(
            model_name="frontproduct",
            name="base_category",
            field=models.ForeignKey(
                related_name="all_front_products",
                blank=True,
                editable=False,
                to="products.ProductCategory",
                on_delete=PROTECT,
            ),
        ),
    ]
