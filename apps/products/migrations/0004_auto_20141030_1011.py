# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import csv
import os

from django.db import models, migrations
from main.utils.slugs import unique_slugify
from main.ext.strings.utils import parse_int
import settings


def create_product_categories(apps, schema_editor):
    ProductCategory = apps.get_model("products", "ProductCategory")
    Image = apps.get_model("images", "Image")


def create_product_category_links(apps, schema_editor):
    ProductCategory = apps.get_model("products", "ProductCategory")


def create_front_products(apps, schema_editor):
    ProductCategory = apps.get_model("products", "ProductCategory")
    FrontProduct = apps.get_model("products", "FrontProduct")
    Image = apps.get_model("images", "Image")


def set_front_product_base_category(apps, schema_editor):
    FrontProduct = apps.get_model("products", "FrontProduct")

    for front_product in FrontProduct.objects.all():
        category = front_product.category

        while category.parent is not None:
            category = category.parent

        front_product.base_category = category

        front_product.save()


def create_baker_products(apps, schema_editor):
    ProductCategory = apps.get_model("products", "ProductCategory")
    BakerProduct = apps.get_model("products", "BakerProduct")


def create_front_product_back_products(apps, schema_editor):
    FrontProduct = apps.get_model("products", "FrontProduct")
    FrontProductBakerProduct = apps.get_model("products", "FrontProductBakerProduct")
    BakerProduct = apps.get_model("products", "BakerProduct")


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0003_frontproduct_base_category"),
        ("images", "0002_auto_20141025_2254"),
    ]

    operations = [
        migrations.RunPython(create_product_categories),
        migrations.RunPython(create_product_category_links),
        migrations.RunPython(create_front_products),
        migrations.RunPython(set_front_product_base_category),
        migrations.RunPython(create_baker_products),
        migrations.RunPython(create_front_product_back_products),
    ]
