# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(model_name="bakerproduct", name="quantity_type",),
        migrations.RemoveField(model_name="frontproduct", name="quantity_type",),
        migrations.AddField(
            model_name="bakerproduct",
            name="quantity_type_custom",
            field=models.IntegerField(
                blank=True,
                null=True,
                choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")],
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="frontproduct",
            name="quantity_type_custom",
            field=models.IntegerField(
                blank=True,
                null=True,
                choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")],
            ),
            preserve_default=True,
        ),
    ]
