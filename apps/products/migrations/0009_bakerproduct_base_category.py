# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0008_auto_20141201_2053"),
    ]

    operations = [
        migrations.AddField(
            model_name="bakerproduct",
            name="base_category",
            field=models.ForeignKey(
                related_name="all_baker_products",
                default=1,
                blank=True,
                to="products.ProductCategory",
                on_delete=PROTECT,
            ),
            preserve_default=False,
        ),
    ]
