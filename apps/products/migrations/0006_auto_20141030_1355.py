# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0005_auto_20141030_1224"),
    ]

    operations = [
        migrations.AlterField(
            model_name="bakerproduct",
            name="slug",
            field=models.SlugField(unique=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name="frontproduct",
            name="base_category",
            field=models.ForeignKey(
                related_name="all_front_products",
                blank=True,
                to="products.ProductCategory",
                on_delete=PROTECT,
            ),
        ),
        migrations.AlterField(
            model_name="frontproduct",
            name="slug",
            field=models.SlugField(unique=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name="frontproductbakerproduct",
            name="ratio",
            field=models.FloatField(default=1, editable=False),
        ),
        migrations.AlterField(
            model_name="productcategory",
            name="slug",
            field=models.SlugField(unique=True, editable=False, blank=True),
        ),
    ]
