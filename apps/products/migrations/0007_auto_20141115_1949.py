# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def reset_front_product_quantity_type_custom(apps, schema_editor):
    FrontProduct = apps.get_model("products", "FrontProduct")

    for fp in FrontProduct.objects.all():
        fp.quantity_type_custom = None
        fp.save()


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0006_auto_20141030_1355"),
    ]

    operations = [
        migrations.RunPython(reset_front_product_quantity_type_custom),
    ]
