# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0014_auto_20150626_2115"),
    ]

    operations = [
        migrations.AddField(
            model_name="productcategory",
            name="fm_h1_tag",
            field=models.CharField(
                max_length=128, null=True, verbose_name="Frontend H1 Tag", blank=True
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="productcategory",
            name="fm_img_tag",
            field=models.TextField(
                null=True, verbose_name="Frontend Alt Img Tag", blank=True
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="productcategory",
            name="fm_meta_description",
            field=models.TextField(
                null=True, verbose_name="Frontend Meta Description", blank=True
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="productcategory",
            name="fm_meta_keywords",
            field=models.CharField(
                max_length=128,
                null=True,
                verbose_name="Frontend Meta Keywords",
                blank=True,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="productcategory",
            name="fm_title",
            field=models.CharField(
                max_length=128, null=True, verbose_name="Frontend Title", blank=True
            ),
            preserve_default=True,
        ),
    ]
