# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from main.ext.strings.utils import clean_whitespace, title


def title_field(obj, field):
    string = clean_whitespace(title(getattr(obj, field, None)))
    setattr(obj, field, string)


def calculate_ratio(obj):
    obj.ratio = obj.goes_to / float(obj.num_items)


def resave_front_product_baker_products(apps, schema_editor):
    FrontProductBakerProduct = apps.get_model("products", "FrontProductBakerProduct")

    for fp_bp in FrontProductBakerProduct.objects.all():
        calculate_ratio(fp_bp)
        fp_bp.save()


def resave_front_products(apps, schema_editor):
    FrontProduct = apps.get_model("products", "FrontProduct")

    for product in FrontProduct.objects.all():
        title_field(product, "name")
        product.save()


def resave_baker_products(apps, schema_editor):
    BakerProduct = apps.get_model("products", "BakerProduct")

    for product in BakerProduct.objects.all():
        title_field(product, "name")
        product.save()


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0007_auto_20141115_1949"),
    ]

    operations = [
        migrations.RunPython(resave_front_product_baker_products),
        migrations.RunPython(resave_front_products),
        migrations.RunPython(resave_baker_products),
    ]
