from main.tests import BaseTestCase
from products.factories import FrontProductFactory
from products.tasks import set_front_product_rating


class SetFrontProductRatingsTestCase(BaseTestCase):
    def test_grandchildren_not_permitted(self):
        front_product = FrontProductFactory()
        set_front_product_rating(front_product.id)
