from main.celery import app
from main.utils.tasks import call_task


@app.task
def set_front_product_ratings():
    from products.models import FrontProduct

    for front_product in FrontProduct.objects.all():
        call_task(set_front_product_rating, front_product_id=front_product.id)


@app.task
def set_front_product_rating(front_product_id):
    from products.models import FrontProduct

    front_product = FrontProduct.objects.get(id=front_product_id)

    front_product.calculate_rating()
    front_product.save()
