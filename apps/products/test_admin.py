import csv
import html
import typing
from decimal import Decimal
from io import StringIO

from rest_framework.routers import flatten

from main.tests import BaseTestCase
from products.factories import (
    FrontProductFactory,
    FrontProductPriceFactory,
    ProductCategoryFactory,
)
from products.models import (
    QUANTITY_HALF_DOZEN,
    FrontProductPrice,
    QUANTITY_INDIVIDUAL,
    QUANTITY_DOZEN,
)
from users.models import User


class FrontProductAdminTestCase(BaseTestCase):
    def setUp(self):
        user = User.objects.create_user("admin@example.com", "password")
        user.is_staff = True
        user.is_superuser = True
        user.save()
        self.client.login(username="admin@example.com", password="password")

    def export_csv_data(self) -> str:
        response = self.client.post(
            "/admin/products/frontproduct/export/", {"file_format": "0"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.has_header("Content-Disposition"))
        self.assertEqual(response["Content-Type"], "text/csv")

        return response.content.decode("utf-8")

    def validate_csv_data(self, csv_data: str):
        # GET the import form
        response = self.client.get("/admin/products/frontproduct/import/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "admin/import_export/import.html")
        self.assertContains(response, 'form action=""')

        # POST the import form
        input_format = "0"
        data = {
            "input_format": input_format,
            "import_file": StringIO(csv_data),
        }
        response = self.client.post("/admin/products/frontproduct/import/", data)
        self.assertEqual(response.status_code, 200)
        self.assertIn("result", response.context)

        return response

    def get_error_message(self, validate_csv_response):
        if validate_csv_response.context["result"].has_errors():
            return ", ".join(
                flatten(
                    [
                        [str(e.error) for e in r.errors]
                        for r in validate_csv_response.context["result"]
                    ]
                )
            )
        return None

    def import_csv_data(self, csv_data: str):
        # Perform initial validation
        validate_csv_response = self.validate_csv_data(csv_data)

        # Ensure no validation failures
        self.assertIsNone(
            self.get_error_message(validate_csv_response),
            "An unexpected occurred while processing CSV",
        )
        self.assertIn("confirm_form", validate_csv_response.context)

        # Perform import
        confirm_form = validate_csv_response.context["confirm_form"]
        data = confirm_form.initial
        process_csv_response = self.client.post(
            "/admin/products/frontproduct/process_import/", data, follow=True
        )
        self.assertEqual(process_csv_response.status_code, 200)

    def csv_rows_to_data(self, rows: typing.List[typing.List[any]]) -> str:
        io = StringIO()
        writer = csv.writer(io)
        writer.writerows(rows)
        return io.getvalue()

    def csv_data_to_rows(self, csv_data: str) -> typing.List[typing.List[any]]:
        io = StringIO(csv_data)
        return list(csv.reader(io))

    def decode_html_response(self, response):
        return html.unescape(response.content.decode("utf-8"))

    def test_import_export_empty_list(self):
        self.import_csv_data(self.export_csv_data())

    def test_export_format(self):
        # GIVEN
        base_category = ProductCategoryFactory(name="Base Category")
        category = ProductCategoryFactory(name="My Category", parent=base_category)
        product = FrontProductFactory(name="My Product", category=category)
        FrontProductPriceFactory(
            front_product=product,
            quantity_type=QUANTITY_INDIVIDUAL,
            price=Decimal("0.30"),
        )
        FrontProductPriceFactory(
            front_product=product,
            quantity_type=QUANTITY_HALF_DOZEN,
            price=Decimal("1.42"),
        )
        FrontProductPriceFactory(
            front_product=product, quantity_type=QUANTITY_DOZEN, price=Decimal("2.40"),
        )

        # WHEN
        csv_data = self.export_csv_data()
        csv_rows = self.csv_data_to_rows(csv_data)

        # THEN
        self.assertEqual(
            csv_rows,
            [
                [
                    "id",
                    "base_category_name",
                    "category_name",
                    "category_is_public",
                    "name",
                    "price_individual",
                    "price_individual_is_public",
                    "price_half_dozen",
                    "price_half_dozen_is_public",
                    "price_dozen",
                    "price_dozen_is_public",
                ],
                [
                    str(product.id),
                    "Base Category",
                    "My Category",
                    "True",
                    "My Product",
                    "0.30",
                    "True",
                    "1.42",
                    "True",
                    "2.40",
                    "True",
                ],
            ],
        )

    def test_edit_product_price(self):
        # GIVEN
        category = ProductCategoryFactory(name="My Category")
        product = FrontProductFactory(name="My Product", category=category)
        front_product_price = FrontProductPriceFactory(
            front_product=product,
            quantity_type=QUANTITY_HALF_DOZEN,
            price=Decimal("1.42"),
        )

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[1][-4] = "1.43"

        # WHEN
        self.import_csv_data(self.csv_rows_to_data(csv_rows))

        # THEN
        front_product_prices = FrontProductPrice.objects.all()
        self.assertEqual(len(front_product_prices), 1)
        self.assertEqual(front_product_prices[0].id, front_product_price.id)
        self.assertEqual(front_product_prices[0].price, Decimal("1.43"))
        self.assertEqual(front_product_prices[0].quantity_type, QUANTITY_HALF_DOZEN)

    def test_edit_product_name(self):
        # GIVEN
        product = FrontProductFactory(name="My Product 1")

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[1][4] = "My Product 2"

        # WHEN
        self.import_csv_data(self.csv_rows_to_data(csv_rows))

        # THEN
        product = product.reload()
        self.assertEqual(product.name, "My Product 2")

    def test_add_product_prices(self):
        # GIVEN
        FrontProductFactory(name="My Product")

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[1][-6] = "0.31"
        csv_rows[1][-4] = "1.43"
        csv_rows[1][-1] = "No"
        csv_rows[1][-2] = "2.41"
        csv_rows[1][-1] = "Yes"

        # WHEN
        self.import_csv_data(self.csv_rows_to_data(csv_rows))

        # THEN
        front_product_prices = FrontProductPrice.objects.all()
        self.assertEqual(len(front_product_prices), 3)
        self.assertEqual(front_product_prices[0].price, Decimal("0.31"))
        self.assertEqual(front_product_prices[0].quantity_type, QUANTITY_INDIVIDUAL)
        self.assertEqual(front_product_prices[0].is_public, False)
        self.assertEqual(front_product_prices[1].price, Decimal("1.43"))
        self.assertEqual(front_product_prices[1].quantity_type, QUANTITY_HALF_DOZEN)
        self.assertEqual(front_product_prices[1].is_public, False)
        self.assertEqual(front_product_prices[2].price, Decimal("2.41"))
        self.assertEqual(front_product_prices[2].quantity_type, QUANTITY_DOZEN)
        self.assertEqual(front_product_prices[2].is_public, True)

    def test_edit_category_names_is_ignored(self):
        # GIVEN
        base_category = ProductCategoryFactory(name="Base Category")
        category = ProductCategoryFactory(name="My Category", parent=base_category)
        product = FrontProductFactory(name="My Product", category=category)
        front_product_price = FrontProductPriceFactory(
            front_product=product,
            quantity_type=QUANTITY_HALF_DOZEN,
            price=Decimal("1.42"),
        )

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[1][1] = "New Base Category"
        csv_rows[1][2] = "New Category"

        # WHEN
        self.import_csv_data(self.csv_rows_to_data(csv_rows))

        # THEN
        self.assertEqual(base_category, base_category.reload())
        self.assertEqual(category, category.reload())
        self.assertEqual(product, product.reload())
        self.assertEqual(front_product_price, front_product_price.reload())

    def test_add_product_is_rejected(self):
        # GIVEN
        product = FrontProductFactory(name="My Product")
        FrontProductPriceFactory(
            front_product=product, quantity_type=QUANTITY_HALF_DOZEN,
        )

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[1][0] = ""

        # WHEN
        response = self.validate_csv_data(self.csv_rows_to_data(csv_rows))
        response_content = self.decode_html_response(response)

        # THEN
        self.assertIn(
            "Creating front products through the import/export tool is not permitted!",
            response_content,
        )

    def test_duplicate_product_name_is_rejected(self):
        # GIVEN
        FrontProductFactory(name="My Product 1")
        FrontProductFactory(name="My Product 2")

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[2][4] = "My Product 1"

        # WHEN
        response = self.validate_csv_data(self.csv_rows_to_data(csv_rows))
        response_content = self.decode_html_response(response)

        # THEN
        self.assertIn(
            "(name)=(My Product 1) already exists.", response_content,
        )

    def test_invalid_price(self):
        # GIVEN
        FrontProductFactory(name="My Product")

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[1][-2] = "abc"

        # WHEN
        response = self.validate_csv_data(self.csv_rows_to_data(csv_rows))
        response_content = self.decode_html_response(response)

        # THEN
        self.assertIn(
            "decimal.ConversionSyntax", response_content,
        )

    def test_invalid_is_public(self):
        # GIVEN
        FrontProductFactory(name="My Product")

        # AND
        csv_rows = self.csv_data_to_rows(self.export_csv_data())
        csv_rows[1][-1] = "JAS"

        # WHEN
        self.import_csv_data(self.csv_rows_to_data(csv_rows))

        # THEN
        front_product_prices = FrontProductPrice.objects.all()
        self.assertEqual(len(front_product_prices), 0)
