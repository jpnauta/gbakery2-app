import factory

from images.factories import ImageFactory
from main.ext.dates.utils import utc_now
from main.factories import BaseModelFactory
from products.models import (
    ProductCategory,
    FrontProduct,
    BakerProduct,
    QUANTITY_DOZEN,
    FrontProductPrice,
    QUANTITY_INDIVIDUAL,
    FrontProductNotReady,
    FrontProductBakerProduct,
)


class ProductCategoryFactory(BaseModelFactory):
    name = factory.Sequence(lambda n: u"Category %s" % n)
    image = factory.SubFactory(ImageFactory)
    is_public = True

    class Meta:
        model = ProductCategory


class BakerProductFactory(BaseModelFactory):
    name = factory.Sequence(lambda n: u"Baker Product %s" % n)
    category = factory.SubFactory(ProductCategoryFactory)
    quantity_type = QUANTITY_DOZEN

    class Meta:
        model = BakerProduct


class FrontProductFactory(BaseModelFactory):
    name = factory.Sequence(lambda n: u"Front Product %s" % n)
    category = factory.SubFactory(ProductCategoryFactory)
    quantity_type = QUANTITY_DOZEN
    image = factory.SubFactory(ImageFactory)

    class Meta:
        model = FrontProduct


class FrontProductPriceFactory(BaseModelFactory):
    front_product = factory.SubFactory(FrontProductFactory)
    quantity_type = QUANTITY_INDIVIDUAL
    price = 42.42

    class Meta:
        model = FrontProductPrice


class FrontProductNotReadyFactory(BaseModelFactory):
    front_product = factory.SubFactory(FrontProductFactory)
    date = factory.LazyAttribute(lambda x: utc_now().date())

    class Meta:
        model = FrontProductNotReady


class FrontProductBakerProductFactory(BaseModelFactory):
    num_items = 2
    goes_to = 1

    class Meta:
        model = FrontProductBakerProduct
