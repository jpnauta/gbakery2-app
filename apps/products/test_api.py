from datetime import date, timedelta

from main.testing.assertions import assert_statuses_are_met
from main.tests import BaseAPITestCase
from orders.factories import (
    OrderFactory,
    FrontProductOrderProductFactory,
    SpecialProductOrderProductFactory,
)
from products.factories import (
    ProductCategoryFactory,
    FrontProductNotReadyFactory,
    FrontProductFactory,
    FrontProductPriceFactory,
    BakerProductFactory,
    FrontProductBakerProductFactory,
)
from products.models import (
    QUANTITY_DOZEN,
    QUANTITY_INDIVIDUAL,
    QUANTITY_HALF_DOZEN,
    REPORT_DISPLAY_ORDER,
    REPORT_DISPLAY_QUANTITY_TYPE,
)


class PublicProductCategoryViewSetTestCase(BaseAPITestCase):
    base_url = "/api/public/products/categories/"

    def test_list__pagination__is_disabled_by_default(self):
        ProductCategoryFactory(slug="category-slug", name="My Category")

        results = self.make_request("GET", self.get_list_url()).json()

        self.assertEqual(len(results), 1)

    def test_list__basic_order_location__data_format(self):
        pc = ProductCategoryFactory(slug="category-slug", name="My Category")
        fp = FrontProductFactory(
            category=pc,
            name="My Product",
            slug="product-slug",
            description="description",
            rating=42,
        )
        fpp = FrontProductPriceFactory(
            front_product=fp, quantity_type=QUANTITY_DOZEN, price="42.43",
        )

        page = self.make_request(
            "GET", self.get_list_url(), query_params=dict(page_size=10)
        ).json()

        self.assertEqual(len(page["results"]), 1)
        self.assertDictContainsSubset(
            dict(id=pc.id, name="My Category", slug="category-slug",),
            page["results"][0],
        )
        self.assertEqual(len(page["results"][0]["front_products"]), 1)
        self.assertDictContainsSubset(
            dict(
                id=fp.id,
                name="My Product",
                slug="product-slug",
                description="description",
                img_sm_square=fp.image_url("sm_square"),
                img_med=fp.image_url("med"),
                rating="42.00000",
            ),
            page["results"][0]["front_products"][0],
        )
        self.assertEqual(len(page["results"][0]["front_products"][0]["prices"]), 1)
        self.assertDictContainsSubset(
            dict(
                id=fpp.id,
                quantity_type=QUANTITY_DOZEN,
                price="42.43",
                quantity_type_name="Dozen",
            ),
            page["results"][0]["front_products"][0]["prices"][0],
        )

    def test_list__filter_by_parent_isnull__is_accepted(self):
        parent = ProductCategoryFactory(parent=None)
        child = ProductCategoryFactory(parent=parent)

        page = self.make_request(
            "GET",
            self.get_list_url(),
            query_params=dict(parent__isnull=True, page_size=10),
        ).json()

        self.assertPaginationResultsMatch(page, [parent.id])

    def test_list__filter_by_parent__is_accepted(self):
        parent = ProductCategoryFactory(parent=None)
        child = ProductCategoryFactory(parent=parent)

        page = self.make_request(
            "GET",
            self.get_list_url(),
            query_params=dict(parent=parent.id, page_size=10),
        ).json()

        self.assertPaginationResultsMatch(page, [child.id])

    def test_list__private_price__is_filtered_out(self):
        pc = ProductCategoryFactory()
        fp = FrontProductFactory(category=pc)
        FrontProductPriceFactory(
            front_product=fp, quantity_type=QUANTITY_INDIVIDUAL, is_public=True
        )
        FrontProductPriceFactory(
            front_product=fp, quantity_type=QUANTITY_DOZEN, is_public=False
        )

        page = self.make_request(
            "GET", self.get_list_url(), query_params=dict(page_size=10)
        ).json()

        self.assertEqual(len(page["results"][0]["front_products"][0]["prices"]), 1)

    def test_list__product_subcategory__data_format(self):
        parent_category = ProductCategoryFactory(name="Parent Category")
        child_category = ProductCategoryFactory(
            parent=parent_category, name="Child Category"
        )
        FrontProductFactory(
            category=child_category, name="My Product",
        )

        with self.assertNumQueries(5):
            page = self.make_request(
                "GET", self.get_list_url(), query_params=dict(page_size=10)
            ).json()

        self.assertEqual(len(page["results"]), 2)
        self.assertEqual(page["results"][0]["name"], "Child Category")
        self.assertEqual(len(page["results"][0]["front_products"]), 1)
        self.assertEqual(page["results"][0]["parent"], parent_category.id)
        self.assertEqual(page["results"][1]["name"], "Parent Category")
        self.assertEqual(len(page["results"][1]["front_products"]), 0)
        self.assertEqual(page["results"][1]["parent"], None)


class DashFrontProductViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/products/"

    def test_permissions__all__are_enforced(self):
        self.login_basic_user()

        assert_statuses_are_met(
            [
                ["GET", self.get_list_url(), 200],
                ["GET", self.get_obj_url(9999), 404],
                ["POST", self.get_list_url(), 405],
                ["PUT", self.get_obj_url(9999), 405],
                ["DELETE", self.get_obj_url(9999), 405],
            ],
            self,
        )


class DashBakerProductViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/baker-products/"

    def test_permissions__all__are_enforced(self):
        self.login_basic_user()

        assert_statuses_are_met(
            [
                ["GET", self.get_list_url(), 200],
                ["GET", self.get_obj_url(9999), 404],
                ["POST", self.get_list_url(), 405],
                ["PUT", self.get_obj_url(9999), 405],
                ["DELETE", self.get_obj_url(9999), 405],
            ],
            self,
        )


class DashProductCategoryViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/products/categories/"

    def setUp(self):
        super().setUp()
        self.user = self.login_basic_user()

    def test_report__basic_products__data_format(self):
        d = date(2020, 1, 1)

        category = ProductCategoryFactory(product_supplier_message="Test message")
        baker_product = BakerProductFactory(
            category=category, quantity_type=QUANTITY_HALF_DOZEN
        )
        front_product_1 = FrontProductFactory(
            category=category, quantity_type=QUANTITY_INDIVIDUAL
        )
        FrontProductBakerProductFactory(
            front_product=front_product_1,
            baker_product=baker_product,
            num_items=3,
            goes_to=2,
        )

        front_product_2 = FrontProductFactory(
            category=category, quantity_type=QUANTITY_HALF_DOZEN, is_starred=True,
        )
        FrontProductBakerProductFactory(
            front_product=front_product_2,
            baker_product=baker_product,
            num_items=1,
            goes_to=1,
        )

        order_1 = OrderFactory(date=d, name="Duplicate Name")
        FrontProductOrderProductFactory(
            front_product=front_product_1,
            order=order_1,
            quantity=3,
            quantity_type=QUANTITY_HALF_DOZEN,
        )
        FrontProductOrderProductFactory(
            front_product=front_product_1,
            order=order_1,
            quantity=6,
            quantity_type=QUANTITY_DOZEN,
        )
        FrontProductOrderProductFactory(
            front_product=front_product_2,
            order=order_1,
            quantity=2,
            quantity_type=QUANTITY_INDIVIDUAL,
        )

        order_2 = OrderFactory(date=d, name="Duplicate Name")
        FrontProductOrderProductFactory(
            front_product=front_product_1,
            order=order_2,
            quantity=9,
            quantity_type=QUANTITY_INDIVIDUAL,
        )
        FrontProductOrderProductFactory(
            front_product=front_product_2,
            order=order_2,
            quantity=1,
            quantity_type=QUANTITY_DOZEN,
        )
        SpecialProductOrderProductFactory(
            order=order_2, quantity=100,
        )

        # Different day - should be ignored
        order_3 = OrderFactory(date=d + timedelta(days=1))
        FrontProductOrderProductFactory(
            front_product=front_product_2,
            order=order_3,
            quantity=100,
            quantity_type=QUANTITY_DOZEN,
        )
        SpecialProductOrderProductFactory(
            order=order_3, quantity=20, quantity_type=QUANTITY_HALF_DOZEN,
        )

        with self.assertNumQueries(19):
            page = self.make_request(
                "GET", self.get_list_url("report/"), query_params=dict(date=d)
            ).json()

        self.assertEqual(
            page,
            [
                dict(
                    all_baker_products=[
                        dict(
                            id=baker_product.id,
                            name=baker_product.name,
                            is_starred=False,
                            quantity=80.0,
                            quantity_type=QUANTITY_DOZEN,
                            slug=baker_product.slug,
                        ),
                    ],
                    all_front_products=[
                        dict(
                            id=front_product_1.id,
                            is_starred=False,
                            name=front_product_1.name,
                            quantities_by_order=[
                                dict(
                                    order=order_1.id,
                                    order__name=order_1.name,
                                    total_quantity=90,
                                ),
                                dict(
                                    order=order_2.id,
                                    order__name=order_2.name,
                                    total_quantity=9,
                                ),
                            ],
                            quantities_by_type={
                                str(QUANTITY_INDIVIDUAL): 9,
                                str(QUANTITY_HALF_DOZEN): 18,
                                str(QUANTITY_DOZEN): 72,
                            },
                            quantity=99,
                            quantity_type=QUANTITY_DOZEN,
                            slug=front_product_1.slug,
                        ),
                        dict(
                            id=front_product_2.id,
                            is_starred=True,
                            name=front_product_2.name,
                            quantities_by_order=[
                                dict(
                                    order=order_2.id,
                                    order__name=order_2.name,
                                    total_quantity=12,
                                ),
                                dict(
                                    order=order_1.id,
                                    order__name=order_1.name,
                                    total_quantity=2,
                                ),
                            ],
                            quantities_by_type={
                                str(QUANTITY_INDIVIDUAL): 2,
                                str(QUANTITY_HALF_DOZEN): 0,
                                str(QUANTITY_DOZEN): 12,
                            },
                            quantity=14,
                            quantity_type=QUANTITY_DOZEN,
                            slug=front_product_2.slug,
                        ),
                    ],
                    id=category.id,
                    name=category.name,
                    product_supplier_message="Test message",
                    slug=category.slug,
                )
            ],
        )

    def test_permissions__all__are_enforced(self):
        self.login_basic_user()

        assert_statuses_are_met(
            [
                ["GET", self.get_list_url(), 200],
                ["GET", self.get_list_url() + "report/", 400],
                ["GET", self.get_obj_url(9999), 404],
                ["POST", self.get_list_url(), 405],
                ["PUT", self.get_obj_url(9999), 405],
                ["DELETE", self.get_obj_url(9999), 405],
            ],
            self,
        )


class DashFrontProductNotReadyViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/products/not-ready/"

    def setUp(self):
        self.login_basic_user()

    def test_permissions__all__are_enforced(self):
        assert_statuses_are_met(
            [
                ["GET", self.get_list_url(), 200],
                ["GET", self.get_obj_url(9999), 404],
                ["POST", self.get_list_url(), 400],
                ["PUT", self.get_obj_url(9999), 404],
                ["DELETE", self.get_obj_url(9999), 404],
            ],
            self,
        )

    def test_list__filter_by_date__is_accepted(self):
        FrontProductNotReadyFactory(date=date(2000, 1, 1))
        fpnr_2 = FrontProductNotReadyFactory(date=date(2000, 1, 2))
        FrontProductNotReadyFactory(date=date(2000, 1, 3))

        params = dict(date=date(2000, 1, 2), page_size=10)
        page = self.make_request("GET", self.get_list_url(), query_params=params).json()

        self.assertPaginationResultsMatch(page, [fpnr_2.id])
