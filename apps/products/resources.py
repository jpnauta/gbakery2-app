from django.core.exceptions import ValidationError
from import_export import resources
from import_export.fields import Field
from import_export.widgets import DecimalWidget

from main.resources.fields import AppBooleanWidget
from products.models import (
    FrontProduct,
    QUANTITY_INDIVIDUAL,
    QUANTITY_HALF_DOZEN,
    QUANTITY_DOZEN,
)


class FrontProductResource(resources.ModelResource):
    class Meta:
        model = FrontProduct
        fields = (
            "id",
            "base_category_name",
            "category_name",
            "category_is_public",
            "name",
            "price_individual",
            "price_individual_is_public",
            "price_half_dozen",
            "price_half_dozen_is_public",
            "price_dozen",
            "price_dozen_is_public",
        )
        export_order = fields
        skip_unchanged = True
        report_skipped = False

    base_category_name = Field()
    category_name = Field()
    category_is_public = Field(widget=AppBooleanWidget())
    price_individual = Field(widget=DecimalWidget())
    price_individual_is_public = Field(widget=AppBooleanWidget())
    price_half_dozen = Field(widget=DecimalWidget())
    price_half_dozen_is_public = Field(widget=AppBooleanWidget())
    price_dozen = Field(widget=DecimalWidget())
    price_dozen_is_public = Field(widget=AppBooleanWidget())

    def _get_price(self, obj: FrontProduct, quantity_type: int):
        price_info = obj.price_info_map.get(quantity_type)
        return price_info and price_info["price"]

    def _get_fpp_is_public(self, obj: FrontProduct, quantity_type: int):
        price_info = obj.price_info_map.get(quantity_type)
        return price_info and price_info["is_public"]

    def dehydrate_base_category_name(self, obj: FrontProduct):
        return obj.base_category.name

    def dehydrate_category_name(self, obj: FrontProduct):
        return obj.category.name

    def dehydrate_category_is_public(self, obj: FrontProduct):
        return obj.category.is_public

    def dehydrate_price_individual(self, obj: FrontProduct):
        return self._get_price(obj, QUANTITY_INDIVIDUAL)

    def dehydrate_price_individual_is_public(self, obj: FrontProduct):
        return self._get_fpp_is_public(obj, QUANTITY_INDIVIDUAL)

    def dehydrate_price_half_dozen(self, obj: FrontProduct):
        return self._get_price(obj, QUANTITY_HALF_DOZEN)

    def dehydrate_price_half_dozen_is_public(self, obj: FrontProduct):
        return self._get_fpp_is_public(obj, QUANTITY_HALF_DOZEN)

    def dehydrate_price_dozen(self, obj: FrontProduct):
        return self._get_price(obj, QUANTITY_DOZEN)

    def dehydrate_price_dozen_is_public(self, obj: FrontProduct):
        return self._get_fpp_is_public(obj, QUANTITY_DOZEN)

    def _get_price_info(self, row, quantity_name):
        price = DecimalWidget().clean(row.get(f"price_{quantity_name}"))
        is_public = (
            AppBooleanWidget().clean(row.get(f"price_{quantity_name}_is_public"))
            or False
        )
        if price or is_public:
            return dict(price=price, is_public=is_public,)
        return None

    def get_or_init_instance(self, instance_loader, row):
        # Build new `price_info_map` to be compared with current `price_info_map`
        row["price_info_map"] = {
            QUANTITY_INDIVIDUAL: self._get_price_info(row, "individual"),
            QUANTITY_HALF_DOZEN: self._get_price_info(row, "half_dozen"),
            QUANTITY_DOZEN: self._get_price_info(row, "dozen"),
        }

        instance, new = super().get_or_init_instance(instance_loader, row)

        if new:
            raise ValidationError(
                "Creating front products through the import/export tool is not permitted!"
            )

        return instance, new

    def import_obj(self, obj, data, dry_run):
        # Temporarily store `price_info_map` until it can be saved later
        obj._price_info_map = data["price_info_map"]
        return super().import_obj(obj, data, dry_run)

    def skip_row(self, instance, original):
        # Don't skip row if prices have changed
        if instance.price_info_map != original.price_info_map:
            return False
        return super().skip_row(instance, original)

    def save_instance(self, instance, *args, **kwargs):
        ret = super().save_instance(instance, *args, **kwargs)

        # Update prices
        instance.update_price_info_map(instance._price_info_map)

        return ret
