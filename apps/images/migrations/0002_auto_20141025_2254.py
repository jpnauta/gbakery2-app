# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import csv
import os
from django.core.files import File
from django.core.management import call_command

from django.db import models, migrations
import shutil
import re
from main.ext.dates.utils import utc_now
import settings


def _open_file(id, name):
    for ext in ["jpg", "JPG", "png"]:
        try:
            return open(
                os.path.join(
                    settings.PROJECT_DIR, "migrate/images/%s/%s.%s" % (id, name, ext)
                ),
                "rb",
            )
        except IOError:
            pass
    raise Exception("Could not find image: %s %s" % (id, name))


def create_images(apps, schema_editor):
    Image = apps.get_model("images", "Image")


class Migration(migrations.Migration):
    dependencies = [
        ("images", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(create_images),
    ]
