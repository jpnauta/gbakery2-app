# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("images", "0002_auto_20141025_2254"),
    ]

    operations = [
        migrations.AlterModelOptions(name="image", options={"ordering": ("image",)},),
    ]
