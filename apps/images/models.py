import os
from django.core.files import File
from django.db import models
from stdimage import StdImageField
from main.models import BaseModel
import settings


class Image(BaseModel):
    MISSING_IMAGE_PATH = os.path.join(
        settings.PROJECT_DIR, "static/assets/images/missing.jpg"
    )
    MISSING_IMAGE_ID = 999999

    image = StdImageField(
        upload_to="images",
        variations={
            "thumb": (50, 50, True),
            "sm": (400, 300),
            "med": (450, 480),
            "lg": (800, 600),
            "xs_square": (100, 100, True),
            "sm_square": (200, 200, True),
            "med_square": (400, 400, True),
        },
    )

    @property
    def image_name(self):
        return self.image.name

    def image_url(self, type=None, **kwargs):
        img = self.image
        if type is not None:
            img = getattr(img, type)

        return "%s%s" % (settings.MEDIA_URL, img)

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.image_name

    @classmethod
    def get_image_url(cls, image, *args, **kwargs):
        if image:
            return image.image_url(*args, **kwargs)
        return cls.default_image_url().image_url(*args, **kwargs)

    @classmethod
    def default_image_url(cls):
        # Attempt to retrieve default image
        try:
            if not hasattr(cls, "_missing_img"):
                # Get the default image and hold reference to it
                cls._missing_img = cls.objects.get(id=cls.MISSING_IMAGE_ID)
            return cls._missing_img
        except cls.DoesNotExist:
            pass

        # Create image object, if needed
        with open(cls.MISSING_IMAGE_PATH, "rb") as img_f:
            img = Image(id=cls.MISSING_IMAGE_ID, image=File(img_f.read()))
            img.save()
            return img

    class Meta:
        ordering = ("image",)
