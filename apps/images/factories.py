import os
from django.conf import settings
from images.models import Image
from main.factories import BaseModelFactory
from django.core.files.uploadedfile import SimpleUploadedFile


def read_file(name):
    return open(os.path.join(settings.PROJECT_DIR, name), mode="rb").read()


TEST_IMAGE = read_file("testing/fixtures/media/images/basic.jpg")


class ImageFactory(BaseModelFactory):
    image = SimpleUploadedFile(
        name="images/basic.jpg", content=TEST_IMAGE, content_type="image/jpg"
    )

    class Meta:
        model = Image
