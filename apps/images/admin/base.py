from django.utils.safestring import mark_safe

from images.models import Image
from main.admin import BaseModelAdmin


class ImageAdmin(BaseModelAdmin):
    model = Image

    list_display = ("image_tag", "image_name", "created", "updated")
    list_filter = ("created",)
    search_fields = ("image",)

    def image_tag(self, obj):
        return mark_safe(
            '<a href="%s"><img src="%s"></a>' % (obj.id, obj.image.xs_square.url)
        )

    image_tag.__name__ = "image"
    image_tag.allow_tags = True


ImageAdmin.register()
