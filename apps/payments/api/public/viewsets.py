import typing

import stripe
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from main.api.utils import get_object_or_202
from main.logging import logger
from main.serializers import AppSerializer
from main.serializers.viewsets import AppPublicViewSetMixin, AppGenericViewSet
from orders.models import Order, PAYMENT_STATUS_PAID
from settings import STRIPE_ENDPOINT_SECRET


class StripeWebhooksViewSet(AppPublicViewSetMixin, AppGenericViewSet):
    model = Order
    queryset = model.objects.all()
    serializer_class = AppSerializer

    def validate_stripe_webhook_payload(self) -> stripe.Event:
        """
        Ensures the request payload has been sent from Stripe

        https://stripe.com/docs/webhooks/signatures
        """
        if STRIPE_ENDPOINT_SECRET is None:
            raise RuntimeError(
                "Environment variable `STRIPE_ENDPOINT_SECRET` has not been configured"
            )

        if "HTTP_STRIPE_SIGNATURE" not in self.request.META:
            raise ValidationError("Missing Stripe signature header")

        try:
            return stripe.Webhook.construct_event(
                self.request.body,
                self.request.META["HTTP_STRIPE_SIGNATURE"],
                STRIPE_ENDPOINT_SECRET,
            )
        except ValueError:
            raise ValidationError("Invalid Stripe webhook payload")
        except stripe.error.SignatureVerificationError as e:
            raise ValidationError(str(e))

    def update_order_status(
        self, event: stripe.Event, payment_status: typing.Optional[str]
    ):
        """
        Changes the order specified in the event to the payment status specified
        """
        order = get_object_or_202(Order, id=event.data.object.get("metadata", {}).get("order_id"))
        order.stripe_charge_status = payment_status
        order.stripe_charge_id = event.data.object.get("charge")
        order.save()

    def update_refund_status(self, event: stripe.Event):
        """
        Changes the order specified in the event to the payment status specified
        """
        order = get_object_or_202(Order, stripe_charge_id=event.data.object.id)
        if event.data.object.refunded:
            order.stripe_charge_status = None
            order.clear_stripe_invoice()
            order.save()

    @action(detail=False, methods=["POST"])
    def handler(self, *args, **kwargs):
        """
        Handles all Stripe webhooks sent to the application
        """
        # Parse and validate event payload
        event = self.validate_stripe_webhook_payload()

        # Handle the event
        if event.type == "invoice.paid":
            self.update_order_status(event, PAYMENT_STATUS_PAID)
        elif event.type == "invoice.voided":
            self.update_order_status(event, None)
        elif event.type == "invoice.marked_uncollectible":
            self.update_order_status(event, PAYMENT_STATUS_PAID)
        elif event.type == "charge.refunded":
            self.update_refund_status(event)
        else:
            logger.warning(f"Unhandled event type {event.type}")

        return Response(dict())
