from unittest.mock import patch

import stripe

from main.testing.decorators import bypass_stripe_webhook_signature_check
from main.tests import BaseAPITestCase
from orders.factories import OrderFactory
from orders.models import Order


class StripeWebhooksViewSetTestCase(BaseAPITestCase):
    base_url = "/api/public/stripe/webhooks/"

    def setUp(self):
        # Delete existing orders
        Order.objects.all().delete()
        self.user = self.login_basic_user()
        super().setUp()

    def mock_webhook_event(self, event_type: str, order_id: int):
        return dict(
            type=event_type,
            data=dict(object=dict(metadata=dict(order_id=str(order_id)))),
        )

    def test_handler__invalid_payload__is_rejected(self):
        with patch.object(stripe.Webhook, "construct_event", side_effect=ValueError()):
            response = self.make_request(
                "POST",
                self.get_list_url("handler/"),
                data="",
                permitted_codes=[400],
                HTTP_STRIPE_SIGNATURE="test_signature",
            ).json()

        self.assertEqual(
            response["detail"], "Invalid Stripe webhook payload",
        )

    def test_handler__invalid_signature__is_rejected(self):
        response = self.make_request(
            "POST",
            self.get_list_url("handler/"),
            data=dict(),
            permitted_codes=[400],
            HTTP_STRIPE_SIGNATURE="test_signature",
        ).json()

        self.assertEqual(
            response["detail"],
            "Unable to extract timestamp and signatures from header",
        )

    def test_handler__order_exists__is_accepted(self):
        # GIVEN
        order = OrderFactory()
        event = self.mock_webhook_event(event_type="invoice.paid", order_id=order.id)

        # WHEN
        with bypass_stripe_webhook_signature_check():
            response = self.make_request(
                "POST",
                self.get_list_url("handler/"),
                data=event,
                HTTP_STRIPE_SIGNATURE="test_signature",
            ).json()

        # THEN
        self.assertEqual(response, dict())

        # AND
        order = order.reload()
        self.assertEqual(order.stripe_charge_status, "paid")

    def test_handler__order_does_not_exist__is_rejected(self):
        # GIVEN
        event = self.mock_webhook_event(event_type="invoice.paid", order_id=999)

        # WHEN/THEN
        with bypass_stripe_webhook_signature_check():
            self.make_request(
                "POST",
                self.get_list_url("handler/"),
                data=event,
                permitted_codes=[202],
                HTTP_STRIPE_SIGNATURE="test_signature",
            ).json()

    def test_handler__missing_signature_header__is_rejected(self):
        # GIVEN
        event = self.mock_webhook_event(event_type="invoice.paid", order_id=999)

        # WHEN/THEN
        response = self.make_request(
            "POST", self.get_list_url("handler/"), data=event, permitted_codes=[400]
        ).json()

        self.assertEqual(
            response["detail"], "Missing Stripe signature header",
        )

    def test_handler__external_invoices__is_rejected(self):
        # GIVEN
        event = dict(
            type="invoice.paid",
            data=dict(object=dict()),
        )

        # WHEN/THEN
        with bypass_stripe_webhook_signature_check():
            response = self.make_request(
                "POST", self.get_list_url("handler/"), data=event, permitted_codes=[202],
                HTTP_STRIPE_SIGNATURE="test_signature",
            ).json()

        self.assert_response_message_equals(response, "No Order matches the given query.")
