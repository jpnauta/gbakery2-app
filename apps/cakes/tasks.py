from main.celery import app
from main.utils.tasks import call_task


@app.task
def set_cake_ratings():
    from cakes.models import CakeProduct

    for cake in CakeProduct.objects.all():
        call_task(set_cake_rating, cake_id=cake.id)


@app.task
def set_cake_rating(cake_id):
    from products.models import FrontProduct

    cake = FrontProduct.objects.get(id=cake_id)

    cake.calculate_rating()
    cake.save()
