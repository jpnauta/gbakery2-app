from cakes.models import CakeProduct
from main.forms import BaseModelForm


class CakeProductForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Workaround for default values not being selected
        self.fields["allowed_sizes"].initial = CakeProduct.DEFAULT_SIZES
        self.fields["allowed_types"].initial = CakeProduct.DEFAULT_TYPES
        self.fields["allowed_fillings"].initial = CakeProduct.DEFAULT_FILLINGS
        self.fields["allowed_icings"].initial = CakeProduct.DEFAULT_ICINGS

    class Meta:
        model = CakeProduct
        exclude = ()
