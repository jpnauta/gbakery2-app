from django.utils.safestring import mark_safe

from main.admin.forms.fields import HTMLField

from cakes.admin.forms import CakeProductForm
from cakes.models import CakeProduct
from django.utils.translation import ugettext_lazy as _
from main.admin import BaseModelAdmin
from main.admin.actions import make_public, make_private


class CakeProductAdmin(BaseModelAdmin):
    model = CakeProduct
    form = CakeProductForm

    formfield_overrides = {"description": {"field": HTMLField}}

    list_display = (
        "id",
        "image_tag",
        "name",
        "is_public",
        "created",
        "updated",
    )
    list_filter = (
        "is_public",
        "created",
    )
    search_fields = ("name",)
    actions = (
        make_public,
        make_private,
    )

    def image_tag(self, obj):
        if obj.image:
            url = obj.image.image.xs_square.url
            return mark_safe('<a href="%s"><img src="%s"></a>' % (obj.id, url))
        return _("(N/A)")

    image_tag.__name__ = "image"
    image_tag.allow_tags = True


CakeProductAdmin.register()
