# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ("cakes", "0006_auto_20150724_1230"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cakeproduct",
            name="allowed_fillings",
            field=multiselectfield.db.fields.MultiSelectField(
                default=(
                    "301",
                    "302",
                    "401",
                    "402",
                    "403",
                    "404",
                    "405",
                    "501",
                    "502",
                    "503",
                    "504",
                    "601",
                    "602",
                    "603",
                    "604",
                    "605",
                ),
                help_text="Leave empty if none are applicable",
                max_length=2048,
                blank=True,
                choices=[
                    (301, "Vanilla custard"),
                    (302, "Chocolate custard"),
                    (701, "Cream cheese"),
                    (401, "White buttercream"),
                    (402, "Mocha buttercream"),
                    (403, "Chocolate buttercream"),
                    (404, "Mint buttercream"),
                    (405, "Lemon buttercream"),
                    (502, "Fresh fruit whipcream"),
                    (503, "Raspberry whipcream"),
                    (504, "Strawberry whipcream"),
                    (601, "Grand marnier"),
                    (602, "Black Forest"),
                    (603, "Hazelnut custard"),
                    (604, "Lemon filling"),
                    (605, "Chocolate Ganache"),
                    (501, "Whipcream"),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="cakeproduct",
            name="allowed_icings",
            field=multiselectfield.db.fields.MultiSelectField(
                default=("401", "402", "403", "404"),
                help_text="Leave empty if none are applicable",
                max_length=2048,
                blank=True,
                choices=[
                    (401, "White"),
                    (402, "Mocha"),
                    (403, "Chocolate"),
                    (404, "Whipcream"),
                    (501, "Mint"),
                    (502, "Grand Marnier"),
                    (503, "Lemon"),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="cakeproduct",
            name="allowed_sizes",
            field=multiselectfield.db.fields.MultiSelectField(
                default=("201", "202", "203", "204", "205"),
                help_text="Leave empty if none are applicable",
                max_length=2048,
                blank=True,
                choices=[
                    (201, "7x7"),
                    (202, "7x10"),
                    (203, "Half Slab"),
                    (204, "Three Quarter Slab"),
                    (205, "Full Slab"),
                    (101, "7 Inch Round"),
                    (102, "8 Inch Round"),
                    (103, "10 Inch Round"),
                    (104, "Baby Cake"),
                    (301, "12 Cupcake cake"),
                    (302, "24 Cupcake cake"),
                    (303, "Individual cupcakes"),
                ],
            ),
        ),
    ]
