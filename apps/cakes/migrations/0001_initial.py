# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import multiselectfield.db.fields
import taggit.managers
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("taggit", "0001_initial"),
        ("images", "0002_auto_20141025_2254"),
    ]

    operations = [
        migrations.CreateModel(
            name="CakeProduct",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("slug", models.SlugField(unique=True, blank=True)),
                (
                    "is_public",
                    models.BooleanField(
                        default=True, help_text="Shows/hides item from customers"
                    ),
                ),
                ("name", models.CharField(unique=True, max_length=256)),
                ("description", models.TextField(null=True, blank=True)),
                ("rating", models.IntegerField(default=0)),
                (
                    "allowed_sizes",
                    multiselectfield.db.fields.MultiSelectField(
                        default=("103", "104", "105", "106", "107"),
                        help_text="Leave empty if none are applicable",
                        max_length=2048,
                        blank=True,
                        choices=[
                            (101, "Small Round"),
                            (102, "Large Round"),
                            (103, "7x7"),
                            (104, "7x10"),
                            (105, "Half Slab"),
                            (106, "Three Quarter Slab"),
                            (107, "Full Slab"),
                        ],
                    ),
                ),
                (
                    "allowed_types",
                    multiselectfield.db.fields.MultiSelectField(
                        default=("201", "202"),
                        help_text="Leave empty if none are applicable",
                        max_length=2048,
                        blank=True,
                        choices=[
                            (201, "White"),
                            (202, "Chocolate"),
                            (203, "White and chocolate cake"),
                            (204, "Red velvet"),
                            (205, "Carrot"),
                        ],
                    ),
                ),
                (
                    "allowed_fillings",
                    multiselectfield.db.fields.MultiSelectField(
                        default=(
                            "301",
                            "302",
                            "303",
                            "304",
                            "305",
                            "306",
                            "307",
                            "308",
                            "309",
                            "310",
                            "311",
                            "312",
                        ),
                        help_text="Leave empty if none are applicable",
                        max_length=2048,
                        blank=True,
                        choices=[
                            (301, "Bavarian cream"),
                            (302, "White butter cream"),
                            (303, "Mocha butter cream"),
                            (304, "Chocolate butter cream"),
                            (305, "Mint butter cream"),
                            (306, "Whip cream"),
                            (307, "Fresh fruit whip cream"),
                            (308, "Raspberry whip cream"),
                            (309, "Grand marnier"),
                            (310, "Black Forest"),
                            (311, "Hazelnut custard"),
                            (312, "Lemon custard"),
                        ],
                    ),
                ),
                (
                    "allowed_icings",
                    multiselectfield.db.fields.MultiSelectField(
                        default=("401", "402", "403", "404"),
                        help_text="Leave empty if none are applicable",
                        max_length=2048,
                        blank=True,
                        choices=[
                            (401, "White"),
                            (402, "Mocha"),
                            (403, "Chocolate"),
                            (404, "Whip cream"),
                        ],
                    ),
                ),
                (
                    "image",
                    models.ForeignKey(
                        related_name="cakes", to="images.Image", on_delete=PROTECT
                    ),
                ),
                (
                    "tags",
                    taggit.managers.TaggableManager(
                        to="taggit.Tag",
                        through="taggit.TaggedItem",
                        blank=True,
                        help_text="A comma-separated list of tags.",
                        verbose_name="Tags",
                    ),
                ),
            ],
            options={"ordering": ("name",), "verbose_name": "cake",},
            bases=(models.Model,),
        ),
    ]
