# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ("cakes", "0003_auto_20141030_1227"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cakeproduct",
            name="allowed_sizes",
            field=multiselectfield.db.fields.MultiSelectField(
                default=("103", "104", "105", "106", "107"),
                help_text="Leave empty if none are applicable",
                max_length=2048,
                blank=True,
                choices=[
                    (101, "7 Inch Round"),
                    (102, "8 Inch Round"),
                    (103, "7x7"),
                    (104, "7x10"),
                    (105, "Half Slab"),
                    (106, "Three Quarter Slab"),
                    (107, "Full Slab"),
                    (108, "24 Cupcake cake"),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="cakeproduct",
            name="slug",
            field=models.SlugField(unique=True, editable=False, blank=True),
        ),
    ]
