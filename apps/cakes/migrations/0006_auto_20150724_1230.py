# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

cake_filling_change_map = {
    301: 301,
    302: 401,
    303: 402,
    304: 403,
    305: 404,
    306: 501,
    307: 502,
    308: 503,
    309: 601,
    310: 602,
    311: 603,
    312: 604,
}

reverse_cake_filling_change_map = {}
for key, item in cake_filling_change_map.items():
    reverse_cake_filling_change_map[item] = key


def change_cake_product_filling_ids(apps, schema_editor):
    for cake in apps.get_model("cakes", "CakeProduct").objects.all():
        if cake.allowed_fillings:
            cake.allowed_fillings = [
                repr(cake_filling_change_map[int(val)]) for val in cake.allowed_fillings
            ]
            cake.save()


def reverse_change_cake_product_filling_ids(apps, schema_editor):
    for cake in apps.get_model("cakes", "CakeProduct").objects.all():
        if cake.allowed_fillings:
            cake.allowed_fillings = [
                repr(reverse_cake_filling_change_map[int(val)])
                for val in cake.allowed_fillings
            ]
            cake.save()


class Migration(migrations.Migration):

    dependencies = [
        ("cakes", "0005_auto_20150724_1203"),
    ]

    operations = [
        migrations.RunPython(
            change_cake_product_filling_ids, reverse_change_cake_product_filling_ids
        )
    ]
