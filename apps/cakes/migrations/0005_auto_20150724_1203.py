# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

cake_size_change_map = {
    101: 101,
    102: 102,
    103: 201,
    104: 202,
    105: 203,
    106: 204,
    107: 205,
    108: 302,
}


def change_cake_product_size_ids(apps, schema_editor):
    for cake in apps.get_model("cakes", "CakeProduct").objects.all():
        if cake.allowed_sizes:
            cake.allowed_sizes = [
                repr(cake_size_change_map[int(val)]) for val in cake.allowed_sizes
            ]
            cake.save()


class Migration(migrations.Migration):

    dependencies = [
        ("cakes", "0004_auto_20150626_2115"),
    ]

    operations = [migrations.RunPython(change_cake_product_size_ids)]
