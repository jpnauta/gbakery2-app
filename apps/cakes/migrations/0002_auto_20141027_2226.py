# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import csv
from datetime import datetime
import os

from django.db import models, migrations
from main.ext.dates.utils import utc_now
from main.utils.slugs import unique_slugify
from main.ext.strings.utils import parse_int
import settings


def _const_list(constants):
    return [repr(x) for x in constants]


def create_cakes(apps, schema_editor):
    CakeProduct = apps.get_model("cakes", "CakeProduct")
    Image = apps.get_model("images", "Image")
    Tag = apps.get_model("taggit", "Tag")
    TagItem = apps.get_model("taggit", "TaggedItem")
    ContentType = apps.get_model("contenttypes", "ContentType")


class Migration(migrations.Migration):
    dependencies = [("contenttypes", "0001_initial"), ("cakes", "0001_initial")]

    operations = [
        migrations.RunPython(create_cakes),
    ]
