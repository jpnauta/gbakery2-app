# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("cakes", "0002_auto_20141027_2226"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cakeproduct",
            name="rating",
            field=models.DecimalField(
                default=0, editable=False, max_digits=19, decimal_places=5
            ),
        ),
    ]
