from main.managers.querysets import BaseQuerySet, PublicPrivateQuerySet, RatedQuerySet


class CakeProductQuerySet(BaseQuerySet, PublicPrivateQuerySet, RatedQuerySet):
    pass
