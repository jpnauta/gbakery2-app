from rest_framework.filters import SearchFilter
from rest_framework.viewsets import ReadOnlyModelViewSet

from cakes.api.frontend.serializers import PublicCakeProductSerializer
from cakes.models import CakeProduct
from main.ext.strings.utils import parse_int

from main.serializers.viewsets import AppPublicViewSetMixin


class PublicCakeProductViewSet(AppPublicViewSetMixin, ReadOnlyModelViewSet):
    lookup_field = "slug"
    model = CakeProduct
    queryset = model.objects.public()
    serializer_class = PublicCakeProductSerializer
    search_fields = ["name", "tags__name"]

    def get_queryset(self):
        qs = super().get_queryset()

        return qs.select_related("image")

    def filter_queryset(self, qs):
        num = parse_int(self.request.query_params.get(SearchFilter.search_param))

        if num:
            # If specific number given, show that cake
            qs = qs.filter(id=num)
        else:
            qs = super().filter_queryset(qs)

            # If number not specified, only show cakes available to public
            qs = qs.filter(is_public=True)

        return qs
