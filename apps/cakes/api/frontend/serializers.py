from rest_framework import serializers
from cakes.models import CakeProduct
from main.serializers import AppModelSerializer


class PublicCakeProductSerializer(AppModelSerializer):
    img_sm = serializers.SerializerMethodField()
    img_xs_square = serializers.SerializerMethodField()
    img_med = serializers.SerializerMethodField()
    img_med_square = serializers.SerializerMethodField()

    def get_img_sm(self, cake):
        return cake.image_url("sm")

    def get_img_xs_square(self, cake):
        return cake.image_url("xs_square")

    def get_img_med_square(self, cake):
        return cake.image_url("med_square")

    def get_img_med(self, cake):
        return cake.image_url("med")

    class Meta:
        model = CakeProduct
        fields = (
            "id",
            "name",
            "created",
            "updated",
            "description",
            "slug",
            "img_sm",
            "img_med",
            "img_xs_square",
            "img_med_square",
            "allowed_sizes",
            "allowed_types",
            "allowed_fillings",
            "allowed_icings",
        )
