# coding=utf-8
from django.db import models
from django.db.models import PROTECT
from django.utils.translation import ugettext as _
from multiselectfield import MultiSelectField
from taggit.managers import TaggableManager
from tinymce.models import HTMLField

from cakes.managers import CakeProductQuerySet
from images.models import Image
from main.managers import AppManager
from main.models import BaseModel, Slugged, PublicPrivate, Rated
from main.utils.generic import django_choice_options


class CakeProduct(BaseModel, Slugged, PublicPrivate, Rated):
    ################### SIZE OPTIONS ###################
    (
        SIZE_SMALL_ROUND,
        SIZE_LARGE_ROUND,
        SIZE_10_INCH_ROUND,
        SIZE_BABY_CAKE,
        SIZE_7X7,
        SIZE_7X10,
        SIZE_HALF_SLAB,
        SIZE_3_4_SLAB,
        SIZE_FULL_SLAB,
        SIZE_12_CUPCAKE_CAKE,
        SIZE_24_CUPCAKE_CAKE,
        SIZE_INDIVIDUAL_CUPCAKES,
    ) = (
        "101",
        "102",
        "103",
        "104",
        "201",
        "202",
        "203",
        "204",
        "205",
        "301",
        "302",
        "303",
    )
    SIZE_OPTIONS = {
        SIZE_SMALL_ROUND: {"name": _("7 Inch Round"),},
        SIZE_LARGE_ROUND: {"name": _("8 Inch Round"),},
        SIZE_10_INCH_ROUND: {"name": _("10 Inch Round"),},
        SIZE_BABY_CAKE: {"name": _("Baby Cake"),},
        SIZE_7X7: {"name": _("7x7"),},
        SIZE_7X10: {"name": _("7x10"),},
        SIZE_HALF_SLAB: {"name": _("Half Slab"),},
        SIZE_3_4_SLAB: {"name": _("Three Quarter Slab"),},
        SIZE_FULL_SLAB: {"name": _("Full Slab"),},
        SIZE_12_CUPCAKE_CAKE: {"name": _("12 Cupcake cake"),},
        SIZE_24_CUPCAKE_CAKE: {"name": _("24 Cupcake cake"),},
        SIZE_INDIVIDUAL_CUPCAKES: {"name": _("Individual cupcakes"),},
    }
    SIZES = django_choice_options(SIZE_OPTIONS, "name")
    DEFAULT_SIZES = (
        SIZE_7X7,
        SIZE_7X10,
        SIZE_HALF_SLAB,
        SIZE_3_4_SLAB,
        SIZE_FULL_SLAB,
    )

    ################### BASE OPTIONS ###################
    (
        TYPE_WHITE,
        TYPE_CHOCOLATE,
        TYPE_WHITE_AND_CHOCOLATE,
        TYPE_RED_VELVET,
        TYPE_CARROT,
    ) = (
        "201",
        "202",
        "203",
        "204",
        "205",
    )
    TYPE_OPTIONS = {
        TYPE_WHITE: {"name": _("White"),},
        TYPE_CHOCOLATE: {"name": _("Chocolate"),},
        TYPE_WHITE_AND_CHOCOLATE: {"name": _("White and chocolate cake"),},
        # TYPE_RED_VELVET: {
        #     'name': _('Red velvet'),
        # },
        TYPE_CARROT: {"name": _("Carrot"),},
    }
    TYPES = django_choice_options(TYPE_OPTIONS, "name")
    DEFAULT_TYPES = (
        TYPE_WHITE,
        TYPE_CHOCOLATE,
    )

    ################### FILLING OPTIONS ###################
    (
        F_CUSTARD,
        F_CHOCOLATE_CUSTARD,
        F_WHITE_BUTTERCREAM,
        F_MOCHA_BUTTERCREAM,
        F_CHOCOLATE_BUTTERCREAM,
        F_MINT_BUTTERCREAM,
        F_LEMON_BUTTERCREAM,
        F_WHIP_CREAM,
        F_FRUIT_WHIP_CREAM,
        F_RASPBERRY_WHIP_CREAM,
        F_STRAWBERRY_WHIP_CREAM,
        F_GRAND_MARNIER,
        F_BLACK_FOREST,
        F_HAZELNUT,
        F_LEMON_CUSTARD,
        F_CHOCOLATE_GANACHE,
        F_CREAM_CHEESE,
        F_OREO_COOKIE,
    ) = (
        "301",
        "302",
        "401",
        "402",
        "403",
        "404",
        "405",
        "501",
        "502",
        "503",
        "504",
        "601",
        "602",
        "603",
        "604",
        "605",
        "701",
        "702",
    )
    FILLING_OPTIONS = {
        F_CUSTARD: {"name": _("Vanilla custard"),},
        F_CHOCOLATE_CUSTARD: {"name": _("Chocolate custard"),},
        F_WHITE_BUTTERCREAM: {"name": _("White buttercream"),},
        F_MOCHA_BUTTERCREAM: {"name": _("Mocha buttercream"),},
        F_CHOCOLATE_BUTTERCREAM: {"name": _("Chocolate buttercream"),},
        F_MINT_BUTTERCREAM: {"name": _("Mint buttercream"),},
        F_LEMON_BUTTERCREAM: {"name": _("Lemon buttercream"),},
        F_WHIP_CREAM: {"name": _("Whipcream"),},
        F_FRUIT_WHIP_CREAM: {"name": _("Fresh fruit whipcream"),},
        F_RASPBERRY_WHIP_CREAM: {"name": _("Raspberry whipcream"),},
        F_STRAWBERRY_WHIP_CREAM: {"name": _("Strawberry whipcream"),},
        F_GRAND_MARNIER: {"name": _("Grand marnier"),},
        F_BLACK_FOREST: {"name": _("Black Forest"),},
        F_HAZELNUT: {"name": _("Hazelnut custard"),},
        F_LEMON_CUSTARD: {"name": _("Lemon filling"),},
        F_CHOCOLATE_GANACHE: {"name": _("Chocolate Ganache"),},
        F_CREAM_CHEESE: {"name": _("Cream cheese"),},
        F_OREO_COOKIE: {"name": _("Oreo cookie"),},
    }
    FILLINGS = django_choice_options(FILLING_OPTIONS, "name")
    DEFAULT_FILLINGS = (
        F_CUSTARD,
        F_CHOCOLATE_CUSTARD,
        F_WHITE_BUTTERCREAM,
        F_MOCHA_BUTTERCREAM,
        F_CHOCOLATE_BUTTERCREAM,
        F_MINT_BUTTERCREAM,
        F_LEMON_BUTTERCREAM,
        F_WHIP_CREAM,
        F_FRUIT_WHIP_CREAM,
        F_RASPBERRY_WHIP_CREAM,
        F_STRAWBERRY_WHIP_CREAM,
        F_GRAND_MARNIER,
        F_BLACK_FOREST,
        F_HAZELNUT,
        F_LEMON_CUSTARD,
        F_CHOCOLATE_GANACHE,
    )

    ################### ICING OPTIONS ###################
    (
        ICING_WHITE,
        ICING_MOCHA,
        ICING_CHOCOLATE,
        ICING_WHIP_CREAM,
        ICING_MINT,
        ICING_GRAND_MARNIER,
        ICING_LEMON,
    ) = (
        "401",
        "402",
        "403",
        "404",
        "501",
        "502",
        "503",
    )
    ICING_OPTIONS = {
        ICING_WHITE: {"name": _("White"),},
        ICING_MOCHA: {"name": _("Mocha"),},
        ICING_CHOCOLATE: {"name": _("Chocolate"),},
        ICING_WHIP_CREAM: {"name": _("Whipcream"),},
        ICING_MINT: {"name": _("Mint"),},
        ICING_GRAND_MARNIER: {"name": _("Grand Marnier"),},
        ICING_LEMON: {"name": _("Lemon"),},
    }
    ICINGS = django_choice_options(ICING_OPTIONS, "name")
    DEFAULT_ICINGS = (
        ICING_WHITE,
        ICING_MOCHA,
        ICING_CHOCOLATE,
        ICING_WHIP_CREAM,
    )

    # TODO category

    name = models.CharField(max_length=256, unique=True)
    description = HTMLField(null=True, blank=True)
    tags = TaggableManager(blank=True)
    image = models.ForeignKey(Image, related_name="cakes", on_delete=PROTECT)

    allowed_sizes = MultiSelectField(
        choices=SIZES,
        default=DEFAULT_SIZES,
        blank=True,
        help_text=_("Leave empty if none are applicable"),
        max_length=2048,
    )
    allowed_types = MultiSelectField(
        choices=TYPES,
        default=DEFAULT_TYPES,
        blank=True,
        help_text=_("Leave empty if none are applicable"),
        max_length=2048,
    )
    allowed_fillings = MultiSelectField(
        choices=FILLINGS,
        default=DEFAULT_FILLINGS,
        blank=True,
        help_text=_("Leave empty if none are applicable"),
        max_length=2048,
    )
    allowed_icings = MultiSelectField(
        choices=ICINGS,
        default=DEFAULT_ICINGS,
        blank=True,
        help_text=_("Leave empty if none are applicable"),
        max_length=2048,
    )

    objects = AppManager.from_queryset(CakeProductQuerySet)()

    def calculate_rating(self):
        # TODO set
        self.rating = 1

    def _clean(self):
        self.title_field("name")  # Capitalize cake name

    def clean(self):
        self._clean()
        super().clean()

    def save(self, *args, **kwargs):
        self._clean()
        super().save(*args, **kwargs)

    def image_url(self, *args, **kwargs):
        return Image.get_image_url(self.image, *args, **kwargs)

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.name

    class Meta:
        verbose_name = _("cake")
        ordering = ("name",)
