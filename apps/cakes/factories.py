import factory

from cakes.models import CakeProduct
from images.factories import ImageFactory
from main.factories import BaseModelFactory


class CakeProductFactory(BaseModelFactory):
    name = factory.Sequence(lambda n: u"Baker Product %s" % n)
    image = factory.SubFactory(ImageFactory)

    class Meta:
        model = CakeProduct
