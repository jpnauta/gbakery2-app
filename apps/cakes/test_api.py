from cakes.factories import CakeProductFactory
from main.testing.assertions import assert_statuses_are_met
from main.tests import BaseAPITestCase


class PublicCakeProductViewSetTestCase(BaseAPITestCase):
    base_url = "/api/public/cakes/"

    def test_permissions__all__are_enforced(self):
        assert_statuses_are_met(
            [
                ["GET", self.get_list_url(), 200],
                ["GET", self.get_obj_url(9999), 404],
                ["POST", self.get_list_url(), 405],
                ["PUT", self.get_obj_url(9999), 405],
                ["DELETE", self.get_obj_url(9999), 405],
            ],
            self,
        )

    def test_list__search_by_cake_id__returns_only_one_product(self):
        cake1 = CakeProductFactory()
        cake2 = CakeProductFactory(name="%s" % cake1.id)

        results = self.make_request(
            "GET", self.get_list_url(), query_params=dict(q=cake1.id)
        ).json()

        self.assertPaginationResultsMatch(results, [cake1.id])

    def test_list__basic_text_search__is_handled(self):
        cake1 = CakeProductFactory(name="find me")
        cake2 = CakeProductFactory(name="filtered out")

        results = self.make_request(
            "GET", self.get_list_url(), query_params=dict(q="find")
        ).json()

        self.assertPaginationResultsMatch(results, [cake1.id])

    def test_list__search_by_tag__is_handled(self):
        cake1 = CakeProductFactory()
        cake1.tags.add("mytag")
        cake2 = CakeProductFactory()

        results = self.make_request(
            "GET", self.get_list_url(), query_params=dict(q="mytag")
        ).json()

        self.assertPaginationResultsMatch(results, [cake1.id])
