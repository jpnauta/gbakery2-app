import factory
from main.factories import BaseModelFactory
from users.models import User


class UserModelFactory(BaseModelFactory):
    email = factory.Sequence(lambda n: "user%s@test.com" % n)
    password = u"password"

    @classmethod
    def _prepare(cls, create, **kwargs):
        password = kwargs.pop("password", None)
        user = super(UserModelFactory, cls)._prepare(create, **kwargs)

        # Hash any password set
        if password:
            user.set_password(password)
            if create:
                user.save()

        return user

    class Meta:
        model = User
