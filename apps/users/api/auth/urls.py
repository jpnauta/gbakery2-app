from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from users.api.auth.routers import auth_router

urlpatterns = (
    path("", include(auth_router.urls)),
    path("", include("dj_rest_auth.urls")),
    path("token/refresh/", TokenObtainPairView.as_view()),
    path("token/verify/", TokenRefreshView.as_view()),
)
