from rest_framework.routers import SimpleRouter

from users.api.auth.viewsets import AuthProfileViewSet

auth_router = SimpleRouter()

auth_router.register(r"profile", AuthProfileViewSet, basename="auth_profile")
