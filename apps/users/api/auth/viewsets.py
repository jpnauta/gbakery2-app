from rest_framework.response import Response

from main.serializers.viewsets import AppGenericViewSet
from users.api.auth.serializers import AuthProfileSerializer
from users.models import User


class AuthProfileViewSet(AppGenericViewSet):
    model = User
    serializer_class = AuthProfileSerializer

    def get_queryset(self):
        # Only allow access to the user's own profile
        return User.objects.filter(id=self.request.user)

    def get_object(self, queryset=None):
        return self.request.user

    def list(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response(serializer.data)
