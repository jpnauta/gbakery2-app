from dj_rest_auth.serializers import UserDetailsSerializer

from main.serializers import AppModelSerializer
from users.models import User


class AppUserDetailsSerializer(UserDetailsSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "email",
        )  # Don't serialize first name, last name
        read_only_fields = ("email",)


class AuthProfileSerializer(AppModelSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["email"].read_only = True

    class Meta:
        model = User
        fields = ("id", "email")
