from django.contrib.auth import admin as user_admin
from main.admin import BaseModelAdmin
from users.admin.forms import UserChangeForm, UserCreationForm

from users.models import User


class UserAdmin(user_admin.UserAdmin, BaseModelAdmin):
    model = User

    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = (
        "email",
        "is_superuser",
        "is_active",
        "last_login",
        "created",
        "updated",
    )
    list_filter = ("is_superuser", "updated")
    fieldsets = (
        (None, {"fields": ("email", "password",)}),
        ("Permissions", {"fields": ("is_superuser", "is_active",)}),
        ("Important dates", {"fields": ("last_login",)}),
    )
    add_fieldsets = (
        (None, {"classes": ("wide",), "fields": ("email", "password1", "password2",)}),
        ("Permissions", {"fields": ("is_superuser", "is_active",)}),
    )
    search_fields = ("email",)
    ordering = ("email",)
    filter_horizontal = ()


UserAdmin.register()
