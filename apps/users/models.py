from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models

from main.ext.dates.utils import utc_now
from .managers import UserManager

# Create your models here.
from main.models import DoubleUUID, BaseModel, Slugged, Activated


class User(
    AbstractBaseUser, PermissionsMixin, BaseModel, Slugged, DoubleUUID, Activated
):
    email = models.EmailField(max_length=128, unique=True)
    is_staff = models.BooleanField(default=True)

    USERNAME_FIELD = "email"
    slug_field = "email"

    objects = UserManager()

    @property
    def username(self):
        return self.email

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.email or ""

    def save(self, *args, **kwargs):
        if self.last_login is None:
            self.last_login = utc_now()
        return super().save(*args, **kwargs)

    class Meta:
        ordering = [
            "email",
        ]
