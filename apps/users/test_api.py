from __future__ import unicode_literals

from main.tests import BaseAPITestCase
from users.factories import UserModelFactory


class RestAuthTestCase(BaseAPITestCase):
    base_url = "/api/auth/"
    login_url = base_url + "login/"
    logout_url = base_url + "logout/"
    user_url = base_url + "user/"
    token_refresh_url = base_url + "token/refresh/"
    token_verify_url = base_url + "token/verify/"

    password = "testPassword"

    def test_login__basic_user__is_permitted(self):
        # GIVEN
        user = self.build_user_with_password(self.password)

        # WHEN
        response = self.make_request(
            "POST", self.login_url, {"email": user.email, "password": self.password}
        ).json()

        # THEN
        self.assertIsNotNone(response["access_token"])
        self.assertDictContainsSubset(
            dict(id=user.id, username=user.email, email=user.email,), response["user"]
        )

    def test_login__invalid_password__is_rejected(self):
        # GIVEN
        user = UserModelFactory()

        # WHEN
        response = self.make_request(
            "POST",
            self.login_url,
            {"email": user.email, "password": "invalid"},
            permitted_codes=[400],
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(detail="Unable to log in with provided credentials.", field=None,),
            response,
        )

    def test_login__get_request__is_rejected(self):
        # WHEN
        response = self.make_request(
            "GET", self.login_url, permitted_codes=[405],
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(detail='Method "GET" not allowed.',), response
        )

    def test_logout__basic_user__is_permitted(self):
        # GIVEN
        self.login_basic_user()

        # WHEN
        self.make_request(
            "POST", self.logout_url, data={}, permitted_codes=[200],
        )

    def test_logout__not_logged_in__is_permitted(self):
        # GIVEN
        self.logout()

        # WHEN
        self.make_request(
            "GET", self.logout_url, permitted_codes=[200],
        )

    def test_token_refresh__valid_token__is_permitted(self):
        # GIVEN
        user = self.build_user_with_password(self.password)

        # WHEN
        r1 = self.make_request(
            "POST", self.login_url, {"email": user.email, "password": self.password}
        ).json()
        r2 = self.make_request(
            "POST", self.token_refresh_url, {"refresh": r1["refresh_token"]}
        ).json()

        self.assertEqual(list(r2.keys()), ["access"])

    def test_token_verify__valid_token__is_permitted(self):
        # GIVEN
        user = self.build_user_with_password(self.password)

        # WHEN
        r1 = self.make_request(
            "POST", self.login_url, {"email": user.email, "password": self.password}
        ).json()
        r2 = self.make_request(
            "POST", self.token_verify_url, {"token": r1["access_token"]}
        ).json()

        # THEN
        self.assertEqual(r2, dict())

    def test_get_user__logged_in__is_accepted(self):
        # GIVEN
        user = self.login_basic_user()

        # WHEN
        response = self.make_request("GET", self.user_url).json()

        # THEN
        self.assertDictContainsSubset(
            dict(id=user.id, username=user.email, email=user.email,), response
        )
