from django.db.models import Sum
from rest_framework.fields import SerializerMethodField
from main.serializers import DateRequiredModelSerializer
from orders.models import OrderProduct
from products.models import (
    BakerProduct,
    FrontProduct,
    ProductCategory,
    QUANTITY_TYPE_OPTIONS,
)


class BaseReportSerializer(DateRequiredModelSerializer):
    """
    All report serializers require the date to report for
    """

    def _quantity_for_front_product(self, product):
        return OrderProduct.objects.all().quantity_for_date_and_front_product(
            self.date, product
        )

    def _quantities_by_type_for_front_product(self, product):
        quantities = {}
        for qt in QUANTITY_TYPE_OPTIONS.keys():
            quantities[qt] = OrderProduct.objects.filter(
                quantity_type=qt
            ).quantity_for_date_and_front_product(self.date, product)

        return quantities


class DashFrontProductReportSerializer(BaseReportSerializer):
    quantity = SerializerMethodField()
    quantities_by_type = SerializerMethodField()
    quantities_by_order = SerializerMethodField()

    def get_quantity(self, product):
        return self._quantity_for_front_product(product)

    def get_quantities_by_type(self, product):
        return self._quantities_by_type_for_front_product(product)

    def get_quantities_by_order(self, product):
        return list(
            product.order_products.values("order", "order__name")
            .for_order_date(self.date)
            .annotate(total_quantity=Sum("total_quantity"))
            .order_by("-total_quantity")
        )

    class Meta:
        model = FrontProduct
        fields = (
            "id",
            "name",
            "slug",
            "quantity",
            "quantities_by_type",
            "quantity_type",
            "is_starred",
            "quantities_by_order",
        )


class DashBakerProductReportSerializer(BaseReportSerializer):
    quantity = SerializerMethodField()

    def get_quantity(self, back_product):
        quantity_sum = 0.0

        for fp_bp in back_product.front_product_baker_products.all():
            ratio = fp_bp.ratio
            quantity_sum += ratio * self._quantity_for_front_product(
                fp_bp.front_product
            )

        return quantity_sum

    class Meta:
        model = BakerProduct
        fields = (
            "id",
            "name",
            "slug",
            "quantity",
            "quantity_type",
            "is_starred",
        )


class DashProductCategoryReportSerializer(BaseReportSerializer):
    all_front_products = SerializerMethodField()
    all_baker_products = SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_all_front_products(self, category):
        qs = (
            category.all_front_products.all()
            .order_by("category", "name")
            .for_order_date(self.date)
            .prefetch_related("prices")
        )
        return DashFrontProductReportSerializer(
            date=self.date, instance=qs, many=True
        ).data

    def get_all_baker_products(self, category):
        qs = (
            category.all_baker_products.all()
            .order_by("category", "name")
            .for_order_date(self.date)
            .prefetch_related("front_product_baker_products__front_product")
        )
        return DashBakerProductReportSerializer(
            date=self.date, instance=qs, many=True
        ).data

    class Meta:
        model = ProductCategory
        fields = (
            "id",
            "name",
            "slug",
            "all_front_products",
            "all_baker_products",
            "product_supplier_message",
        )
