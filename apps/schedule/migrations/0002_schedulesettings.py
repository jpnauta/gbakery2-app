# Generated by Django 2.2.16 on 2021-10-31 22:55

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScheduleSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('_singleton', models.BooleanField(default=True, editable=False, unique=True)),
                ('num_business_days', models.IntegerField(default=2, help_text="Determines when to close online orders. Default is 2; if you change this, don't forget to change it back later!", validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(6)], verbose_name='Number of business days')),
                ('order_cutoff_time', models.TimeField(default=datetime.time(16, 0), help_text="Determines when to transition to the next business day, in 24 hour time. Default is 16:00:00 (4pm); if you change this, don't forget to change it back later!", validators=[django.core.validators.MinValueValidator(datetime.time(8, 0))], verbose_name='Order cutoff time')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
