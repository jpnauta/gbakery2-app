from datetime import time

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import TextField, DateField, IntegerField, TimeField
from django.utils.translation import ugettext as _

from main.models import BaseModel, SingletonModel


class ClosingRule(BaseModel):
    start = DateField(_("Start"))
    end = DateField(_("End"))
    reason = TextField(_("Reason"))

    def __str__(self):
        return _("closed from %(start)s to %(end)s due to %(reason)s") % {
            "start": str(self.start),
            "end": str(self.end),
            "reason": self.reason,
        }


class ScheduleSettings(SingletonModel):
    NUM_BUSINESS_DAYS_DEFAULT = 2
    ORDER_CUTOFF_TIME_DEFAULT = time(16, 0)
    ORDER_EARLIEST_TIME_DEFAULT = time(9, 0)
    ORDER_LATEST_TIME_DEFAULT = time(17, 0)

    num_business_days = IntegerField(
        _("Number of business days"),
        help_text=_(
            f"Determines when to close online orders. Default is {NUM_BUSINESS_DAYS_DEFAULT}; if you "
            f"change this, don't forget to change it back later!"
        ),
        validators=[
            MinValueValidator(1),
            MaxValueValidator(6),
        ],
        default=NUM_BUSINESS_DAYS_DEFAULT,
    )
    order_cutoff_time = TimeField(
        _("Order cutoff time"),
        help_text=_(
            f"Determines when to transition to the next business day. Default "
            f"is {ORDER_CUTOFF_TIME_DEFAULT}; if you change this, don't forget to change it back later!"
        ),
        validators=[
            MinValueValidator(time(8)),
        ],
        default=ORDER_CUTOFF_TIME_DEFAULT,
    )
    order_earliest_time = TimeField(
        _("Earliest order time"),
        help_text=_(
            f"Determines the earliest time orders can be placed. Default "
            f"is {ORDER_EARLIEST_TIME_DEFAULT}; if you change this, don't forget to change it back later!"
        ),
        validators=[
            MinValueValidator(time(0)),
        ],
        default=ORDER_EARLIEST_TIME_DEFAULT,
    )
    order_latest_time = TimeField(
        _("Latest order time"),
        help_text=_(
            f"Determines the latest time orders can be placed. Default "
            f"is {ORDER_LATEST_TIME_DEFAULT}; if you change this, don't forget to change it back later!"
        ),
        validators=[
            MinValueValidator(time(8)),
        ],
        default=ORDER_LATEST_TIME_DEFAULT,
    )

    def __str__(self):
        return _("Global settings")

    class Meta:
        verbose_name_plural = _("Schedule settings")
