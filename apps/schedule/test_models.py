from datetime import time

from django.core.exceptions import ValidationError
from django.db import IntegrityError

from main.tests import BaseTestCase
from schedule.models import ScheduleSettings


class ScheduleSettingsTestCase(BaseTestCase):
    def setUp(self):
        ScheduleSettings.objects.all().delete()

    def test_created_once(self):
        settings, created = ScheduleSettings.objects.get_or_create()
        self.assertEqual(created, True)
        self.assertEqual(settings.num_business_days, 2)

        settings, created = ScheduleSettings.objects.get_or_create()
        self.assertEqual(created, False)

    def test_singleton(self):
        ScheduleSettings.objects.create()
        with self.assertRaises(IntegrityError):
            ScheduleSettings.objects.create()

    def test_num_business_days_validators(self):
        with self.assertRaises(ValidationError):
            ScheduleSettings(num_business_days=0).full_clean()
        ScheduleSettings(num_business_days=1).full_clean()
        ScheduleSettings(num_business_days=6).full_clean()
        with self.assertRaises(ValidationError):
            ScheduleSettings(num_business_days=7).full_clean()

    def test_order_cutoff_time_validators(self):
        with self.assertRaises(ValidationError):
            ScheduleSettings(order_cutoff_time=time(7)).full_clean()
        ScheduleSettings(order_cutoff_time=time(8)).full_clean()
        ScheduleSettings(order_cutoff_time=time(23)).full_clean()
