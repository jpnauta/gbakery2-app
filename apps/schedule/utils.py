from datetime import date

from schedule.models import ClosingRule


def get_closing_rule_for(d: date) -> ClosingRule:
    """
    Returns QuerySet of ClosingRules that are currently valid
    """
    return ClosingRule.objects.filter(start__lte=d, end__gte=d).first()
