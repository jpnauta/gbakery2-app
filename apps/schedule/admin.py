# -*- coding: utf-8 -*-
from django.contrib import admin

from main.admin import BaseModelAdmin
from schedule.models import ClosingRule, ScheduleSettings


class ClosingRuleAdmin(BaseModelAdmin):
    model = ClosingRule


class ScheduleSettingsAdmin(BaseModelAdmin):
    model = ScheduleSettings


admin.site.register(ClosingRule, ClosingRuleAdmin)
admin.site.register(ScheduleSettings, ScheduleSettingsAdmin)
