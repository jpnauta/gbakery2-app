from datetime import timedelta, date

from main.tests import BaseTestCase
from schedule.models import ClosingRule
from schedule.utils import get_closing_rule_for


class UtilsTestCase(BaseTestCase):
    def test_get_closing_rule_for_now(self):
        d = date(2015, 12, 25)
        closing_rule = ClosingRule.objects.create(
            start=date(2015, 12, 25) - timedelta(days=2),
            end=date(2015, 12, 25) + timedelta(days=2),
            reason="Public holiday",
        )
        self.assertIsNone(get_closing_rule_for(d - timedelta(days=3)))
        self.assertEqual(get_closing_rule_for(d - timedelta(days=2)), closing_rule)
        self.assertEqual(get_closing_rule_for(d + timedelta(days=2)), closing_rule)
        self.assertIsNone(get_closing_rule_for(d + timedelta(days=3)))
