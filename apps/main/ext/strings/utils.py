def underscore_to_title(word):
    result = " ".join(char.capitalize() for char in word.split("_"))
    return result.title()


def title(string):
    if string is not None:
        return string.title()


def clean_whitespace(string):
    return u" ".join(string.split())


def parse_int(s):
    try:
        return int(s)
    except (TypeError, ValueError):
        pass
    return None
