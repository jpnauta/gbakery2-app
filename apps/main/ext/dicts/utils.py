from django.utils.encoding import force_text


def merge(one: dict, two: dict):
    d = dict(one)

    for k, v in two.items():
        d[k] = v

    return d


def deep_merge(src, dest):
    if dest is None:
        dest = {}

    if src is None:
        src = {}

    for key, value in src.items():
        if isinstance(value, dict):
            node = dest.setdefault(key, {})
            deep_merge(value, node)
        elif isinstance(value, list):
            dest.setdefault(key, [])
            existing = dest.get(key)

            for item in value:
                if item not in existing:
                    existing.append(item)
        else:
            dest[key] = value

    return dest


def shallow_merge(src, dest):
    for key, item in dest.items():
        src[key] = item


def rename_key(dictionary, old, new, default=None):
    """
    Renames a dictionary key to another value
    :param dictionary: The dictionary to change
    :param old: The name of the old key
    :param new: The name of the new key
    """
    val = dictionary.pop(old, default)
    dictionary[new] = val


def encoded_dict(map):
    return dict([(key, force_text(val).encode("utf-8")) for key, val in map.items()])
