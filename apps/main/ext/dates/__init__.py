from pytz import timezone

from settings import TIME_ZONE

TIME_ZONE_PYTZ = timezone(TIME_ZONE)
"""
Local time in MST/MDT
"""
