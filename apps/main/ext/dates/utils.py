import re
import typing
from datetime import date, datetime
from datetime import time

from django.utils import timezone
from django.utils.dateparse import parse_datetime
from pytz import UTC, tzinfo

from main.ext.dates import TIME_ZONE_PYTZ


def localize(dt: datetime, tz: tzinfo) -> datetime:
    if dt and tz:
        dt = tz.localize(dt)
    return dt


def to_timezone(dt: datetime, tz: tzinfo) -> datetime:
    if dt and tz:
        # Localize, if needed
        if not dt.tzinfo:
            dt = localize(dt, UTC)

        # Convert to local time
        dt = dt.astimezone(tz)
    return dt


def utc_now() -> datetime:
    return timezone.now()


def local_tz_now() -> datetime:
    """
    Same as `datetime(...)`, but localized to MST/MDT
    """
    dt = timezone.now()
    return dt.astimezone(TIME_ZONE_PYTZ)


def date_combine(dt, t):
    """Acts the same as dt.combine, but preserves tzinfo"""
    new_dt = datetime.combine(dt, t)

    if isinstance(dt, datetime):  # Localize dt objects (not dates)
        new_dt = localize(new_dt, dt.tzinfo)

    return new_dt


def start_of_date(dt: datetime) -> datetime:
    if dt:
        return date_combine(dt, time.min)


def end_of_date(dt: datetime) -> datetime:
    if dt:
        return date_combine(dt, time.max)


def date_range(dt: datetime) -> typing.Tuple[datetime, datetime]:
    """
    Retrieves the date range for the given date

    example: if the date is today at 8:00pm, it will return (today 0:00, today 23:59)
    in the same timezone
    """
    return start_of_date(dt), end_of_date(dt)


def strip_dt_quotations(string):
    """
    Removes the leading & trailing quotations that appear when a GET request
    receives an ISO
    """
    if string:
        return re.sub('"', "", string)


def parse_dt(string: str) -> datetime:
    if string:
        string = strip_dt_quotations(string)
        return parse_datetime(string)


def date_to_datetime(
    d: date,
    hour: typing.Optional[int] = 0,
    minute: typing.Optional[int] = 0,
    second: typing.Optional[int] = 0,
) -> datetime:
    """
    Converts a date object into a datetime object
    :param d: date object
    :param hour: hour
    :param minute: minute
    :param second: second
    :return: combined datetime
    """
    return datetime(d.year, d.month, d.day, hour, minute, second)


def local_datetime(*args, **kwargs) -> datetime:
    """
    Same as `datetime(...)`, but localized to MST/MDT
    """
    dt = datetime(*args, **kwargs)
    return TIME_ZONE_PYTZ.localize(dt)
