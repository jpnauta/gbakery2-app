from django.db.models.query import QuerySet


def get_first(iterable, default=None):
    """
    Retrieves the first item in the given iterable/list/generator/etc.
    """
    # If Django queryset, only load first element (reduces load on DB)
    if isinstance(iterable, QuerySet):
        iterable = iterable[:1]

    if iterable:
        for item in iterable:
            return item
    return default
