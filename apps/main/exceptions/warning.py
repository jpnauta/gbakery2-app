import warnings


class AppDeprecationWarning(PendingDeprecationWarning):
    """
    Indicates that code will soon be depreciated.

    This warning will cause an exception on all systems EXCEPT live!
    """

    pass


class AppPendingDeprecationWarning(PendingDeprecationWarning):
    """
    Indicates that code will soon be depreciated.
    """

    pass


class AppRuntimeWarning(RuntimeWarning):
    pass


def deprecation_warning(msg):
    """
    Indicates that code will soon be depreciated.

    This warning will cause an exception on all systems EXCEPT live!
    """
    warnings.warn(msg, AppDeprecationWarning)


def pending_deprecation_warning(msg):
    """
    Indicates that code will soon be depreciated.
    """
    warnings.warn(msg, AppPendingDeprecationWarning)


def runtime_warning(msg):
    warnings.warn(msg, AppRuntimeWarning)
