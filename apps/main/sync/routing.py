from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import url

from orders.api.dashboard.consumers import DashboardOrderConsumer

application = ProtocolTypeRouter(
    {"websocket": URLRouter([url(r"dashboard/orders", DashboardOrderConsumer),]),}
)
