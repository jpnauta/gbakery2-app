from __future__ import unicode_literals
from main.caching.keys import BaseCacheKey


class LastSignalEnqueuedKey(BaseCacheKey):
    cache_prefix = "last_signal_enqueued"

    def __init__(self, identifier):
        self.identifier = identifier

    def get_key(self):
        return self.identifier

    def get_timeout(self):
        return 60 * 1000  # 1 minute
