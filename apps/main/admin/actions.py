from django.contrib import messages


def make_public(modeladmin, request, queryset):
    queryset.update(is_public=True)

    messages.success(request, "Success! Selected items will be visible to customers")


make_public.short_description = "Allow customers to view selected"


def make_private(modeladmin, request, queryset):
    queryset.update(is_public=False)

    messages.success(request, "Success! Selected items will be hidden from customers")


make_private.short_description = "Hide selected from customers"
