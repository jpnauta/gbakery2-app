from django.conf.urls import include

from django.contrib import admin
from django.urls import path

admin.autodiscover()

urlpatterns = (
    # Admin urls
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path("admin/", admin.site.urls),
    path("tinymce/", include("tinymce.urls")),
    path("taggit_autosuggest/", include("taggit_autosuggest.urls")),
)
