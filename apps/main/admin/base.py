from allauth.account.models import EmailAddress
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.urls import reverse
from django_celery_beat.models import (
    ClockedSchedule,
    SolarSchedule,
    IntervalSchedule,
    CrontabSchedule,
)
from rest_framework.authtoken.models import TokenProxy
from taggit.models import Tag

import settings


def object_link(func, name=None):
    """
    Wrapper to link a foreign key field to the edit page of that foreign key
    """

    def wrapper(self, obj):
        ct = func(self, obj)
        print(self, obj, ct, ct._meta)
        url = reverse(
            "admin:%s_%s_change"
            % (ct._meta.app_label, ct._meta.model.__name__.lower()),
            args=(ct.id,),
        )
        return '<a href="%s">%s</a>' % (url, ct)

    wrapper.allow_tags = True
    wrapper.__name__ = name or func.__name__

    return wrapper


class BaseModelAdmin(admin.ModelAdmin):
    model = None  # To be set by subclasses
    list_select_related = True
    actions_on_bottom = False
    actions_on_top = True

    @classmethod
    def register(cls):
        try:
            admin.site.register(cls.model, cls)
        except AlreadyRegistered:
            # If already registered and running tests, ignore
            if not settings.TEST_MODE:
                raise


class BaseTabularInline(admin.TabularInline):
    pass


# Flag used to check if task has been ran. Mostly used for unit tests.
_deregister_unnecessary_admin_pages_complete = False


def deregister_unnecessary_admin_pages():
    """
    Removes unnecessary pages from the Admin home page
    """
    global _deregister_unnecessary_admin_pages_complete
    if not _deregister_unnecessary_admin_pages_complete:
        admin.site.unregister(Site)
        admin.site.unregister(Tag)
        admin.site.unregister(Group)
        admin.site.unregister(EmailAddress)
        admin.site.unregister(TokenProxy)
        admin.site.unregister(ClockedSchedule)
        admin.site.unregister(SolarSchedule)
        admin.site.unregister(IntervalSchedule)
        admin.site.unregister(CrontabSchedule)
    _deregister_unnecessary_admin_pages_complete = True
