from __future__ import absolute_import

from urllib.parse import urljoin

from django.conf import settings
from storages.backends.gcloud import GoogleCloudStorage
from storages.backends.s3boto3 import S3Boto3Storage
from storages.backends.sftpstorage import SFTPStorage
from storages.utils import setting

import settings


class BaseSFTPStorage(SFTPStorage):
    def __init__(self, host=None, *args, **kwargs):
        # Hack fix until this commit is released:
        # https://github.com/jschneier/django-storages/commit/ac1e1a5900cdd735b79fe33ef3b22e60bf5f8b8e
        host = host or setting("SFTP_STORAGE_HOST")
        super().__init__(host=host, *args, **kwargs)


class StaticSFTPStorage(BaseSFTPStorage):
    def __init__(self, *args, **kwargs):
        kwargs["root_path"] = "/upload/static/"
        kwargs["base_url"] = settings.STATIC_URL
        super().__init__(*args, **kwargs)


class MediaSFTPStorage(BaseSFTPStorage):
    def __init__(self, *args, **kwargs):
        kwargs["root_path"] = "/upload/media/"
        kwargs["base_url"] = settings.MEDIA_URL
        super().__init__(*args, **kwargs)


class AppMinioMediaStorage(S3Boto3Storage):
    bucket_name = "media"


class AppMinioStaticStorage(S3Boto3Storage):
    bucket_name = "static"


class AppGoogleCloudStorage(GoogleCloudStorage):
    bucket_name = None  # Defined by subclasses
    bucket_url = None  # Defined by subclasses

    def get_default_settings(self):
        settings = super().get_default_settings()
        settings["bucket_name"] = self.bucket_name
        return settings

    def url(self, name):
        return urljoin(self.bucket_url, name)


class GoogleCloudMediaStorage(AppGoogleCloudStorage):
    bucket_name = setting("GS_MEDIA_BUCKET_NAME")
    bucket_url = settings.MEDIA_URL


class GoogleCloudStaticStorage(AppGoogleCloudStorage):
    bucket_name = setting("GS_STATIC_BUCKET_NAME")
    bucket_url = settings.STATIC_URL
