from django.db.models.signals import post_save, post_delete

handlers = []


def signal_wrapper(handler):
    handlers.append(
        handler
    )  # Store reference to handler (so it's not destroyed by garbage collector)
    return handler


def generic_signal_wrapper(func):
    @signal_wrapper
    def handler(instance, **kwargs):
        func(instance, **kwargs)

    return handler


def model_created_signal_wrapper(func):
    @signal_wrapper
    def handler(instance, created, **kwargs):
        if created:
            func(instance, **kwargs)

    return handler


def call_after_create(func, klass):
    return post_save.connect(receiver=model_created_signal_wrapper(func), sender=klass)


def call_after_delete(func, klass):
    return post_delete.connect(receiver=generic_signal_wrapper(func), sender=klass)


def call_after_save(func, klass):
    return post_save.connect(receiver=generic_signal_wrapper(func), sender=klass)
