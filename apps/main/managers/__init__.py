from django.db.models.manager import BaseManager
from .querysets import *


class AppManager(BaseManager):
    def __iter__(self):
        return self.all().__iter__()
