from datetime import timedelta
from django.db.models.query import QuerySet
from main.models.aggregates import SumWithDefault
from main.ext.dates.utils import utc_now


class BaseQuerySet(QuerySet):
    def regular_sum(self, **kwargs):
        """
        Shortcut method for performing field sum aggregates

        qs.regular_sum(name='field')['name'] => sum of 'field' attribute
        qs.regular_sum(name1='field1', name2='field2')

        :return: QuerySet with corresponding aggregates
        """
        aggregates = ({key: SumWithDefault(kwargs[key], default=0)} for key in kwargs)

        qs = self
        for aggregate in aggregates:
            qs = qs.aggregate(**aggregate)
        return qs

    def last_30_days(self):
        """Retrieves objects created in the last ~30 days"""
        last_month = (utc_now() - timedelta(days=30)).date()
        return self.filter(created__gte=last_month)

    def last_created(self, index=0):
        """
        Retrieves the last create model object in the database
        """
        return self.order_by("-created")[index]

    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None


class ActivatedQuerySet(QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def inactive(self):
        return self.filter(is_active=False)


class PublicPrivateQuerySet(QuerySet):
    def public(self):
        return self.filter(is_public=True)

    def private(self):
        return self.filter(is_public=False)


class RatedQuerySet(QuerySet):
    def most_popular(self):
        return self.order_by("rating")
