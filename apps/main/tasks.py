from main.celery import app
from django.core.management import call_command

from settings import DB_BACKUPS_TASK_ENABLED


@app.task
def perform_db_backup():
    """
    Exports a snapshot of the current database to the backups S3 bucket
    """
    if DB_BACKUPS_TASK_ENABLED:
        call_command("db_dump")
        call_command("db_upload")
