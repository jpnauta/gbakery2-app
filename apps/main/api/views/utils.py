from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import exception_handler
from stripe.error import StripeError

from main.api.errors.handlers import DRFApiErrorHandler
from main.logging import logger


def django_errors_to_app_errors(errors, request):
    error_handler = DRFApiErrorHandler(errors, request=request)
    return error_handler.get_data()


def api_exception_handler(exc, context):
    """
    Exception handler for all DRF exceptions
    """
    response = exception_handler(exc, context)  # Call DRF's default exception handler

    if response is not None:
        if isinstance(exc, ValidationError):
            response.data = django_errors_to_app_errors(
                response.data, request=context["request"]
            )

    # Handle uncaught stripe errors
    if isinstance(exc, StripeError):
        logger.exception(exc)
        data = django_errors_to_app_errors(
            f"Unexpected error while communicating with Stripe: "
            f'"{exc.user_message}"',
            request=context["request"],
        )
        response = Response(data, status=400)

    return response
