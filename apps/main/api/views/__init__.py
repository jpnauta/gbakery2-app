from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response


# Simple view to check if client can connect to server
@api_view(["GET"])
@permission_classes((AllowAny,))
def connection_test(request):
    return Response({})
