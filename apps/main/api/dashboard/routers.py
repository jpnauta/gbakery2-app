from rest_framework.routers import SimpleRouter

from orders.api.dashboard.viewsets import (
    DashboardOrderViewSet,
    DashboardOrderFlagViewSet,
    DashboardOrderLocationViewSet,
    DashboardOrderProductViewSet,
)

from products.api.dashboard.viewsets import (
    DashFrontProductNotReadyViewSet,
    DashFrontProductViewSet,
    DashBakerProductViewSet,
    DashProductCategoryViewSet,
)

dashboard_router = SimpleRouter()

# orders module
dashboard_router.register(r"orders/flags", DashboardOrderFlagViewSet)
dashboard_router.register(r"orders/locations", DashboardOrderLocationViewSet)
dashboard_router.register(r"orders/products", DashboardOrderProductViewSet)
dashboard_router.register(r"orders", DashboardOrderViewSet)
dashboard_router.register(r"products/categories", DashProductCategoryViewSet)
dashboard_router.register(r"products/not-ready", DashFrontProductNotReadyViewSet)
dashboard_router.register(r"products", DashFrontProductViewSet)
dashboard_router.register(r"baker-products", DashBakerProductViewSet)
