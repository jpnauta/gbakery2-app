from django.conf.urls import url, include

from main.api.dashboard.routers import dashboard_router

urlpatterns = (url(r"^", include(dashboard_router.urls)),)
