from django.urls import path, include

from main.api.views import connection_test

urlpatterns = (
    path("auth/", include("users.api.auth.urls")),
    path("dashboard/", include("main.api.dashboard.urls")),
    path("public/", include("main.api.public.urls")),
    path("connection-test/", connection_test, name="connection_test"),
)
