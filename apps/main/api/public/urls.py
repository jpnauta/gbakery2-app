from django.conf.urls import url, include

from main.api.public.routers import public_router

urlpatterns = (url(r"^", include(public_router.urls)),)
