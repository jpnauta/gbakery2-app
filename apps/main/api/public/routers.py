from rest_framework.routers import SimpleRouter

from cakes.api.frontend.viewsets import PublicCakeProductViewSet
from orders.api.public.viewsets import PublicOrderViewSet
from payments.api.public.viewsets import StripeWebhooksViewSet
from products.api.public.viewsets import PublicProductCategoryViewSet

public_router = SimpleRouter()

# products module
public_router.register(r"products/categories", PublicProductCategoryViewSet)

# cakes module
public_router.register(r"cakes", PublicCakeProductViewSet)

# orders module
public_router.register(r"orders", PublicOrderViewSet)

public_router.register(r"stripe/webhooks", StripeWebhooksViewSet)
