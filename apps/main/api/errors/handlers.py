from abc import abstractmethod
from rest_framework import status
from rest_framework.response import Response
from main.ext.lists.utils import get_first
from main.api.errors.utils import get_error_field_name, GENERIC_ERROR_KEYS


class ErrorInfo(object):
    """
    Contains metadata about a single error found in an error list/dict object
    """

    def __init__(self, error_str, field, field_name):
        self.error_str = error_str
        self.field = field
        self.field_name = field_name

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        if self.field_name:
            return "%s: %s" % (self.field_name, self.error_str)
        return self.error_str


class BaseApiErrorHandler(object):
    """
    Error handler for JSON API error responses
    """

    default_status_code = status.HTTP_400_BAD_REQUEST
    request_required = False  # Ensure that the request object is given
    response_class = Response
    error_info_class = ErrorInfo

    field_name_mappings = {}

    _error_list = None

    def __init__(self, request=None, status_code=None, error_code=None):
        # Ensure response object is given
        if self.request_required and request is None:
            raise RuntimeError("request object not given")

        self.request = request
        self.status_code = (
            status_code if status_code is not None else self.default_status_code
        )
        self.error_code = error_code

    @abstractmethod
    def get_error_object(self):
        """
        :return: the object's ErrorDict/ErrorList object containing the errors that have occurred
        """
        raise NotImplementedError("defined by subclasses")

    def get_field_name(self, field):
        """
        Converts the given field key into a readable field name
        :param field: the key to convert
        :return: a human-readable name for the field
        """
        return get_error_field_name(field, self.field_name_mappings)

    def get_status_code(self):
        """
        Determines the status code to use for the error handler's response
        :return:
        """
        return self.status_code

    def _clean_errors(self, obj, _field=None):
        if isinstance(obj, dict):
            for _subfield, sub_obj in obj.items():
                f = _subfield if isinstance(_subfield, str) else _field
                for info in self._clean_errors(sub_obj, _field=f):
                    yield info
        elif isinstance(obj, list):
            for sub_obj in obj:
                for info in self._clean_errors(sub_obj, _field=_field):
                    yield info
        else:
            if _field in GENERIC_ERROR_KEYS:  # If generic key found, dispose of it
                _field = None
            yield self.error_info_class(obj, _field, self.get_field_name(_field))

    def clean_errors(self):
        """
        Finds and cleans all errors found
        """
        return self._clean_errors(self.get_error_object())

    @property
    def error_list(self):
        """
        Accessor for list of errors at runtime. To add errors to this list, see get_error_list.
        """
        if self._error_list is None:
            self._error_list = list(self.clean_errors())
        return self._error_list

    def get_detail(self):
        return get_first(self.error_list)

    def get_field(self):
        first = get_first(self.error_list)
        if first:
            return first.field

    def get_readable_error_list(self):
        """
        Creates a list of strings, where each string contains a single error message
        """
        return list(map(lambda o: repr(o), self.error_list))

    def get_error_code(self):
        return self.error_code

    def get_data(self):
        """
        Retrieves the data to use for the error handler's response
        :return:
        """
        detail = self.get_detail()

        data = dict(
            detail=repr(self.get_detail()) if detail else None, field=self.get_field(),
        )

        # Add error code, if given
        error_code = self.get_error_code()
        if error_code is not None:
            data["error_code"] = error_code

        return data

    def get_response(self, use_dict=None):
        """
        Creates a JSON Response based on the information in this error handler
        """
        # DEPRECATED - old error reported now removed
        # if not use_dict:
        #     # Old error reporting method - soon to be removed
        #     return self.response_class(data=self.get_readable_error_list(), status=self.get_status_code())

        return self.response_class(data=self.get_data(), status=self.get_status_code())


class DRFApiErrorHandler(BaseApiErrorHandler):
    """
    Exception handler used by the app's DRF exception handler
    """

    def __init__(self, errors, **kwargs):
        super().__init__(**kwargs)
        self._errors = errors

    def get_error_object(self):
        return self._errors
