from django.forms.utils import ErrorDict, ErrorList
from main.ext.strings.utils import underscore_to_title

GENERIC_ERROR_KEYS = ["__all__", "non_field_errors"]


def _formatted_form_errors(obj, field_name_mappings, generic_keys):
    new_obj = obj

    if isinstance(obj, dict):
        new_obj = ErrorDict()
        for field, item in obj.items():
            # Rename dict field name
            field = get_error_field_name(
                field,
                field_name_mappings=field_name_mappings,
                generic_keys=generic_keys,
            )
            new_obj[field] = formatted_form_errors(
                item, generic_keys=generic_keys, field_name_mappings=field_name_mappings
            )
    elif isinstance(obj, list):
        new_obj = ErrorList(
            [
                formatted_form_errors(
                    o,
                    generic_keys=generic_keys,
                    field_name_mappings=field_name_mappings,
                )
                for o in obj
            ]
        )

    return new_obj


def formatted_form_errors(obj, field_name_mappings=None, generic_keys=None):
    """
    Renames the field names of an error list/dict combo to be more user-presentable
    """
    if field_name_mappings is None:
        field_name_mappings = {}

    return _formatted_form_errors(
        obj, field_name_mappings=field_name_mappings, generic_keys=generic_keys
    )


def get_error_field_name(field, field_name_mappings=None, generic_keys=None):
    """
    Converts the given field key into a readable field name
    :param field: the key to convert
    :param field_name_mappings custom specification of certain field names
    :return: a human-readable name for the field
    """
    if field_name_mappings is None:
        field_name_mappings = {}

    if generic_keys is None:
        generic_keys = GENERIC_ERROR_KEYS

    if field is not None and field not in generic_keys:
        if field in field_name_mappings:
            return field_name_mappings[field]
        else:
            return underscore_to_title(field)
