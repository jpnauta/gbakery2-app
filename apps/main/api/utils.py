from django.shortcuts import _get_queryset
from rest_framework.exceptions import APIException


class NotAccepted(APIException):
    status_code = 202


def get_object_or_202(klass, *args, **kwargs):
    queryset = _get_queryset(klass)
    try:
        return queryset.get(*args, **kwargs)
    except queryset.model.DoesNotExist:
        raise NotAccepted('No %s matches the given query.' % queryset.model._meta.object_name)
