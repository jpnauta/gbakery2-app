from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import (
    ListView,
    DetailView,
    TemplateView,
    RedirectView,
    FormView,
)
from .mixins import *


class BaseTemplateView(TemplateView):
    pass


class BaseListView(ListView):
    pass


class BaseDetailView(DetailView):
    pass


class BaseRedirectView(RedirectView):
    pass


class BaseFormView(FormView):
    pass


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
