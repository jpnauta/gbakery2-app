import logging

import settings


class RequireSentryEnabled(logging.Filter):
    def filter(self, record):
        return settings.ENABLE_SENTRY_ERRORS
