from __future__ import unicode_literals

from abc import abstractmethod
from main.utils.caching import get_default_cache


class BaseCacheKey(object):
    """
    Represents a string key to the cache. Helps with prefixing cache values
    and providing easy methods to the cache.
    """

    cache = get_default_cache()
    cache_prefix = "keys"  # Should be defined by subclasses

    @abstractmethod
    def get_key(self):
        raise NotImplemented

    @abstractmethod
    def get_timeout(self):
        raise NotImplemented

    @classmethod
    def get_cache_prefix(cls):
        return cls.cache_prefix

    def get_full_key(self):
        return "%s.%s" % (self.get_cache_prefix(), self.get_key())

    @classmethod
    def set(cls, *args, **kwargs):
        cls.cache.set(*args, **kwargs)

    @classmethod
    def get(cls, *args, **kwargs):
        return cls.cache.get(*args, **kwargs)

    @classmethod
    def delete(cls, *args, **kwargs):
        return cls.cache.delete(*args, **kwargs)

    @classmethod
    def incr(cls, *args, **kwargs):
        return cls.cache.incr(*args, **kwargs)

    @classmethod
    def iter_keys(cls, *args, **kwargs):
        return cls.cache.iter_keys(*args, **kwargs)

    @classmethod
    def iter_all_keys(cls):
        """
        Iterates over each key found in the object's cache prefix
        """
        return cls.iter_keys("%s.*" % cls.get_cache_prefix())

    def set_val(self, *args, **kwargs):
        """
        Quick method to set a value according to the object's preferences
        """
        kwargs.setdefault("timeout", self.get_timeout())
        return self.set(self.get_full_key(), *args, **kwargs)

    def get_val(self, *args, **kwargs):
        """
        Quick method to get a value according to the object's preferences
        """
        return self.get(self.get_full_key(), *args, **kwargs)

    def delete_val(self, *args, **kwargs):
        """
        Quick method to delete a value according to the object's preferences
        """
        return self.delete(self.get_full_key(), *args, **kwargs)

    def incr_val(self, *args, **kwargs):
        """
        Quick method to increment a value according to the object's preferences
        """
        return self.incr(self.get_full_key(), *args, **kwargs)
