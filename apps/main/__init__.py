from __future__ import absolute_import, unicode_literals
from django.apps import AppConfig
from .celery import app as celery_app


class MainConfig(AppConfig):
    name = "main"
    verbose_name = "Core"

    def ready(self):
        from main import signal_handlers

        super().ready()
        signal_handlers.load_all()

        from .serializers.consumers import AppConsumer
        import main.sync.routing  # Import all consumers

        AppConsumer.register_all()


default_app_config = "main.MainConfig"

# Tell celery where to find configuration
__all__ = ["celery_app"]
