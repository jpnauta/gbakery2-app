from django.core import mail
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from main.constants import NO_REPLY_ADDRESS_VALUE


class BaseEmailMultiAlternatives(mail.EmailMultiAlternatives):
    def __init__(self, *args, **kwargs):
        from_email = u"Glamorgan Bakery <%s>" % NO_REPLY_ADDRESS_VALUE

        # Convert subject to unicode, if needed (fixes Django proxy error)
        subject = kwargs.pop("subject", None)
        if subject:
            subject = repr(subject)

        super().__init__(subject=subject, from_email=from_email, *args, **kwargs)


class TemplateEmailMessage(BaseEmailMultiAlternatives):
    def __init__(self, template_name, context={}, *args, **kwargs):
        super().__init__(*args, **kwargs)

        body = render_to_string(template_name, context)
        self.attach_alternative(body, u"text/html")


def send_templated_message(
    template_id: str, template_data: dict, to=None, reply_to=None
):
    """
    Sends an email using the Sendgrid template specified
    :param template_id: ID of Sendgrid template
    :param template_data: Data to include in Sendgrid template
    :param to: Email address to send to
    :param reply_to: Reply to email
    """
    msg = EmailMessage(reply_to=reply_to, to=to,)
    msg.template_id = template_id
    msg.dynamic_template_data = template_data
    msg.send(fail_silently=False)
