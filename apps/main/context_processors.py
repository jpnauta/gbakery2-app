import settings


def load_general_constants(request):
    return {
        "SCHEDULE_FOLDER_URL": settings.SCHEDULE_FOLDER_URL,
        "FULL_SITE_URL": settings.FULL_SITE_URL,
    }
