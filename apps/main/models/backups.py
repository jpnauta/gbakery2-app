import os

from storages.backends.gcloud import GoogleCloudStorage
from storages.utils import setting

from settings import DB_BACKUPS_PATH


class GoogleCloudBackupStorage(GoogleCloudStorage):
    project_id = setting("DB_BACKUPS_PROJECT_ID", None)
    credentials = setting("DB_BACKUPS_CREDENTIALS", None)
    bucket_name = setting("DB_BACKUPS_BUCKET_NAME", None)
    auto_create_bucket = setting("DB_BACKUPS_AUTO_CREATE_BUCKET", False)
    auto_create_acl = setting("DB_BACKUPS_AUTO_CREATE_ACL", "projectPrivate")
    file_name_charset = setting("DB_BACKUPS_FILE_NAME_CHARSET", "utf-8")
    file_overwrite = setting("DB_BACKUPS_FILE_OVERWRITE", True)
    max_memory_size = setting("DB_BACKUPS_MAX_MEMORY_SIZE", 0)


def get_backups_storage_client() -> GoogleCloudBackupStorage:
    return GoogleCloudBackupStorage()


def get_db_backups_local_path():
    backup_path = DB_BACKUPS_PATH
    backup_directory = os.path.dirname(backup_path)

    if not os.path.exists(backup_directory):
        os.makedirs(backup_directory)

    return backup_path


def get_db_backups_file_name():
    return os.path.basename(DB_BACKUPS_PATH)
