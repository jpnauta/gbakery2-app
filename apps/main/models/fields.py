from django.core.validators import MinValueValidator
from django.db import models
from decimal import Decimal


class CurrencyField(models.DecimalField):
    DECIMAL_PLACES = 2
    MAX_DIGITS = 8

    def __init__(self, *args, **kwargs):
        kwargs["decimal_places"] = self.DECIMAL_PLACES
        kwargs["max_digits"] = self.MAX_DIGITS
        kwargs["validators"] = kwargs.get("validators", [])
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        try:
            return super().to_python(value).quantize(Decimal("0.01"))
        except AttributeError:
            return None

    @classmethod
    def to_cents(cls, amount: Decimal) -> int:
        """
        :param amount: Dollar amount (e.g. "12.23")
        :return: Amount in cents (e.g. 1223)
        """
        return int(amount * 100)

    @classmethod
    def from_cents(cls, amount: int) -> Decimal:
        """
        :param amount: Amount in cents (e.g. 1223)
        :return: Dollar amount (e.g. "12.23")
        """
        return Decimal(amount) / 100
