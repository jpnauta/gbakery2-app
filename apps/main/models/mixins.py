import uuid as _uuid
from decimal import Decimal

from django.core.validators import EMPTY_VALUES, MaxValueValidator, MinValueValidator
from django.db import models
from django.forms import model_to_dict
from django.utils.translation import ugettext as _
from rest_framework.exceptions import ValidationError

from main.ext.strings.utils import title, clean_whitespace
from main.models.fields import CurrencyField
from main.utils.slugs import unique_slugify


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def reload(self):
        """
        Reloads the object from the database
        """
        return self.__class__._default_manager.get(pk=self.pk)

    def has_foreign_key(self, name):
        return hasattr(self, name) and getattr(self, name) is not None

    def title_field(self, field):
        string = clean_whitespace(title(getattr(self, field, None)))
        setattr(self, field, string)

    def pre_save(self):
        """
        Override this function to perform actions right before object is saved. Similar to
        django signals, but used for setting default values, etc.
        """
        pass

    def post_save(self):
        """
        Override this function to perform actions right after object is saved. Similar to
        django signals, but used for setting default values, etc.
        """
        pass

    def save(self, *args, **kwargs):
        self.pre_save()
        obj = super().save(*args, **kwargs)
        self.post_save()
        return obj

    def force_require_fields(self, fields, message="This field is required"):
        """
        Requires the specified fields, even if they've been marked as blank=True. To be used
        with clean_fields() for optional require logic.
        """
        errors = {}
        for field in fields:
            val = getattr(self, field)

            if val in EMPTY_VALUES:
                errors[field] = message

        if errors:
            raise ValidationError(errors)

    class Meta:
        abstract = True


class Slugged(models.Model):
    """
    Abstract model that handles auto-generating slugs
    """

    slug_field = "name"
    slug = models.SlugField(unique=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            unique_slugify(self, getattr(self, self.slug_field))
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class DoubleUUID(models.Model):
    """
    Adds 2 UUID fields. Useful for password-less verification
    """

    uuid = models.UUIDField(default=_uuid.uuid4, editable=False)
    uuid2 = models.UUIDField(default=_uuid.uuid4, editable=False)

    class Meta:
        abstract = True


class Activated(models.Model):
    is_active = models.BooleanField(
        _("active status"),
        default=True,
        help_text=_("Determines if this is enabled or disabled"),
    )

    class Meta:
        abstract = True


class PublicPrivate(models.Model):
    is_public = models.BooleanField(
        default=True, help_text=_("Shows/hides item from customers")
    )

    class Meta:
        abstract = True


class Rated(models.Model):
    rating = models.DecimalField(
        max_digits=19, decimal_places=5, default=0, editable=False
    )

    class Meta:
        abstract = True


class UniqueProperName(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def _clean(self):
        self.title_field("name")  # Capitalize name

    def clean(self):
        self._clean()
        super().clean()

    def save(self, *args, **kwargs):
        self._clean()
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class ModelDiffMixin(object):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        return dict(diffs)

    @property
    def has_changed(self):
        return bool(self.diff)

    def get_changed_fields(self):
        return self.diff.keys()

    def field_has_changed(self, key):
        return key in self.get_changed_fields()

    def one_of_fields_have_changed(self, *keys):
        for key in keys:
            if self.field_has_changed(key):
                return True
        return False

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def field_changed_from_to(self, field, before, after, allow_reverse=False):
        """
        Indicates whether a field changed from value a to value b.

        Example: Suppose 'my_field' changed from 'before' to 'after'
            model.field_changed_from_to('my_field', 'before', 'after')  # True
            model.field_changed_from_to('my_field', 'before', 'val')  # False
            model.field_changed_from_to('my_field', 'val', 'after')  # False
            model.field_changed_from_to('my_field', 'after', 'before')  # False
            model.field_changed_from_to('my_field', 'after', 'before', allow_reverse=True)  # True
        """
        if self.field_has_changed(field):  # Ensure field has actually changed
            r_before, r_after = self.get_field_diff(field)

            if r_before == before and r_after == after:
                return True

            # Check reverse, if specified
            if allow_reverse and r_before == after and r_after == before:
                return True

        return False

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super().save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=[field.name for field in self._meta.fields])


class ModelDiff(ModelDiffMixin, models.Model):
    class Meta:
        abstract = True


class CustomizablePrice(models.Model):
    custom_price = CurrencyField(
        help_text=_("Overrides the calculated price, if given"),
        null=True,
        blank=True,
        validators=[MinValueValidator(Decimal("0"))],
    )
    calculated_price = CurrencyField(
        help_text=_("Price calculated by the system"),
        editable=False,
        null=True,
        blank=True,
        validators=[MinValueValidator(Decimal("0"))],
    )

    @property
    def price(self):
        if self.custom_price is not None:
            return self.custom_price
        return self.calculated_price

    class Meta:
        abstract = True


class SingletonModel(BaseModel):
    """
    Creates a model where there can only ever be one instance
    """
    _singleton = models.BooleanField(default=True, editable=False, unique=True)

    class Meta:
        abstract = True
