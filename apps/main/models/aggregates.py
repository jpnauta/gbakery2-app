from django.db.models import Max, Sum
from django.db.models.functions import Coalesce
from django.db.models import aggregates as sql_aggregates


class SumWithDefault(Coalesce):
    """Acts the same as the Sum aggregate, but returns then default when no entries are found"""

    def __init__(self, *args, **kwargs):
        default = kwargs.pop("default", 0)
        super().__init__(Sum(*args, **kwargs), default)


def regular_sum(queryset, field):
    """Retrieves the sum of the given field name, or 0 if no objects exist"""
    return queryset.aggregate(SumWithDefault(field, default=0))[
        "%s__sumwithdefault" % field
    ]


class SQLSumWithDefault(sql_aggregates.Sum):
    sql_template = "COALESCE(%(function)s(%(field)s), %(default)s)"


setattr(sql_aggregates, "SumWithDefault", SQLSumWithDefault)


def max_with_default(queryset, field, default=0):
    """Retrieves the sum of the given field name, or 0 if no objects exist"""
    return queryset.aggregate(max=Max(field))["max"] or default
