import os
import subprocess

import settings


def get_postgresql_env(db_name: str):
    """
    :return environment variables for running a postgresql command in a shell
    """
    env = os.environ.copy()
    env["PGDATABASE"] = settings.DATABASES[db_name].get("NAME")
    env["PGUSER"] = settings.DATABASES[db_name].get("USER")
    env["PGPASSWORD"] = settings.DATABASES[db_name].get("PASSWORD")
    env["PGHOST"] = settings.DATABASES[db_name].get("HOST")
    env["PGPORT"] = str(settings.DATABASES[db_name].get("PORT"))
    return env


def run_postgresql_shell_command(command: str, env: dict):
    """
    Runs the given command via shell with the appropriate `PG` environment variables
    """
    subprocess.run(command, shell=True, env=env, check=True)
