from __future__ import unicode_literals
from urllib.parse import urlparse, urlencode

from django.conf import settings
from django.urls import reverse
import tldextract
from main.ext.dicts.utils import encoded_dict


def full_url(key, *args, **kwargs):
    return link_with_domain(reverse(key, args=args, kwargs=kwargs))


def link_with_domain(link):
    return settings.FULL_SITE_URL + link


def url_with_querystring(path, **kwargs):
    return path + "?" + urlencode(encoded_dict(kwargs))


def get_filename(url):
    """
    Extracts the filename specified in the given URL, or None if it is not given
    """
    if url:
        return url.split("/")[-1]


def get_hostname(url):
    """
    :return: the hostname of the URL
    """
    if url:
        return urlparse(url).hostname


def get_domain(url):
    """
    :return: the domain of the URL
    """
    if url:
        extractor = tldextract.extract(url)

        domain = extractor.domain
        if extractor.suffix:
            domain = "%s.%s" % (domain, extractor.suffix)

        return domain


def get_path(url):
    """
    :return: the path of the URL
    """
    if url:
        return urlparse(url).path
