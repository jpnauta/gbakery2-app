import os
import shutil
from distutils.dir_util import copy_tree

import settings

MEDIA_FIXTURES_ROOT = os.path.join(settings.PROJECT_DIR, "testing/fixtures/media/")


def clear_test_media():
    """
    Deletes all files from the test media directory. Should be run before every test run.
    """
    dir = settings.MEDIA_ROOT

    if os.path.exists(dir):
        shutil.rmtree(dir)
    os.mkdir(dir)


def prepare_media_fixtures():
    """
    Copies all media fixtures into the media root. Should be run before every test run.
    """
    src = MEDIA_FIXTURES_ROOT
    dest = settings.MEDIA_ROOT

    # Copy all fixtures to media root
    copy_tree(src, dest)


def prepare_media():
    """
    Prepares the media directory for testing.
    """
    assert settings.TEST_MODE

    clear_test_media()
    prepare_media_fixtures()
