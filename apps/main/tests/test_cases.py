import asyncio
import json

from channels.testing import ChannelsLiveServerTestCase
from channels.testing.websocket import WebsocketCommunicator
from django.core.serializers.json import DjangoJSONEncoder
from django.template.defaultfilters import truncatechars
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import AccessToken

from main.urls.utils import url_with_querystring
from users.factories import UserModelFactory


class BaseTestCaseMixin(object):
    def ensure_successful_response(self, response, permitted_codes=None):
        # Set default permitted_codes
        if permitted_codes is None:
            permitted_codes = [200, 201, 204]

        # Convert single status codes expected to array
        if not isinstance(permitted_codes, list):
            permitted_codes = [permitted_codes]

        code = response.status_code
        if code not in permitted_codes:
            reason = truncatechars(response.content or str(response), 500)
            raise AssertionError(
                "unexpected status code: %s (%s %s)"
                % (code, response.reason_phrase, reason)
            )

    def json_dumps(self, data):
        return json.dumps(data, cls=DjangoJSONEncoder)

    def json_loads(self, str):
        if str:  # Don't load JSON if no data given
            return json.loads(str)


class BaseTestCase(TestCase, BaseTestCaseMixin):
    pass


class BaseClientTestCase(BaseTestCase):
    pass


class ApiAssertionTestCase(BaseTestCase):
    """
    Provides various assertions specific to API testing (e.g. validating JSON)
    """

    def parse_json_number(self, num):
        try:
            return float(num)
        except (TypeError, ValueError):
            raise AssertionError("expected number, received %s" % num)

    def assertEqualJsonNumber(self, json_num, num2, *args):
        num1 = self.parse_json_number(json_num)
        self.assertEqual(num1, num2, *args)


class BaseAPITestCase(ApiAssertionTestCase, BaseClientTestCase):
    client = APIClient()
    auth_token = None

    base_url = None  # Defined by subclass

    def get_list_url(self, postfix=None):
        """
        Returns: URL string accessing the list endpoint of the API under test
        """
        if postfix is None:
            postfix = ""

        return f"{self.base_url}{postfix}"

    def get_obj_url(self, identifier, postfix=None):
        """
        Args:
            identifier: object identifier (usually the object's ID or slug)

        Returns: URL string accessing the object endpoint of the API under test
        """
        if postfix is None:
            postfix = ""

        return f"{self.base_url}{identifier}/{postfix}"

    def tearDown(self):
        self.logout()  # Logout after each test
        return super().tearDown()

    def logout(self):
        self.client.logout()
        self.auth_token = None  # Clear auth token, as needed

    def login_user(self, user, password, permitted_codes=None):
        self.logout()
        response = self.make_request(
            "POST", "/api/auth/login/", {"email": user.email, "password": password}
        )

        self.auth_token = response.json()[
            "access_token"
        ]  # Keep auth token for later use

        self.ensure_successful_response(response, permitted_codes=permitted_codes)

    def build_user_with_password(self, password, **kwargs):
        user = UserModelFactory.build(**kwargs)
        user.set_password(password)
        user.save()
        return user

    def login_basic_user(self, **kwargs):
        password = kwargs.pop("password", "mypassword")
        user = self.build_user_with_password(password, **kwargs)
        self.login_user(user, password)

        return user

    def make_bad_request(self, method, path, **kwargs):
        return self.make_request(method, path, permitted_codes=[400], **kwargs)

    def make_request(
        self,
        method,
        path,
        data=None,
        permitted_codes=None,
        query_params=None,
        *args,
        **kwargs,
    ):
        if data is None:
            data = {}
        data = self.json_dumps(data)

        # Add query parameters to URL, if given
        if query_params is not None:
            path = url_with_querystring(path, **query_params)

        if self.auth_token is not None:  # Include
            kwargs["HTTP_AUTHORIZATION"] = "Bearer %s" % self.auth_token

        response = self.client.generic(
            method, path, data, content_type="application/json", *args, **kwargs
        )

        self.ensure_successful_response(response, permitted_codes)

        return response

    def assertPaginationResultsMatch(self, page, obj_ids):
        """
        Asserts that the given pagination object has only returns the objects denoted by the specified object IDs

        Args:
            page: Pagination dict object
            obj_ids: list of object IDs expected
        """
        expected_count = len(obj_ids)
        results = page["results"]
        self.assertEqual(
            page["count"],
            expected_count,
            "Incorrect number of results found in page %s" % page,
        )
        self.assertEqual(
            len(results),
            expected_count,
            "Incorrect number of results found in page %s" % page,
        )

        actual_ids = [r["id"] for r in results]

        for idx in obj_ids:
            self.assertIn(idx, actual_ids, "Missing object ID %s: %s" % (idx, page))

    def assert_response_message_equals(self, response: dict, message: str):
        self.assertEqual(response["detail"], message)


class BaseChannelTestCase(ChannelsLiveServerTestCase):
    communicator: WebsocketCommunicator = None

    def get_communicator(self) -> WebsocketCommunicator:
        raise NotImplementedError("Defined by subclasses")

    def setUp(self):
        super().setUp()
        self.communicator = self.get_communicator()

    def build_action_message(self, action, pk=None, data=None):
        return dict(action=action, pk=pk, data=data,)

    async def connect(self):
        await self.communicator.connect()

    async def disconnect(self):
        await self.communicator.disconnect()

    async def send_to(self, data):
        data_str = json.dumps(data)
        await self.communicator.send_to(data_str)

    async def receive_from(self):
        return json.loads(await self.communicator.receive_from())

    async def assert_all_received(self):
        with self.assertRaises(asyncio.TimeoutError):
            await self.communicator.receive_from()

    async def subscribe(self, data=None, permitted_codes=None):
        if permitted_codes is None:
            permitted_codes = [200]

        await self.send_to(dict(action="subscribe", data=data,))
        response = await self.receive_from()

        self.assertIn(response["response_status"], permitted_codes)

        return response

    def get_jwt_token(self, user):
        """
        :param user: User object
        :return: JWT token for user
        """
        return str(AccessToken.for_user(user))
