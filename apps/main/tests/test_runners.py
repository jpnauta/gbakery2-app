from django.test.runner import DiscoverRunner

from main.tests.media import prepare_media


class BaseDjangoTestSuiteRunner(DiscoverRunner):
    def build_suite(self, *args, **kwargs):
        # Prepare media
        prepare_media()

        return super().build_suite(*args, **kwargs)
