from io import StringIO
from PIL import Image
from django.core.files.base import ContentFile


def create_thumbnail(img, thumb, w, h=None):
    # h = None to mantain the aspect ratio
    # Don't do anything if there is no image to thumbnail.
    # Also, don't overwrite an existing thumbnail
    # (prevents a new thumbnail from being created every save).
    if not img or thumb:
        return
    image = Image.open(img.file)
    # If the image is smaller than w x h, don't bother creating a thumbnail.
    width, height = image.size
    if h:
        if width < w or height < h:
            return
        # Crop as little as possible to square, keeping the center.
        if width > height:
            delta = width - height
            left = int(delta / 2)
            upper = 0
            right = height + left
            lower = height
        else:
            delta = height - width
            left = 0
            upper = int(delta / 2)
            right = width
            lower = width + upper
        image = image.crop((left, upper, right, lower))
        # Create the thumbnail as a w x h square.
        image.thumbnail((w, h), Image.ANTIALIAS)
    else:
        ratio = width / height
        image.thumbnail((w, int(w * ratio)), Image.ANTIALIAS)
    # Save the thumbnail in the FileField.
    # Using Image.save(content, 'jpeg') seems to work for png too.
    buf = StringIO()
    image.save(buf, "jpeg", quality=95)
    cf = ContentFile(buf.getvalue())
    thumb.save(name=img.name, content=cf, save=False)
