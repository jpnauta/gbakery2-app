from django.conf import settings


def call_task(task, *args, **kwargs):
    """
    Simple function for calling a celery task. However, it may skip
    using celery if specified in the settings
    """
    use_celery = not getattr(settings, "ALLOW_CELERY_BYPASS", False)
    if use_celery:
        task.s(*args, **kwargs).apply_async()
    else:
        task(*args, **kwargs)
