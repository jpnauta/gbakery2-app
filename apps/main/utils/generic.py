def django_choice_options(dict_list, attr):
    """
    Converts a dict-of-dict options into the type of list-of-list options that django field
    choices require.
    :param dict_list: The dictionary of options, where each key is the unique choice value
    :param attr: The key value of each option's description
    """
    return list((key, item[attr]) for key, item in dict_list.items())
