def to_int(val, default=None):
    """
    Attempts to convert a value to an int
    """
    try:
        val = int(val)
    except (TypeError, ValueError):
        val = default
    return val
