from __future__ import unicode_literals

from django.db import models
from rest_framework.exceptions import ValidationError
from rest_framework.filters import OrderingFilter
import rest_framework_filters as filters

from main.exceptions.warning import deprecation_warning
from main.ext.dicts.utils import merge
from main.serializers.filters.fields import (
    AppDateFilter,
    AppDateTimeFilter,
)


class AppOrderingFilter(OrderingFilter):
    def rename_ordering_fields(self, ordering, view):
        """
        Uses the view's ordering_rename_map to rename any fields specified
        """
        ordering_rename_map = getattr(view, "ordering_rename_map", {})

        for field in ordering:
            asc_dec = field.startswith("-") and "-" or ""
            ref_field = field.lstrip("-")

            if ref_field in ordering_rename_map:
                ref_field = ordering_rename_map[ref_field]

            yield "%s%s" % (asc_dec, ref_field)

    def get_ordering(self, request, queryset, view):
        ordering = super().get_ordering(request, queryset, view)

        # Perform field renaming, as needed
        if ordering:
            ordering = list(self.rename_ordering_fields(ordering, view))

        return ordering


class AppFilterSet(filters.FilterSet):
    """
    Base class for all filter sets in this application

    https://github.com/philipn/django-rest-framework-filters
    """

    FILTER_DEFAULTS = merge(
        filters.FilterSet.FILTER_DEFAULTS,
        {
            models.DateField: {"filter_class": AppDateFilter},
            models.DateTimeField: {"filter_class": AppDateTimeFilter},
        },
    )

    COMPARISON_FIELDS = [
        "gte",
        "lte",
        "gt",
        "lt",
    ]  # To easily add all comparisons to field
    DATETIME_COMPARISON_FIELDS = COMPARISON_FIELDS + [
        "range"
    ]  # To easily add all comparisons to field

    deprecated_filter_map = {
        # Use this to support deprecated data keys
        # 'deprecated_key': 'new_key',
    }

    required_fields = [
        # Fields that must be given to perform query
    ]

    distinct_fields = [
        # Fields that require a `distinct` filter clause
    ]

    def __init__(self, *args, **kwargs):
        # Raise 400 error if invalid data

        super().__init__(*args, **kwargs)
        # Make data editable - required for deprecation rewrites
        if hasattr(self.data, "_mutable"):
            setattr(self.data, "_mutable", True)

        for old_key, new_key in self.deprecated_filter_map.items():
            if old_key in self.data:
                deprecation_warning('Key "%s" renamed to "%s"' % (old_key, new_key))
                self.data[new_key] = self.data.pop(old_key)[0]

        # Enable `distinct` logic for specified fields
        for field in self.distinct_fields:
            self.filters[field].distinct = True

    @property
    def qs(self):
        # Ensure fields in `require_fields` are given
        for field in self.required_fields:
            if not self.data.get(field, None):
                raise ValidationError(
                    'Key "%s" does not exists in your filters' % field
                )

        return super().qs
