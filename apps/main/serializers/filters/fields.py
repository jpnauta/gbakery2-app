import six
from django import forms
from django_filters.fields import Lookup
from rest_framework_filters import filters


def _strip_quotations(value):
    """
    Removes quotations from date strings if/where needed
    :param value: string that may have
    :return: stripped value, or original value if no quotations found
    """
    if (
        value
        and isinstance(value, six.string_types)
        and value.startswith('"')
        and value.endswith('"')
    ):
        value = value[1:-1]

    return value


class AppDateField(forms.DateTimeField):
    input_formats = ["%Y-%m-%d"]

    def to_python(self, value):
        value = _strip_quotations(value)  # Strip quotations if/where needed
        return super().to_python(value)


class AppDateFilter(filters.DateFilter):
    field_class = AppDateField


class AppDateTimeField(forms.DateTimeField):
    input_formats = [
        "%Y-%m-%dT%H:%M:%S.%fZ",
        "%Y-%m-%dT%H:%M:%S",
    ]

    def to_python(self, value):
        value = _strip_quotations(value)  # Strip quotations if/where needed
        if value and value == "true":
            value = None
        return super().to_python(value)


class AppDateTimeFilter(filters.DateTimeFilter):
    field_class = AppDateTimeField


class AppTagsMatchFilter(filters.CharFilter):
    """
    Filters by one of the tags specified (case insensitive)

    Example: ?tags__match=tag1,tag2  # Get results tagged with tag1 OR tag2
    """

    def filter(self, qs, value):
        value = [v.lower() for v in value.split(",")]
        value = Lookup(value=value, lookup_type="name__in")

        return super().filter(qs, value)


class AppModelChoiceField(forms.ModelChoiceField):
    def to_python(self, value):
        # Don't validate foreign keys!
        # - Unnecessarily allows hackers to find IDs of items they don't own
        # - Doesn't allow for `?product_attribute=0` for empty filtering
        return super().to_python(value)


class AppModelChoiceFilter(filters.ModelChoiceFilter):
    field_class = AppModelChoiceField
