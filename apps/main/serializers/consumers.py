import json
from typing import Type

from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
from django.db import transaction
from django.db.models.base import Model
from django.db.models.signals import post_save, post_delete
from rest_framework.exceptions import (
    APIException,
    AuthenticationFailed,
    NotAuthenticated,
)
from rest_framework.serializers import Serializer
from rest_framework_simplejwt.exceptions import TokenBackendError
from rest_framework_simplejwt.state import token_backend

from main.api.errors.handlers import DRFApiErrorHandler
from users.models import User

CREATE = "create"
UPDATE = "update"
DELETE = "delete"
SUBSCRIBE = "subscribe"


class AppConsumer(AsyncWebsocketConsumer):
    """
    Base class for for connecting a DRF serializer to a websocket channel
    """

    model: Model = None  # Defined by subclasses
    serializer_class: Type[Serializer] = None  # Defined by subclasses
    group_name: str = None  # Defined by subclasses

    lookup_field = "pk"
    permission_classes = ()

    error_handler_class = DRFApiErrorHandler

    @classmethod
    def _get_model_label(cls):
        """ Return string in the format of 'app.model' """

        return "%s.%s" % (
            cls.model._meta.app_label.lower(),
            cls.model._meta.object_name.lower(),
        )

    @classmethod
    def all_subclasses(cls):
        return set(cls.__subclasses__()).union(
            [s for c in cls.__subclasses__() for s in c.all_subclasses()]
        )

    @classmethod
    def register_all(cls):
        for subclass in cls.all_subclasses():
            if getattr(subclass, "model", None) is not None:
                subclass.register()

    @classmethod
    def register(cls):
        """ Register a models for its post save and delete signals """
        post_save.connect(cls.post_save_receiver, sender=cls.model)
        post_delete.connect(cls.post_delete_receiver, sender=cls.model)

    @classmethod
    def post_save_receiver(cls, instance, created, **kwargs):
        """ Reciever of the post_save signal, On commit call the process_message. """

        method = CREATE if created else UPDATE
        cls.process_message_after_commit(instance, method, **kwargs)

    @classmethod
    def post_delete_receiver(cls, instance, **kwargs):
        """ Reciever of the post_delete signal, On commit call the process_message. """

        cls.process_message_after_commit(instance, DELETE, **kwargs)

    @classmethod
    def process_message_after_commit(cls, instance, action, **kwargs):
        if not transaction.get_connection().in_atomic_block:
            async_to_sync(cls.process_message)(instance, action, **kwargs)
        else:
            transaction.on_commit(
                lambda: async_to_sync(cls.process_message)(instance, action, **kwargs)
            )

    @classmethod
    def get_data(cls, instance: Model):
        return cls.serializer_class(instance=instance).data

    @classmethod
    async def process_message(cls, instance, action, **kwargs):
        """ Prepare and send the msg payload to the channel layer """
        data = cls.get_data(instance)
        message = cls.prepare_message(action=action, data=data)

        channel_layer = get_channel_layer()
        await channel_layer.group_send(
            cls.group_name, dict(type="notify", text=cls.serialize(message),)
        )

    async def notify(self, event):
        """
        Function called to notify all users of an event
        """
        await self.send(text_data=event["text"])

    @classmethod
    def serialize(cls, obj: dict):
        return json.dumps(obj)

    @classmethod
    def prepare_message(cls, action, data=None, errors=None, status=200):
        """ Prepare data payload """

        return dict(errors=errors, data=data, action=action, response_status=status,)

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method." % self.__class__.__name__
        )
        return self.serializer_class

    def get_serializer_context(self):
        return dict()

    def serialize_data(self, instance):
        return self.get_serializer(instance).data

    async def connect(self):
        await super().connect()

    async def disconnect(self, code):
        await super().disconnect(code)
        await self.channel_layer.group_discard(self.group_name, self.channel_name)

    async def reply(self, action, data=None, errors=None, status=200) -> None:
        """
        Helper method to send a encoded response to the message's reply_channel.
        """
        message = self.prepare_message(
            action=action, data=data, errors=errors, status=status,
        )

        data_str = self.serialize(message)
        await self.send(data_str)

    async def receive(self, text_data=None, bytes_data=None):
        message = json.loads(text_data)
        action = message["action"]
        try:
            if action == SUBSCRIBE:
                await self.subscribe(message)
        except APIException as e:
            # Forward any API exception to user
            errors = self.error_handler_class(e.detail).get_data()
            await self.reply(action, errors=errors, status=e.status_code)

    async def subscribe(self, message: dict):
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        await self.reply(SUBSCRIBE, dict())


class DashboardConsumer(AppConsumer):
    """
    Base class for dashboard subscription websockets
    """

    model: Model = None  # Defined by subclasses
    group_name: str = None  # Defined by subclasses

    user = None

    def user_is_permitted(self):
        """
        Indicates if the user is permitted to connect
        :return: Whether or not to accept connection
        """
        return self.user is not None and self.user.is_authenticated()

    async def subscribe(self, message: dict):
        data = message.get("data", {})
        token = data.pop("token", None)

        if token is None:
            raise NotAuthenticated

        # Authenticate user before subscribe
        try:
            token_data = token_backend.decode(token)
            user_id = token_data["user_id"]
            self.user = User.objects.get(id=user_id)
        except TokenBackendError:
            raise AuthenticationFailed

        await super().subscribe(message)
