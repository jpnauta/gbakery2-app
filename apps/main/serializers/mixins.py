from django.core.validators import EMPTY_VALUES
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import Serializer

from main.ext.lists.utils import get_first


class AppSerializerMixin(object):
    field_name_mappings = {}  # Use this to rename field names shown in error messages

    @classmethod
    def get_context_attr(cls, content, key):
        return content and content.get(key)

    def force_require_fields(self, attrs, fields, message="This field is required"):
        """
        Requires the specified fields, even if they've been marked as blank=True. To be used
        with validate() for optional require logic.
        """
        errors = {}
        for field in fields:
            val = attrs.get(field)

            if val in EMPTY_VALUES:
                errors[field] = message

        if errors:
            raise ValidationError(errors)


class AppSerializer(AppSerializerMixin, Serializer):
    pass


class AppModelSerializer(AppSerializerMixin, ModelSerializer):
    pass


class NestedAppModelSerializer(AppModelSerializer):
    pk_field = "id"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields[
            self.pk_field
        ].read_only = False  # Allows the nested element to be updated

    def delete(self, instance):
        """
        Deletes the specified instance
        :param instance: model object
        """
        instance.delete()

    def create_m2m_related(self, related_data, related_key, related_val):
        """
        Shortcut method to create many-to-many related data.

        Any items in `related_data` without primary keys will be created. Any items in the `related_qs`
        that are not in `related_data` will be deleted.

        TODO: how to effectively handle items with primary key that are not in `related_qs`?
           - Currently, it creates a new entity
        """
        # TODO docs

        # Create m2m
        for obj_data in related_data:
            obj_data[related_key] = related_val
            if self.pk_field in obj_data:
                del obj_data[self.pk_field]  # Just in case it's been set
            self.create(obj_data)

    def update_m2m_related(self, related_data, related_key, related_val, related_qs):
        """
        Shortcut method to update many-to-many related data.

        Any items in `related_data` without primary keys will be created. Any items in the `related_qs`
        that are not in `related_data` will be deleted.

        TODO: how to effectively handle items with primary key that are not in `related_qs`?
           - Currently, it creates a new entity
        """
        pk_field = self.pk_field

        obj_ids = [
            obj_data[pk_field] for obj_data in related_data if pk_field in obj_data
        ]

        # Delete any related objects not included in the request
        for obj in related_qs:
            if obj.id not in obj_ids:
                self.delete(obj)

        # Create or update related objects that are in the request
        for obj_data in related_data:
            obj_data[related_key] = related_val

            instance = get_first(
                [obj for obj in related_qs if obj.id == obj_data.get(pk_field)]
            )
            if instance is not None:
                # Update existing
                self.update(instance, obj_data)
            else:
                # Create new
                if pk_field in obj_data:
                    del obj_data[pk_field]  # Just in case it's been set
                self.create(obj_data)


class DetailsModelSerializer(AppModelSerializer):
    def __init__(self, *args, **kwargs):
        self.details = self.get_context_attr(kwargs.get("context"), "details") or False
        super().__init__(*args, **kwargs)


class DateRequiredModelSerializer(AppModelSerializer):
    def __init__(self, date, *args, **kwargs):
        self.date = date
        super().__init__(*args, **kwargs)


class M2MRelatedModelSerializer(AppModelSerializer):
    """
    Suppose you had a set of models with this `m2m` relationship:

    MyModel <---instance_field--- MyRelation ---relation_field---> MyRelated

    And you wanted serializer for `MyModel` to save data like this:

    {
        "my_related": [1, 2, 3]
    }

    You can use this mixin's utility functions to accomplish this.
    """

    def prepare_for_m2m_create(
        self, relation_model_class, new_related_ids, relation_field
    ):
        """
        Prepares the relations for `new_related_ids` given to be created in the DB. Should be used in
        the serializer's `create` function. See class description for more info.

        :param relation_model_class: `MyRelation` class
        :param relation_field: Name of field that relates `MyRelation` to `MyRelated`
        :return: [relations_to_create, relations_to_update, relations_to_delete] list of relations to be
        created, updated and deleted (must be handled manually after `update()`
        :return:
        """
        relations_to_create = []
        for related in new_related_ids:
            relation = relation_model_class()
            setattr(relation, relation_field, related)
            relations_to_create.append(relation)

        return relations_to_create

    def prepare_for_m2m_update(
        self, instance, relation_model_class, old_relations, new_related, relation_field
    ):
        """
        Prepares the relations for `new_related_ids` given to be updated in the DB. Should be used in
        the serializer's `update` function. See class description for more info.

        :param instance: `MyModel` instance
        :param relation_model_class: `MyRelation` class
        :param old_relations: queryset of `MyRelation` objects (E.g. `instance.my_relations.all()`)
        :param new_related: IDs of new `MyRelated` objects. Assumes each ID is unique, valid, saved.
        :param relation_field: Name of field that relates `MyRelation` to `MyRelated`
        :return: [relations_to_create, relations_to_update, relations_to_delete] list of relations to be
        created, updated and deleted (must be handled manually after `update()`
        """

        # Compile a list of new relations to be created/updated
        relations_to_create = []
        relations_to_update = []
        for related in new_related:
            try:
                # Attempt to find existing relation
                relation = next(
                    r for r in old_relations if related.id == getattr(r, relation_field)
                )

                # Update existing
                relations_to_update.append(relation)
            except StopIteration:
                # Create new relation
                relation = relation_model_class()
                setattr(relation, relation_field, related)
                relations_to_create.append(relation)

        # Finds relations to be deleted
        new_related = [related.id for related in new_related]
        relations_to_delete = [
            r for r in old_relations if getattr(r, relation_field) not in new_related
        ]

        return relations_to_create, relations_to_update, relations_to_delete

    def bulk_create_m2m_relations(
        self, instance, instance_field, relation_model_class, relations_to_create
    ):
        """
        Creates a list of `MyRelation` objects to the DB using the `QuerySet.bulk_create` function.
        See class description for more info.

        WARNING: Using `QuerySet.bulk_create` will not trigger Django's built-in signals

        :param instance: `MyModel` instance
        :param instance_field: Name of field that relates `MyRelation` to `MyModel`
        :param relation_model_class: `MyRelation` class
        :param relations_to_create: list of `MyRelation` objects to create
        :return:
        """
        for relation in relations_to_create:
            setattr(relation, instance_field, instance)
        relation_model_class.objects.bulk_create(relations_to_create)

    def bulk_delete_m2m_relations(self, relation_model_class, relations_to_delete):
        """
        Deletes a list of `MyRelation` objects to the DB using the `QuerySet.delete` function.
        See class description for more info.

        WARNING: Using `QuerySet.delete` will not trigger Django's built-in signals

        :param relation_model_class: `MyRelation` class
        :param relations_to_delete: list of `MyRelation` objects to delete
        :return:
        """
        ids = [relation.id for relation in relations_to_delete]
        relation_model_class.objects.filter(id__in=ids).delete()
