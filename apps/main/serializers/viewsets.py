from __future__ import unicode_literals

from rest_framework.viewsets import GenericViewSet, ModelViewSet, ReadOnlyModelViewSet

from main.serializers.permissions import NO_AUTHENTICATION_REQUIRED


class AppGenericViewSet(GenericViewSet):
    """
    The GenericViewSet class does not provide any actions by default,
    but does include the base set of generic view behavior, such as
    the `get_object` and `get_queryset` methods.
    """

    pass


class AppPublicViewSetMixin(object):
    """
    Disables authentication for a viewset
    """

    permission_classes = NO_AUTHENTICATION_REQUIRED


class AppReadOnlyModelViewSet(ReadOnlyModelViewSet):
    """
    A viewset that provides default `list()` and `retrieve()` actions.
    """

    pass


class AppModelViewSet(ModelViewSet):
    """
    A viewset that provides default `create()`, `retrieve()`, `update()`,
    `partial_update()`, `destroy()` and `list()` actions.
    """

    pass


class ShowDetailsViewSetMixin(object):
    show_details = False

    def list(self, *args, **kwargs):
        self._set_show_details()  # Allow user to request detailed data
        return super().list(*args, **kwargs)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["details"] = self.show_details
        return context

    def _set_show_details(self):
        self.show_details = self.request.query_params.get("details")
