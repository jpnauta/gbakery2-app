from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import DecimalField

from main.models import CurrencyField


class AppListField(serializers.ListField):
    """
    A field whose values are lists of items described by the given item field. The item field can
    be another field type (e.g., CharField) or a serializer. However, for serializers, you should
    instead just use it with the `many=True` option.
    """

    def __init__(self, *args, **kwargs):
        self.allow_duplicates = kwargs.pop("allow_duplicates", True)
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        data = super().to_internal_value(data)

        if not self.allow_duplicates:
            if len(data) != len(set(data)):
                raise ValidationError("Duplicate entries not permitted")

        return data


class CurrencySerializerField(DecimalField):
    def __init__(self, *args, **kwargs):
        kwargs["max_digits"] = CurrencyField.MAX_DIGITS
        kwargs["decimal_places"] = CurrencyField.DECIMAL_PLACES
        super().__init__(*args, **kwargs)
