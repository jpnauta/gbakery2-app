from rest_framework.settings import api_settings

DEFAULT_PERMISSION_CLASSES = api_settings.DEFAULT_PERMISSION_CLASSES
AUTHENTICATION_REQUIRED = DEFAULT_PERMISSION_CLASSES
NO_AUTHENTICATION_REQUIRED = ()
