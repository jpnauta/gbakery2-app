from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class AppPageNumberPagination(PageNumberPagination):
    page_size_query_param = "page_size"
    max_page_size = 1000

    def get_next_page_number(self):
        if self.page.has_next():
            return self.page.next_page_number()
        return None

    def get_previous_page_number(self):
        if self.page.has_previous():
            return self.page.previous_page_number()
        return None

    def get_paginated_response(self, data):
        return Response(
            OrderedDict(
                [
                    ("count", self.page.paginator.count),
                    ("next_page_number", self.get_next_page_number()),
                    ("page_number", self.page.number),
                    ("num_pages", self.page.paginator.num_pages),
                    ("previous_page_number", self.get_previous_page_number()),
                    ("results", data),
                ]
            )
        )


class OptionalPageNumberPagination(AppPageNumberPagination):
    """
    Same as `AppPageNumberPagination`, but data is not paginated unless `page_size` is
    specified
    """

    page_size = None
