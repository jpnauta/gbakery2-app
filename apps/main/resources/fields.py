from django.core.exceptions import ValidationError
from import_export.widgets import BooleanWidget, IntegerWidget

from main.ext.lists.utils import get_first
from products.models import QUANTITY_TYPE_OPTIONS


class AppBooleanWidget(BooleanWidget):
    TRUE_VALUES = BooleanWidget.TRUE_VALUES + ["YES", "yes", "Yes"]
    FALSE_VALUES = BooleanWidget.FALSE_VALUES + ["NO", "no", "No"]


class QuantityTypeWidget(IntegerWidget):
    def clean(self, value, row=None, *args, **kwargs):
        # Convert quantity type string back to ID
        value = get_first(
            k
            for k, v in QUANTITY_TYPE_OPTIONS.items()
            if v["name"].lower() == value.lower()
        )
        if value is None:
            raise ValidationError(f"Invalid quantity type: '{row['quantity_type']}'")

        return super().clean(value, row=row, *args, **kwargs)

    def render(self, value, obj=None):
        # Convert quantity type ID to human readable string
        value = QUANTITY_TYPE_OPTIONS[value]["name"]

        return super().render(value, obj=obj)
