from django import template

register = template.Library()


@register.simple_tag
def ui_phone_number_mask():
    return u"(999)999-9999"
