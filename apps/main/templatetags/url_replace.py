from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    """
    Replaces the current url parameters with the given
    parameters. Useful for preserving url parameters
    for common urls (e.g. pagination with filtering)
    """
    request = context["request"]
    params = request.GET.copy()

    for key in kwargs:
        params[key] = kwargs[key]

    return params.urlencode()
