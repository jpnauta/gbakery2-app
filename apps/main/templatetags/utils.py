from django import template

register = template.Library()


@register.filter
def image_url(obj, field):
    if obj:
        return obj.image_url(field)
