from django import forms
from django.core.exceptions import ValidationError


class BaseFormMixin(object):
    field_name_mappings = {}  # Use this to rename field names shown in error messages


class BaseModelForm(forms.ModelForm, BaseFormMixin):
    pass


class BaseForm(forms.Form, BaseFormMixin):
    pass


class BaseModelFormSetMixin(object):
    def clean_at_least_one_exists(self):
        """
        Ensures that at least one baker product is related
        """
        # get forms that actually have valid data
        count = 0
        for form in self.forms:
            try:
                c = form.cleaned_data
                if c and not c.get("DELETE"):  # Exclude items about to be deleted
                    count += 1
            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass
        if count < 1:
            raise ValidationError("You must have at least one related item")

    def clean_unique(self, field_name):
        values = set()
        for form in self.forms:
            value = form.cleaned_data.get(field_name)

            if value:
                if value in values:
                    raise forms.ValidationError(
                        'Duplicate values for "%s" are not allowed.' % field_name
                    )
                values.add(value)


class BaseModelFormSet(forms.BaseModelFormSet, BaseModelFormSetMixin):
    pass


class BaseInlineFormSet(forms.BaseInlineFormSet, BaseModelFormSet):
    pass
