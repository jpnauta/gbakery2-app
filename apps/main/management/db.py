from django.db import connections
from django.db.migrations.executor import MigrationExecutor
from django.db.utils import OperationalError


def is_database_synchronized(database):
    connection = connections[database]
    connection.prepare_database()
    executor = MigrationExecutor(connection)
    targets = executor.loader.graph.leaf_nodes()
    return False if executor.migration_plan(targets) else True


def is_database_running(database):
    db_conn = connections[database]
    try:
        db_conn.ensure_connection()
    except OperationalError:
        return False
    else:
        return True
