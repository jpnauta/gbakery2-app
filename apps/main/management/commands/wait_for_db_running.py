from __future__ import unicode_literals

from time import sleep

from django.core.management.base import BaseCommand
from django.db.utils import DEFAULT_DB_ALIAS

from main.management.db import is_database_running


class Command(BaseCommand):
    help = "Waits until database is running and can be connected to"

    def handle(self, **options):
        while True:
            if is_database_running(DEFAULT_DB_ALIAS):
                break

            print("Waiting for database to be running...")
            sleep(5)
