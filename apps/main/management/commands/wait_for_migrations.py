from __future__ import unicode_literals

from time import sleep

from django.core.management.base import BaseCommand
from django.db.utils import DEFAULT_DB_ALIAS

from main.management.db import is_database_synchronized


class Command(BaseCommand):
    help = "Waits until all migrations are applied"

    def handle(self, **options):
        while True:
            if is_database_synchronized(DEFAULT_DB_ALIAS):
                break

            print("Waiting for migrations to be applied...")
            sleep(5)
