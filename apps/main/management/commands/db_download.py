from django.core.management.base import BaseCommand

from main.models.backups import (
    get_backups_storage_client,
    get_db_backups_file_name,
    get_db_backups_local_path,
)


class Command(BaseCommand):
    help = "Downloads DB backup from cloud storage to local machine"

    def handle(self, *args, **options):
        client = get_backups_storage_client()

        outfile = get_db_backups_local_path()
        filename = get_db_backups_file_name()

        print("Downloading %s to %s..." % (filename, outfile))

        with client.open(filename) as gcs_file:
            with open(outfile, "wb") as local_file:
                local_file.write(gcs_file.read())
