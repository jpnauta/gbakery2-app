from django.core.management.base import BaseCommand

from main.models.backups import get_db_backups_local_path
from main.models.utils import get_postgresql_env, run_postgresql_shell_command


class Command(BaseCommand):
    db_name = "default"
    help = "Restores the database from a file"

    def handle(self, *args, **options):
        infile = get_db_backups_local_path()

        print("Dropping existing database...")
        env = get_postgresql_env(self.db_name)
        db = env["PGDATABASE"]
        env["PGDATABASE"] = ""
        run_postgresql_shell_command(
            "psql -c 'REVOKE CONNECT ON DATABASE %s FROM public'" % db, env
        )
        run_postgresql_shell_command(
            "psql -c 'SELECT pg_terminate_backend(pg_stat_activity.pid) "
            "FROM pg_stat_activity WHERE pid <> pg_backend_pid()'",
            env,
        )
        run_postgresql_shell_command("dropdb '%s'" % db, env)
        run_postgresql_shell_command("createdb '%s'" % db, env)

        print('Restoring %s into "%s"...' % (infile, db))
        env = get_postgresql_env(self.db_name)
        run_postgresql_shell_command("psql %s < %s" % (db, infile), env)
