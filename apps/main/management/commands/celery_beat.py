from __future__ import unicode_literals

import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Runs a celerybeat worker in development mode"

    def handle(self, **options):
        os.system('celery -A apps.main beat -l info --pidfile="/tmp/celerybeat.pid"')
