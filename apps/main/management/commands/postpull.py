from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Runs various tasks to be performed after pull from repository"

    def handle(self, **options):
        call_command("migrate")  # Compile .less files
