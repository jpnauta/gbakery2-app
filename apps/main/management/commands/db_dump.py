from django.core.management.base import BaseCommand

from main.models.backups import get_db_backups_local_path
from main.models.utils import run_postgresql_shell_command, get_postgresql_env


class Command(BaseCommand):
    db_name = "default"
    help = "Dumps database into a file"

    def handle(self, *args, **options):
        infile = get_db_backups_local_path()
        outfile = get_db_backups_local_path()

        env = get_postgresql_env(self.db_name)
        db = env["PGDATABASE"]
        print('Backing up "%s" into %s...' % (db, outfile))
        run_postgresql_shell_command("pg_dump %s > %s" % (db, infile), env)
