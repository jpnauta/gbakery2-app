from django.core.management.base import BaseCommand

from main.models.backups import (
    get_backups_storage_client,
    get_db_backups_local_path,
    get_db_backups_file_name,
)


class Command(BaseCommand):
    help = "Uploads DB backup from local machine to cloud storage"

    def handle(self, *args, **options):
        client = get_backups_storage_client()

        infile = get_db_backups_local_path()
        filename = get_db_backups_file_name()

        print("Uploading %s to %s..." % (infile, filename))

        with open(infile, "rb") as f:
            client.save(filename, f)
