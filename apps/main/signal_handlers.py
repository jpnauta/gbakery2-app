import settings


def load_all():
    """
    Imports all signal handlers
    """
    # Import all signal handlers
    for module in settings.INSTALLED_APPS:
        try:
            # Attempt to import 'signal_handlers' module
            __import__(module, fromlist=["signal_handlers"])
        except ImportError:  # Ignore apps without 'signal_handlers' module
            pass
