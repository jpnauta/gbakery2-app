import asyncio
import json
from functools import wraps, partial
from unittest.mock import patch

import stripe


def async_test(f=None):
    if f is None:
        return partial(async_test)

    @wraps(f)
    def wrapper(*args, **kwargs):
        coro = asyncio.coroutine(f)
        future = coro(*args, **kwargs)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(future)

    return wrapper


def bypass_stripe_webhook_signature_check():
    """
    Disables Stripe's webhook event validation for the code block
    """

    def mock_construct_event(data, *args, **kwargs):
        return stripe.Event.construct_from(json.loads(data), stripe.api_key)

    return patch.object(
        stripe.Webhook, "construct_event", side_effect=mock_construct_event
    )
