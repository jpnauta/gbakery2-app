def assert_statuses_are_met(permission_list, test_case):
    """
    Shortcut method to test permissions of a collection of API endpoints
    Args:
        permission_list: list of (method, url, status_code) tuples
            - method: 'GET', 'POST', etc.
            - url: '/api/venue/my-api/999/'
            - status_code: expected response code
        test_case: BaseAPITestCase instance
    """

    for method, url, status_code in permission_list:
        # Positive test case - ensure with permissions does not get 403
        try:
            test_case.make_request(method, url, permitted_codes=[status_code])
        except AssertionError as e:
            test_case.fail(
                "Error occurred while checking with permissions: %s %s %s"
                % (method, url, e)
            )
