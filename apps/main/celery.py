from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

app = Celery("main")

# Configure settings
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    # PERIODIC TASKS
    "set_front_product_ratings": {
        "task": "products.tasks.set_front_product_ratings",
        "schedule": crontab(minute=3, hour="*/12"),
        "args": (),
    },
    "set_cake_ratings": {
        "task": "cakes.tasks.set_cake_ratings",
        "schedule": crontab(minute=17, hour="*/12"),
        "args": (),
    },
    "update_order_indexes": {
        "task": "orders.tasks.update_order_indexes",
        "schedule": crontab(minute=49, hour="*/12"),
        "args": (),
    },
    "perform_db_backup": {
        "task": "main.tasks.perform_db_backup",
        "schedule": crontab(minute=36, hour=12),
        "args": (),
    },
}
