from itertools import groupby

from django.db.models import Q
from main.managers import ActivatedQuerySet, BaseQuerySet
from products.models import QUANTITY_INDIVIDUAL


class OrderQuerySet(ActivatedQuerySet):
    def for_date(self, date):
        return self.filter(date=date)

    def for_hour_string(self, hour_string):
        return self.filter(hour_string=hour_string)

    def filled(self):
        return self.filter(filled=True)

    def picked_up(self):
        return self.filter(picked_up=True)


class OrderProductQuerySet(BaseQuerySet):
    def for_order_date(self, d):
        return self.filter(order__date=d)

    def unfilled(self):
        return self.filter(order__filled=False, filled=False)

    def filled(self):
        return self.filter(Q(order__filled=True) | Q(filled=True))

    def product_str(self):
        return u", ".join([repr(obj) for obj in self])

    def for_front_product(self, front_product):
        return self.filter(front_product=front_product)

    def total_quantity(self):
        return self.regular_sum(total_quantity="total_quantity")["total_quantity"]

    def quantity_for_date_and_front_product(self, d, product):
        return (
            self.for_order_date(d)
            .for_front_product(product)
            .unfilled()
            .total_quantity()
        )

    def picked_up(self):
        return self.filter(order__picked_up=True)

    def _increment_total(self, obj, quantity, filled, picked_up):
        obj["total"] += quantity
        if not filled:
            obj["to_be_filled"] += quantity
        if not picked_up:
            obj["to_be_picked_up"] += quantity

    def _increment_totals(self, totals, quantity_type, quantity, filled, picked_up):
        quantity_types = totals["quantity_types"]

        if not quantity_types.get(quantity_type):
            quantity_types[quantity_type] = dict(
                total=0, to_be_filled=0, to_be_picked_up=0,
            )

        self._increment_total(totals, quantity, filled, picked_up)
        self._increment_total(
            quantity_types[quantity_type], quantity, filled, picked_up
        )

    def _totals_product(
        self,
        order_products,
        id=None,
        name=None,
        order_name=None,
        quantity_type=None,
        product_type=None,
        is_starred=False,
    ):
        totals_product = dict(
            id=id,
            name=name,
            order_name=order_name,
            product_type=product_type,
            quantity_type=quantity_type,
            is_starred=is_starred,
            totals=dict(
                total=0, to_be_filled=0, to_be_picked_up=0, quantity_types=dict(),
            ),
        )

        for op in order_products:
            self._increment_totals(
                totals_product["totals"],
                op.quantity_type,
                op.total_quantity,
                op.is_filled(),
                op.is_picked_up(),
            )

        return totals_product

    def calculate_totals(self):
        from orders.models import (
            OP_TYPE_FRONT_PRODUCT,
            OP_TYPE_SPECIAL_PRODUCT,
            FrontProduct,
        )

        totals = []

        ops = self.select_related("order", "front_product")
        front_product_ops = [
            op for op in ops if op.product_type == OP_TYPE_FRONT_PRODUCT
        ]

        # Calculate front product stats
        k = lambda op: (op.front_product_id is None, op.front_product_id)
        groups = groupby(sorted(front_product_ops, key=k), key=k)
        for (_, front_product_id), group in groups:
            grouped_ops = list(group)
            fp = grouped_ops[0].front_product
            order = grouped_ops[0].order
            if fp:
                name = fp.name
                quantity_type = fp.quantity_type if fp else None
                is_starred = fp.is_starred
            else:
                # Should only happen with old data
                name = "(ERROR)"
                quantity_type = QUANTITY_INDIVIDUAL
                is_starred = False

            totals_product = self._totals_product(
                grouped_ops,
                id=front_product_id,
                name=name,
                order_name=order.name,
                quantity_type=quantity_type,
                product_type=OP_TYPE_FRONT_PRODUCT,
                is_starred=is_starred,
            )
            totals.append(totals_product)

        # Calculate special product stats
        special_product_ops = [
            op for op in ops if op.product_type == OP_TYPE_SPECIAL_PRODUCT
        ]
        for op in special_product_ops:
            totals_product = self._totals_product(
                [op],
                id=None,
                name=op.name,
                order_name=op.order.name,
                quantity_type=op.quantity_type,
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                is_starred=True,
            )
            totals.append(totals_product)

        return totals


class OrderFlagQuerySet(BaseQuerySet):
    pass
