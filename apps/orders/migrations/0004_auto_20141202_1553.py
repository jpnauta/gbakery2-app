# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("orders", "0003_order_special_products_text"),
    ]

    operations = [
        migrations.CreateModel(
            name="SpecialProduct",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                (
                    "filled",
                    models.BooleanField(
                        default=False, help_text="Product has been filled"
                    ),
                ),
                ("description", models.CharField(max_length=1024)),
                (
                    "order",
                    models.ForeignKey(
                        related_name="special_products",
                        to="orders.Order",
                        on_delete=PROTECT,
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(name="order", options={"ordering": ("name",)},),
        migrations.AlterField(
            model_name="order",
            name="special_products_text",
            field=models.TextField(
                default="",
                help_text="all special products ordered, each separated by a new line",
                blank=True,
            ),
        ),
        migrations.AlterField(
            model_name="orderfrontproduct",
            name="description",
            field=models.CharField(max_length=1024, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name="orderfrontproduct",
            name="total_quantity",
            field=models.PositiveIntegerField(editable=False, blank=True),
        ),
    ]
