# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import csv
import os

from django.db import models, migrations
import settings


def create_order_locations(apps, schema_editor):
    OrderLocation = apps.get_model("orders", "OrderLocation")


class Migration(migrations.Migration):

    dependencies = [
        ("orders", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(create_order_locations),
    ]
