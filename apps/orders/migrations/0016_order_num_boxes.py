# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-11-29 03:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("orders", "0015_auto_20180223_1430"),
    ]

    operations = [
        migrations.AddField(
            model_name="order",
            name="num_boxes",
            field=models.IntegerField(blank=True, null=True, verbose_name="# of boxes"),
        ),
    ]
