# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Order",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                (
                    "is_active",
                    models.BooleanField(
                        default=True,
                        help_text="Determines if this is enabled or disabled",
                        verbose_name="active status",
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        help_text="Customer's full name",
                        max_length=128,
                        verbose_name="customer name",
                    ),
                ),
                (
                    "phone_number",
                    models.CharField(
                        help_text="Customer's phone number",
                        max_length=128,
                        null=True,
                        verbose_name="customer phone number",
                        blank=True,
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        help_text="Customer's email address",
                        max_length=128,
                        null=True,
                        verbose_name="customer email",
                        blank=True,
                    ),
                ),
                (
                    "type",
                    models.IntegerField(
                        default=1,
                        verbose_name="order type",
                        choices=[(1, "Regular"), (2, "Cake")],
                    ),
                ),
                ("date", models.DateField(verbose_name="pickup date")),
                (
                    "time",
                    models.TimeField(null=True, verbose_name="pickup time", blank=True),
                ),
                (
                    "filled",
                    models.BooleanField(
                        default=False, help_text="Order is currently being filled"
                    ),
                ),
                (
                    "picked_up",
                    models.BooleanField(
                        default=False, help_text="Order has been picked up"
                    ),
                ),
                (
                    "in_progress",
                    models.BooleanField(
                        default=False, help_text="Order is currently being packed"
                    ),
                ),
                (
                    "verified",
                    models.BooleanField(
                        default=False, help_text="Order has been verified"
                    ),
                ),
                ("hour_string", models.CharField(max_length=2, blank=True)),
                ("ref_id", models.PositiveIntegerField(blank=True)),
                ("reference_id", models.CharField(max_length=128, blank=True)),
                (
                    "comment",
                    models.TextField(
                        null=True, verbose_name="comments/details", blank=True
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="OrderCollection",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
            ],
            options={"abstract": False,},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="OrderFrontProduct",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("quantity", models.PositiveIntegerField()),
                (
                    "quantity_type",
                    models.IntegerField(
                        choices=[(1, "Individual"), (2, "Half Dozen"), (3, "Dozen")]
                    ),
                ),
                (
                    "filled",
                    models.BooleanField(
                        default=False, help_text="Product has been filled"
                    ),
                ),
                ("total_quantity", models.PositiveIntegerField(blank=True)),
                ("description", models.CharField(max_length=1024, blank=True)),
                (
                    "front_product",
                    models.ForeignKey(
                        related_name="order_front_products",
                        to="products.FrontProduct",
                        on_delete=PROTECT,
                    ),
                ),
                (
                    "order",
                    models.ForeignKey(
                        related_name="order_front_products",
                        to="orders.Order",
                        on_delete=PROTECT,
                    ),
                ),
            ],
            options={"verbose_name": "order product",},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="OrderLocation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("description", models.CharField(unique=True, max_length=64)),
                ("hotkey", models.CharField(unique=True, max_length=2)),
            ],
            options={"abstract": False,},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name="order",
            name="collection",
            field=models.ForeignKey(
                to="orders.OrderCollection", blank=True, on_delete=PROTECT
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="order",
            name="front_products",
            field=models.ManyToManyField(
                related_name="orders",
                through="orders.OrderFrontProduct",
                to="products.FrontProduct",
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="order",
            name="location",
            field=models.ForeignKey(
                blank=True,
                to="orders.OrderLocation",
                help_text="Where the order is located (once filled)",
                null=True,
                on_delete=PROTECT,
            ),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="order",
            unique_together=set(
                [("date", "hour_string", "ref_id"), ("date", "reference_id")]
            ),
        ),
    ]
