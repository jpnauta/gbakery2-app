# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_date_str_for_existing_orders(apps, schema_editor):
    Order = apps.get_model("orders", "Order")

    for order in Order.objects.all():
        order._date_str = order.date.strftime("%Y-%m-%d")
        order.save()


class Migration(migrations.Migration):
    dependencies = [
        ("orders", "0008_order_cancelled"),
    ]

    operations = [
        migrations.AddField(
            model_name="order",
            name="_date_str",
            field=models.CharField(default="TEMP", max_length=256, editable=False),
            preserve_default=False,
        ),
        migrations.RunPython(set_date_str_for_existing_orders),
    ]
