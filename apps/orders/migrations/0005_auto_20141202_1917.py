# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db.models import PROTECT


class Migration(migrations.Migration):

    dependencies = [
        ("orders", "0004_auto_20141202_1553"),
    ]

    operations = [
        migrations.CreateModel(
            name="OrderFlag",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                ("slug", models.SlugField(unique=True, editable=False, blank=True)),
                ("name", models.CharField(unique=True, max_length=128)),
            ],
            options={"abstract": False,},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="OrderFlagOrder",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("updated", models.DateTimeField(auto_now=True)),
                (
                    "flag",
                    models.ForeignKey(
                        related_name="order_flag_orders",
                        to="orders.OrderFlag",
                        on_delete=PROTECT,
                    ),
                ),
                (
                    "order",
                    models.ForeignKey(
                        related_name="order_flag_orders",
                        to="orders.Order",
                        on_delete=PROTECT,
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name="orderflagorder", unique_together=set([("flag", "order")]),
        ),
        migrations.AddField(
            model_name="order",
            name="order_flags",
            field=models.ManyToManyField(
                related_name="orders",
                through="orders.OrderFlagOrder",
                to="orders.OrderFlag",
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="order",
            name="special_products_text",
            field=models.TextField(default="", editable=False, blank=True),
        ),
    ]
