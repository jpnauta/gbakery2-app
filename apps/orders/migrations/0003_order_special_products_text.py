# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("orders", "0002_auto_20141030_1318"),
    ]

    operations = [
        migrations.AddField(
            model_name="order",
            name="special_products_text",
            field=models.TextField(
                default="",
                verbose_name="all special products ordered, each separated by a new line",
            ),
            preserve_default=True,
        ),
    ]
