# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_reference_id(order):
    order.reference_id = "%s-%s" % (order.hour_string.zfill(2), order.ref_id)


def update_order_reference_id(apps, schema_editor):
    Order = apps.get_model("orders", "Order")

    for order in Order.objects.all():
        set_reference_id(order)
        order.save()


class Migration(migrations.Migration):
    dependencies = [
        ("orders", "0006_auto_20141202_2215"),
    ]

    operations = [
        migrations.RunPython(update_order_reference_id),
    ]
