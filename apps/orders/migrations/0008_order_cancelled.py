# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("orders", "0007_auto_20141213_1322"),
    ]

    operations = [
        migrations.AddField(
            model_name="order",
            name="cancelled",
            field=models.BooleanField(
                default=False, help_text="Order has been cancelled"
            ),
            preserve_default=True,
        ),
    ]
