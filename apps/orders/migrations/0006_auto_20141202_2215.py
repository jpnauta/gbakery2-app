# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("orders", "0005_auto_20141202_1917"),
    ]

    operations = [
        migrations.AddField(
            model_name="order",
            name="cannot_be_filled",
            field=models.BooleanField(default=False, editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="order",
            name="hour_string",
            field=models.CharField(max_length=2, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name="order",
            name="ref_id",
            field=models.PositiveIntegerField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name="order",
            name="reference_id",
            field=models.CharField(max_length=128, editable=False, blank=True),
        ),
    ]
