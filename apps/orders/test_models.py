from datetime import date, time, timedelta, datetime

from ddt import ddt, data, unpack
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from mock import ANY
from pytz import UTC
from rest_framework.exceptions import ValidationError

from main.ext.dates.utils import local_datetime
from main.tests import BaseTestCase
from orders.factories import (
    OrderFactory,
    FrontProductOrderProductFactory,
    SpecialProductOrderProductFactory,
)
from products.factories import FrontProductFactory


@ddt
class OrderTestCase(BaseTestCase):
    def test_it_generates_a_new_slug_on_creation(self):
        order = OrderFactory(date=date(2010, 2, 1), time=time(11, 0))

        self.assertEqual(order.reference_id, "11-1")

    def test_it_increments_slug_numbers(self):
        d = date(2010, 2, 2)
        order1 = OrderFactory(date=d, time=time(11, 0))
        order2 = OrderFactory(date=d, time=time(11, 0))

        self.assertEqual(order1.reference_id, "11-1")
        self.assertEqual(order2.reference_id, "11-2")

    def test_it_allows_the_id_for_different_dates(self):
        d = date(2010, 2, 3)
        order1 = OrderFactory(date=d, time=time(9, 0))
        order2 = OrderFactory(date=d + timedelta(days=1), time=time(9, 0))

        self.assertEqual(order1.reference_id, "09-1")
        self.assertEqual(order2.reference_id, "09-1")

    def test_it_uses_zero_when_no_time_is_set(self):
        d = date(2010, 2, 3)
        order = OrderFactory(date=d, time=None)

        self.assertEqual(order.reference_id, "00-1")

    def test_it_generates_a_new_id_if_date_changed(self):
        d = date(2010, 2, 5)
        order1 = OrderFactory(date=d, time=time(8, 0))
        order2 = OrderFactory(date=d + timedelta(days=1), time=time(8, 0))

        self.assertEqual(order1.reference_id, "08-1")
        self.assertEqual(order2.reference_id, "08-1")

        order2.date = d
        order2.save()

        self.assertEqual(order2.reference_id, "08-2")

    def test_it_keeps_the_same_id_if_date_not_changed(self):
        order = OrderFactory(date=date(2010, 2, 10), time=time(8, 0))

        self.assertEqual(order.reference_id, "08-1")

        order.save()

        self.assertEqual(order.reference_id, "08-1")

    @data(
        (date(2000, 1, 1), time(9), datetime(2000, 1, 1, 16, tzinfo=UTC)),
        (date(2000, 1, 1), None, datetime(2000, 1, 1, 7, tzinfo=UTC)),
        (date(2000, 6, 1), time(23), datetime(2000, 6, 2, 5, tzinfo=UTC)),
        (date(2000, 6, 1), None, datetime(2000, 6, 1, 6, tzinfo=UTC)),
    )
    @unpack
    def test_utc_date_tm(self, d, t, dt):
        order = OrderFactory.build(date=d, time=t)
        self.assertEqual(order.utc_date_tm, dt)

    @data(
        (date(2000, 1, 1), time(9), local_datetime(2000, 1, 1, 9)),
        (date(2000, 1, 1), None, local_datetime(2000, 1, 1)),
        (date(2000, 6, 1), time(23), local_datetime(2000, 6, 1, 23)),
        (date(2000, 6, 1), None, local_datetime(2000, 6, 1)),
    )
    @unpack
    def test_local_date_tm(self, d, t, dt):
        order = OrderFactory.build(date=d, time=t)
        self.assertEqual(order.local_date_tm, dt)


class OrderProductTestCase(BaseTestCase):
    def test_full_clean__front_product__front_product_is_required(self):
        order = FrontProductOrderProductFactory.build(front_product=None)

        with self.assertRaises(ValidationError) as e:
            order.full_clean()

        self.assertIn("'front_product': 'This field is required'", repr(e.exception))

    def test_save__special_product_with_front_product__front_product_is_removed(self):
        front_product = FrontProductFactory()
        order = OrderFactory()
        order = SpecialProductOrderProductFactory.build(
            front_product=front_product, order=order
        )

        order.save()

        self.assertEqual(order.front_product, None)

    def test_full_clean__special_product__name_is_required(self):
        order = SpecialProductOrderProductFactory.build(name=None)

        with self.assertRaises(ValidationError) as e:
            order.full_clean()

        self.assertIn("'name': 'This field is required'", repr(e.exception))

    def test_enforces_uniqueness_on_stripe_charge_id(self):
        OrderFactory(stripe_charge_id="test_charge")

        with self.assertRaises(IntegrityError):
            OrderFactory(stripe_charge_id="test_charge")

    def test_enforces_uniqueness_on_stripe_invoice_id(self):
        OrderFactory(stripe_invoice_id="test_invoice")

        with self.assertRaises(IntegrityError):
            OrderFactory(stripe_invoice_id="test_invoice")
