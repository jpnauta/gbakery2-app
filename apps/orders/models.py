from __future__ import unicode_literals

import re
from datetime import datetime, time
from decimal import Decimal

import phonenumbers
import stripe
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Max, PROTECT, CASCADE, SET_NULL
from django.utils.encoding import force_text
from django.utils.translation import ugettext as _
from pytz import UTC
from rest_framework.exceptions import ValidationError
from stripe.error import InvalidRequestError

from main.constants import DATE_FORMAT
from main.emails import send_templated_message
from main.ext.dates import TIME_ZONE_PYTZ
from main.ext.dates.utils import date_to_datetime, localize, to_timezone, local_tz_now
from main.ext.strings.utils import clean_whitespace
from main.logging import logger
from main.managers import AppManager
from main.models import (
    BaseModel,
    Activated,
    Slugged,
    UniqueProperName,
    CustomizablePrice,
)
from main.models.fields import CurrencyField
from main.utils.generic import django_choice_options
from products.models import (
    FrontProduct,
    FrontProductNotReady,
    QUANTITY_TYPES,
    QUANTITY_TYPE_OPTIONS,
    FrontProductPrice,
)
from settings import (
    FULL_DASH_SITE_URL,
    ORDER_CONFIRMATION_DASHBOARD_SENDGRID_ID,
    SUPPORT_EMAIL,
)
from .managers import OrderQuerySet, OrderProductQuerySet, OrderFlagQuerySet

(OP_TYPE_FRONT_PRODUCT, OP_TYPE_SPECIAL_PRODUCT,) = (
    "front_product",
    "special_product",
)
OP_TYPE_OPTIONS = {
    OP_TYPE_FRONT_PRODUCT: {"name": _("Front Product"),},
    OP_TYPE_SPECIAL_PRODUCT: {"name": _("Special Product"),},
}

OP_TYPES = django_choice_options(OP_TYPE_OPTIONS, "name")


# Payment statuses
(
    PAYMENT_STATUS_PAYMENT_PENDING,
    PAYMENT_STATUS_PAID,
    PAYMENT_STATUS_MARKED_PAID,
    PAYMENT_STATUS_UNPAID,
) = (
    "payment_pending",
    "paid",
    "marked_as_paid",
    "unpaid",
)
PAYMENT_STATUS_TYPE_OPTIONS = {
    PAYMENT_STATUS_PAYMENT_PENDING: {"name": _("Payment Pending")},
    PAYMENT_STATUS_PAID: {"name": _("Paid"),},
    PAYMENT_STATUS_MARKED_PAID: {"name": _("Marked as Paid")},
    PAYMENT_STATUS_UNPAID: {"name": _("Unpaid")},
}

PAYMENT_STATUS_TYPES = django_choice_options(OP_TYPE_OPTIONS, "name")


class OrderLocation(BaseModel):
    description = models.CharField(max_length=64, unique=True)
    hotkey = models.CharField(max_length=2, unique=True)

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return "%s (%s)" % (self.description, self.hotkey)


class OrderCollection(BaseModel):
    pass


class Order(CustomizablePrice, Activated, BaseModel):
    (TYPE_REGULAR, TYPE_CAKE,) = (
        1,
        2,
    )
    TYPE_OPTIONS = {
        TYPE_REGULAR: {"name": _("Regular"),},
        TYPE_CAKE: {"name": _("Cake"),},
    }
    TYPES = django_choice_options(TYPE_OPTIONS, "name")

    DATE_STR_FORMAT = "%Y-%m-%d"

    # Customer info
    name = models.CharField(
        _("customer name"), max_length=128, help_text=_("Customer's full name")
    )
    phone_number = models.CharField(
        _("customer phone number"),
        max_length=128,
        null=True,
        blank=True,
        help_text=_("Customer's phone number"),
    )
    email = models.EmailField(
        _("customer email"),
        max_length=128,
        null=True,
        blank=True,
        help_text=_("Customer's email address"),
    )

    # Basic info
    collection = models.ForeignKey(OrderCollection, blank=True, on_delete=PROTECT)
    type = models.IntegerField(_("order type"), choices=TYPES, default=TYPE_REGULAR)
    date = models.DateField(_("pickup date"), db_index=True)
    time = models.TimeField(_("pickup time"), null=True, blank=True)

    # Order state info
    filled = models.BooleanField(
        default=False, help_text=_("Order is currently being filled")
    )
    picked_up = models.BooleanField(
        default=False, help_text=_("Order has been picked up")
    )
    in_progress = models.BooleanField(
        default=False, help_text=_("Order is currently being packed")
    )
    verified = models.BooleanField(
        default=False, help_text=_("Order has been verified")
    )
    cancelled = models.BooleanField(
        default=False, help_text=_("Order has been cancelled")
    )
    marked_as_paid = models.BooleanField(
        default=False, help_text=_("Order has been manually marked as paid")
    )

    # Reference ID & metadata
    hour_string = models.CharField(max_length=2, blank=True, editable=False)
    ref_id = models.PositiveIntegerField(
        blank=True, editable=False
    )  # Unique with respect to the date & hour of order
    reference_id = models.CharField(max_length=128, blank=True, editable=False)
    cannot_be_filled = models.BooleanField(default=False, editable=False)

    # Order info
    comment = models.TextField(_("comments/details"), null=True, blank=True)
    location = models.ForeignKey(
        OrderLocation,
        null=True,
        blank=True,
        help_text=_("Where the order is located (once filled)"),
        related_name="orders",
        on_delete=SET_NULL,
    )
    num_boxes = models.IntegerField(_("# of boxes"), null=True, blank=True)

    # Derived data
    _date_str = models.CharField(
        max_length=256, editable=False
    )  # String representation of date

    front_products = models.ManyToManyField(
        FrontProduct, through="orders.OrderProduct", related_name="orders"
    )
    order_flags = models.ManyToManyField(
        "orders.OrderFlag", through="orders.OrderFlagOrder", related_name="orders"
    )

    last_amount_invoiced = CurrencyField(
        help_text=_("Amount that was last invoiced to user, in CAD"),
        null=True,
        blank=True,
        editable=False,
        validators=[MinValueValidator(Decimal("0")),],
    )

    stripe_customer_id = models.CharField(
        max_length=1024,
        editable=False,
        help_text="ID reference to customer object on Stripe",
        null=True,
        blank=True,
    )
    stripe_invoice_id = models.CharField(
        max_length=1024,
        editable=False,
        help_text="ID reference to invoice object on Stripe",
        null=True,
        blank=True,
        unique=True,
    )
    stripe_charge_id = models.CharField(
        max_length=1024,
        editable=False,
        help_text="ID reference charge to customer's credit on Stripe",
        null=True,
        blank=True,
        unique=True,
    )
    stripe_charge_status = models.CharField(
        max_length=256,
        choices=PAYMENT_STATUS_TYPES,
        editable=False,
        help_text="The payment status of the order according to Stripe. Automatically updated when status is changed on Stripe",
        null=True,
        blank=True,
    )

    objects = AppManager.from_queryset(OrderQuerySet)()

    def _set_default_collection(self):
        if not self.has_foreign_key("collection"):
            collection = OrderCollection()
            collection.save()
            self.collection = collection

    @property
    def local_date_tm_start(self) -> datetime:
        """
        :return: datetime in MST/MDT of the order date at midnight
        """
        return localize(date_to_datetime(self.date), TIME_ZONE_PYTZ)

    @property
    def local_date_tm(self) -> datetime:
        """
        :return: datetime in MST/MDT of the date and time of the order. If no time is specified, the
        time will be midnight.
        """
        d = self.local_date_tm_start
        t = self.time if self.time is not None else time(0)

        return datetime(d.year, d.month, d.day, t.hour, t.minute, tzinfo=d.tzinfo)

    @property
    def utc_date_tm(self) -> datetime:
        """
        :return: datetime in UTC of the date and time of the order. If no time is specified, the
        time will be midnight in MST/MDT.
        """
        return to_timezone(self.local_date_tm, UTC)

    @property
    def price_cents(self) -> int:
        return CurrencyField.to_cents(self.price)

    @property
    def user_friendly_date_str(self) -> str:
        return self.date.strftime("%Y-%m-%d")

    @property
    def user_friendly_time_str(self) -> str:
        if self.time is not None:
            return self.time.strftime("%-I%p")
        return "(Unknown)"

    @property
    def _hour_string(self):
        """
        Retrieves the hour which the order is created, or '0' if
        none set. Used for generating reference_id
        """
        if self.time:
            return repr(self.time.hour)
        else:
            return "0"

    @property
    def stripe_invoice_link(self):
        if self.stripe_invoice_id is not None:
            return f"https://dashboard.stripe.com/invoices/{self.stripe_invoice_id}"
        return None

    @property
    def stripe_charge_link(self):
        if self.stripe_charge_id is not None:
            return f"https://dashboard.stripe.com/charges/{self.stripe_charge_id}"
        return None

    @property
    def edit_url(self):
        return f"{FULL_DASH_SITE_URL}/orders/{self.id}/edit"

    @property
    def stripe_metadata(self):
        return dict(
            order_id=self.id,
            order_reference_id=self.reference_id,
            order_date=self.user_friendly_date_str,
            order_time=self.user_friendly_time_str,
            order_edit_url=self.edit_url,
        )

    def _set_hour_string(self):
        self.hour_string = self._hour_string

    def _set_ref_id(self):
        qs = Order.objects.for_date(self.date).for_hour_string(self.hour_string)

        last_id = qs.aggregate(Max("ref_id"))["ref_id__max"] or 0
        self.ref_id = last_id + 1

    def _check_set_ref_id(self):
        if self.has_foreign_key("date"):
            if self.has_foreign_key("ref_id"):  # On update, update if slug conflict
                # Find any conflicts
                conflicts = Order.objects.filter(
                    ref_id=self.ref_id, date=self.date, hour_string=self.hour_string
                ).exclude(id=self.id)

                if (
                    conflicts.count() > 0
                ):  # If any conflicts found, regenerate reference
                    self._set_ref_id()
            else:  # On create, set reference regardless
                self._set_ref_id()

    def _check_order_state(self):
        # If order has been filled, it is no longer in progress of being filled
        if self.filled:
            self.in_progress = False

    def _set_reference_id(self):
        self.reference_id = "%s-%s" % (self.hour_string.zfill(2), self.ref_id)

    def _set_cannot_be_filled(self, force_cannot_be_filled=False):
        fp_not_ready = FrontProductNotReady.objects.filter(
            date=self.date, front_product__orders=self
        )

        self.cannot_be_filled = not self.filled and (
            force_cannot_be_filled or fp_not_ready.exists()
        )

    def _set_date_str(self):
        if self.date:
            self._date_str = self.date.strftime(self.DATE_STR_FORMAT)

    def save(self, *args, **kwargs):
        force_cannot_be_filled = kwargs.pop("force_cannot_be_filled", False)

        self._set_default_collection()
        self._set_hour_string()
        self._check_set_ref_id()
        self._set_reference_id()
        self._check_order_state()
        self._set_cannot_be_filled(force_cannot_be_filled=force_cannot_be_filled)
        self._set_date_str()

        # Temporarily replace timestamp with time string - fixes swampdragon bug
        _time = self.time
        self.time = _time.strftime("%H:%M") if _time else None
        super().save(*args, **kwargs)
        self.time = _time

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.name

    def formatted_phone_number(self):
        if self.phone_number:
            number = phonenumbers.PhoneNumber(
                country_code=1, national_number=self.phone_number
            )
            return phonenumbers.format_number(
                number, phonenumbers.PhoneNumberFormat.NATIONAL
            )

    @classmethod
    def is_order_reference_id(cls, string):
        """
        Indicates whether or not the given string has the format of a typical
        order reference ID
        """
        return re.match(r"^\d\d-\d+$", string or "")

    def _set_cancelled_comment(self):
        new_comment = "CANCELLED"

        if self.order_products.exists():
            product_list = self.order_products.all().product_str()
            new_comment = "%s - %s" % (new_comment, product_list)

        if self.comment:
            new_comment = "%s - %s" % (new_comment, self.comment)

        self.comment = new_comment

    def cancel(self):
        """
        Performs the appropriate tasks when an order is cancelled
        """
        if not self.cancelled:
            if self.stripe_charge_id is not None:
                raise ValidationError(
                    "Stripe charge must be refunded before being cancelled"
                )

            if self.stripe_invoice_id is not None:
                raise ValidationError(
                    "Stripe invoice must be voided before being cancelled"
                )

            self._set_cancelled_comment()
            self.verified = (
                True  # No need to verify this order since it's been cancelled
            )
            self.cancelled = True

            self.order_products.all().delete()  # Remove all products on order

        self.save()

    @classmethod
    def clean_order_cancelled(cls, product_count):
        if product_count > 0:
            raise ValidationError(
                _(
                    "Cancelled orders cannot have products! Please create a new order instead."
                )
            )

    def _clean_order_cancelled(self):
        if self.cancelled:
            self.clean_order_cancelled(self.order_products.count())

    def clean(self):
        self._clean_order_cancelled()

    def get_stripe_customer(self) -> stripe.Customer:
        return stripe.Customer.retrieve(self.stripe_customer_id)

    def get_stripe_invoice(self) -> stripe.Invoice:
        if self.stripe_invoice_id is None:
            raise ValidationError("Invoice has not been sent")

        return stripe.Invoice.retrieve(self.stripe_invoice_id)

    def get_stripe_charge(self) -> stripe.Charge:
        if self.stripe_charge_id is None:
            raise ValidationError("Charge was never created")

        return stripe.Charge.retrieve(self.stripe_charge_id)

    def _stripe_due_date_timestamp(self):
        return int(self.utc_date_tm.timestamp())

    def get_or_create_stripe_customer(self):
        """
        Attempts to fetch to original customer, otherwise
        creates a new one
        """
        # Attempt to get original customer
        customer = None
        if self.stripe_customer_id is not None:
            try:
                customer = stripe.Customer.retrieve(self.stripe_customer_id)
            except InvalidRequestError as e:
                # Customer does not exist
                logger.warning(e, exc_info=True)

        # Create new customer as needed
        if customer is None or customer.get("deleted"):
            customer = stripe.Customer.create(
                email=self.email,
                name=f"{self.name}",
                description=f"{self.name} - {self.date.strftime(DATE_FORMAT)}",
            )

        self.stripe_customer_id = customer.stripe_id

        return customer

    def validate_create_stripe_invoice(self):
        """
        Performs validations to prepare for invoice creation
        """
        # Validate data
        self.force_require_fields(["email", "price"])

        if self.local_date_tm < local_tz_now():
            raise ValidationError(
                "Cannot invoice customer for an order date in the past"
            )

    def clear_stripe_invoice(self):
        """
        Clears the current stripe invoice info. Used when a Stripe invoice
        is voided or refunded
        """
        self.stripe_invoice_id = None
        self.stripe_charge_id = None
        self.stripe_charge_status = None

    def refund_stripe_charge(self):
        """
        Refunds the invoice. If the invoice is in a state that cannot be
        handled, a ValidationError will be thrown.
        """
        if self.stripe_charge_id is None:
            raise ValidationError(
                "Cannot refund an order if the customer's credit card was never charged"
            )

        stripe_charge = self.get_stripe_charge()

        stripe.Refund.create(charge=stripe_charge.stripe_id)

        # Clear old invoice ID to prepare for new one
        self.clear_stripe_invoice()

    def void_stripe_invoice(self):
        """
        Voids the invoice. If the invoice is in a state that cannot be
        handled, a ValidationError will be thrown.
        """
        stripe_invoice = self.get_stripe_invoice()

        if stripe_invoice.charge is not None:
            raise ValidationError(
                "Cannot void an order if the customer's credit card was charged"
            )

        if stripe_invoice.status in ("void", "draft"):
            # Do nothing - invoice is already voided
            pass
        elif stripe_invoice.status == "open":
            # Invoice is pending -> void it
            stripe.Invoice.void_invoice(stripe_invoice.id)
        elif stripe_invoice.status == "paid":
            # Invoice was paid out of band; do nothing
            pass
        else:
            raise ValidationError(
                f"Current status is in invalid status `{stripe_invoice.status}`. "
                f"Please change this status manually and try again."
            )

        # Clear old invoice ID to prepare for new one
        self.clear_stripe_invoice()

    def create_stripe_invoice(self):
        """
        Creates an invoice for this order on Stripe
        """
        if self.stripe_invoice_id is not None:
            raise ValidationError(
                "Invoice must be voided before creating a new invoice"
            )

        if self.stripe_charge_id is not None:
            raise ValidationError(
                "Charge must be refunded before creating a new invoice"
            )

        if self.marked_as_paid:
            raise ValidationError(
                "Cannot send invoice for an order that is marked as paid"
            )

        # Create stripe objects
        stripe_customer = self.get_or_create_stripe_customer()
        for order_product in self.order_products:
            stripe.InvoiceItem.create(
                customer=stripe_customer,
                unit_amount=0,
                currency="cad",
                description=order_product.customer_friendly_description,
                quantity=order_product.quantity,
            )
        stripe.InvoiceItem.create(
            customer=stripe_customer,
            amount=self.price_cents,
            currency="cad",
            description=f"Total items price",
        )
        stripe_invoice = stripe.Invoice.create(
            customer=stripe_customer,
            collection_method="send_invoice",
            due_date=self._stripe_due_date_timestamp(),
            metadata=self.stripe_metadata,
        )

        # Invoice customer
        stripe.Invoice.send_invoice(stripe_invoice.stripe_id)

        # Save data to model
        self.stripe_customer_id = stripe_customer.stripe_id
        self.stripe_invoice_id = stripe_invoice.stripe_id
        self.stripe_charge_id = stripe_invoice.charge
        self.stripe_charge_status = PAYMENT_STATUS_PAYMENT_PENDING
        self.last_amount_invoiced = self.price

    @property
    def purchase_status(self) -> str:
        if self.marked_as_paid:
            return PAYMENT_STATUS_MARKED_PAID
        if self.stripe_charge_status is not None:
            return self.stripe_charge_status
        return PAYMENT_STATUS_UNPAID

    @property
    def purchase_status_text(self):
        return PAYMENT_STATUS_TYPE_OPTIONS.get(self.purchase_status, dict()).get("name")

    def validate_marked_as_paid(self):
        """
        Should be called whenever order is saved and `marked_as_paid` is set
        to True
        """
        if self.stripe_charge_status == PAYMENT_STATUS_PAYMENT_PENDING:
            raise ValidationError(
                "Cannot mark order as paid when it has a payment pending; "
                "please go to order's Stripe invoice and mark the invoice paid instead."
            )

        if self.stripe_charge_status == PAYMENT_STATUS_PAID:
            raise ValidationError(
                "Cannot mark order as paid when it has been paid via Stripe."
            )

    def mark_as_paid(self):
        if self.marked_as_paid:
            raise ValidationError("Order has already been marked as paid")

        self.validate_marked_as_paid()

        self.marked_as_paid = True

    def unmark_as_paid(self):
        if not self.marked_as_paid:
            raise ValidationError("Order has not been marked as paid")

        self.marked_as_paid = False

    def mark_invoice_as_paid(self):
        stripe.Invoice.pay(self.stripe_invoice_id, paid_out_of_band=True)
        self.stripe_charge_status = PAYMENT_STATUS_PAID

    class Meta:
        unique_together = (
            ("date", "hour_string", "ref_id",),
            ("date", "reference_id",),
        )
        ordering = ("name",)

    @classmethod
    def calculate_order_price(cls, attrs: dict):
        # Attempt to derive price from order products
        calculated_price = Decimal("0")

        for op in attrs["order_products"]:
            op_price = (
                op["custom_price"]
                if op.get("custom_price") is not None
                else op["calculated_price"]
            )

            if op_price is None:
                calculated_price = None
                break
            else:
                calculated_price += op_price * op["quantity"]
        attrs["calculated_price"] = calculated_price

        return calculated_price

    def order_confirmation_email_data(self):
        """
        :return: Template data for order confirmation email
        """
        return dict(
            id=self.id,
            date=self.date.strftime("%A, %b %-d, %Y"),
            time=self.time.strftime("%-I%p"),
            name=self.name,
            price=str(self.price),
            order_products=[
                dict(description=op.description, price=str(op.price),)
                for op in self.order_products.all()
            ],
        )

    def send_order_confirmation_email(self):
        """
        Sends an order confirmation to the user
        """
        if self.email is None:
            raise RuntimeError("Email required to send order confirmation")

        send_templated_message(
            ORDER_CONFIRMATION_DASHBOARD_SENDGRID_ID,
            dict(order=self.order_confirmation_email_data()),
            to=[self.email, SUPPORT_EMAIL],
            reply_to=[SUPPORT_EMAIL],
        )


class OrderProduct(CustomizablePrice, BaseModel):
    filled = models.BooleanField(default=False, help_text=_("Product has been filled"))
    order = models.ForeignKey(Order, related_name="order_products", on_delete=CASCADE)
    front_product = models.ForeignKey(
        FrontProduct,
        related_name="order_products",
        on_delete=CASCADE,
        blank=True,
        null=True,
    )
    front_product_price = models.ForeignKey(
        FrontProductPrice,
        related_name="order_products",
        on_delete=CASCADE,
        blank=True,
        null=True,
    )
    quantity = models.PositiveIntegerField()
    quantity_type = models.IntegerField(choices=QUANTITY_TYPES)
    product_type = models.CharField(
        choices=OP_TYPES, max_length=64, default=OP_TYPE_FRONT_PRODUCT, null=False
    )

    # Derived data
    total_quantity = models.PositiveIntegerField(
        blank=True, editable=False,
    )  # Auto set, = quantity * quantity_type.quantity
    name = models.CharField(
        max_length=128, blank=True, help_text="Required special products only",
    )
    description = models.CharField(
        max_length=1024,
        blank=True,
        editable=False,
        help_text="E.g. '1 doz. cheese buns'",
    )

    objects = AppManager.from_queryset(OrderProductQuerySet)()

    @property
    def quantity_type_quantity(self):
        return QUANTITY_TYPE_OPTIONS[self.quantity_type]["quantity"]

    @property
    def quantity_type_name(self):
        return QUANTITY_TYPE_OPTIONS[self.quantity_type]["name"]

    @property
    def quantity_type_short_name(self):
        return QUANTITY_TYPE_OPTIONS[self.quantity_type]["short_name"]

    @property
    def quantity_type_short_name_2(self):
        return QUANTITY_TYPE_OPTIONS[self.quantity_type]["short_name_2"]

    @property
    def customer_friendly_description(self):
        """
        Description used to describe products purchased by customers
        """
        return f"{self.name} ({self.quantity_type_name})"

    @property
    def total_calculated_price(self):
        """
        Description used to describe products purchased by customers
        """
        if self.calculated_price is not None:
            return self.quantity * self.calculated_price
        return None

    def _set_total_quantity(self):
        self.total_quantity = self.quantity * self.quantity_type_quantity

    def _set_description(self):
        if self.has_foreign_key("quantity"):
            if self.product_type == OP_TYPE_FRONT_PRODUCT:
                self.name = force_text(self.front_product)
            desc = "%s %s %s" % (
                self.quantity,
                self.quantity_type_short_name_2,
                self.name,
            )
            self.description = clean_whitespace(desc)

    def _set_front_product(self):
        # If special product, for front_product to None
        if self.product_type == OP_TYPE_SPECIAL_PRODUCT:
            self.front_product = None

    def is_filled(self):
        return self.filled or self.order.filled

    def is_starred(self):
        return self.product_type == OP_TYPE_SPECIAL_PRODUCT or bool(
            self.front_product and self.front_product.is_starred
        )

    def is_picked_up(self):
        return self.order.picked_up

    def clean_fields(self, exclude=None):
        if self.product_type == OP_TYPE_FRONT_PRODUCT:
            # Required fields for front products
            self.force_require_fields(["front_product"])
        elif self.product_type == OP_TYPE_SPECIAL_PRODUCT:
            # Required fields for special products
            self.force_require_fields(["name"])

        return super().clean_fields(exclude=exclude)

    def save(self, *args, **kwargs):
        self._set_total_quantity()
        self._set_description()
        self._set_front_product()
        super().save()

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.description

    class Meta:
        verbose_name = _("order product")
        ordering = ("-id",)


class OrderFlag(BaseModel, Slugged, UniqueProperName):
    objects = AppManager.from_queryset(OrderFlagQuerySet)()

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def __repr__(self):
        return self.name


class OrderFlagOrder(BaseModel):
    flag = models.ForeignKey(
        OrderFlag, related_name="order_flag_orders", on_delete=CASCADE
    )
    order = models.ForeignKey(
        Order, related_name="order_flag_orders", on_delete=CASCADE
    )

    class Meta:
        unique_together = (("flag", "order",),)
