from django.utils.safestring import mark_safe

from main.admin import (
    BaseModelAdmin,
    BaseTabularInline,
    deregister_unnecessary_admin_pages,
)
from main.admin.filters import by_null_filter
from orders.admin.forms import OrderProductInlineFormset, OrderFlagOrderInlineFormset
from orders.models import (
    Order,
    OrderLocation,
    OrderFlagOrder,
    OrderFlag,
    OrderProduct,
    PAYMENT_STATUS_TYPE_OPTIONS,
)


class OrderLocationAdmin(BaseModelAdmin):
    model = OrderLocation

    list_display = (
        "description",
        "hotkey",
    )
    list_filter = ("created",)
    search_fields = (
        "description",
        "hotkey",
    )


class OrderProductInline(BaseTabularInline):
    formset = OrderProductInlineFormset
    model = OrderProduct


class OrderFlagOrderInline(BaseTabularInline):
    formset = OrderFlagOrderInlineFormset
    model = OrderFlagOrder


class OrderAdmin(BaseModelAdmin):
    model = Order

    list_display = (
        "name",
        "phone_number",
        "email",
        "created",
        "date",
        "time",
        "filled",
        "picked_up",
        "in_progress",
        "stripe_invoice",
        "stripe_charge",
        "_stripe_charge_status",
    )
    exclude = (
        "hour_string",
        "ref_id",
        "reference_id",
        "collection",
    )
    list_filter = (
        "date",
        "filled",
        "picked_up",
        "in_progress",
        by_null_filter("stripe_charge_id", "Is paid via Stripe", "No", "Yes"),
        by_null_filter("stripe_invoice_id", "Is invoiced via Stripe", "No", "Yes"),
    )
    search_fields = (
        "name",
        "email",
        "stripe_invoice_id",
        "stripe_charge_id",
    )
    inlines = (
        OrderProductInline,
        OrderFlagOrderInline,
    )

    def stripe_invoice(self, obj):
        if obj.stripe_invoice_id is not None:
            return mark_safe(
                f'<a href="{obj.stripe_invoice_link}" target="_blank">{obj.stripe_invoice_id}</a>'
            )
        return "-"

    def stripe_charge(self, obj):
        if obj.stripe_charge_id is not None:
            return mark_safe(
                f'<a href="{obj.stripe_charge_link}" target="_blank">{obj.stripe_charge_id}</a>'
            )
        return "-"

    def _stripe_charge_status(self, obj):
        # Don't know why, but the default admin property doesn't show anything
        return PAYMENT_STATUS_TYPE_OPTIONS.get(obj.stripe_charge_status, dict()).get(
            "name"
        )


class OrderFlagAdmin(BaseModelAdmin):
    model = OrderFlag

    search_fields = ("name",)


OrderAdmin.register()
OrderLocationAdmin.register()
OrderFlagAdmin.register()

# This is not the most elegant place to do this, but at least it works
deregister_unnecessary_admin_pages()
