from datetime import timedelta
from string import ascii_letters

import factory

from main.ext.dates.utils import local_tz_now
from main.factories import BaseModelFactory
from orders.models import (
    Order,
    OrderFlag,
    OrderFlagOrder,
    OrderLocation,
    OrderProduct,
    OP_TYPE_FRONT_PRODUCT,
    OP_TYPE_SPECIAL_PRODUCT,
    PAYMENT_STATUS_PAYMENT_PENDING,
    PAYMENT_STATUS_PAID,
)
from products.factories import FrontProductFactory
from products.models import QUANTITY_INDIVIDUAL


class BaseOrderFactory(BaseModelFactory):
    date = factory.LazyAttribute(lambda x: (local_tz_now() + timedelta(days=1)).date())
    name = u"Test Order"
    calculated_price = 42

    class Meta:
        model = Order


class OrderFactory(BaseOrderFactory):
    pass


class InvoicableOrderFactory(BaseOrderFactory):
    """
    Creates order that can be used to create Stripe invoice
    """

    email = "user@glamorganbakery.com"
    calculated_price = "12.25"


class InvoicedOrderFactory(InvoicableOrderFactory):
    """
    Simulates an order that has an invoice generated via Stripe
    """

    stripe_customer_id = "fake_stripe_customer_id"
    stripe_invoice_id = "fake_stripe_invoice_id"
    stripe_charge_status = PAYMENT_STATUS_PAYMENT_PENDING
    last_amount_invoiced = "12.25"


class OnlineOrderFactory(InvoicableOrderFactory):
    """
    Simulates an order that has been purchased via the online checkout page
    """

    stripe_customer_id = "fake_stripe_customer_id"
    stripe_charge_id = "fake_stripe_charge_id"
    stripe_invoice_id = None
    stripe_charge_status = PAYMENT_STATUS_PAID
    last_amount_invoiced = "13.25"


class OrderFlagFactory(BaseModelFactory):
    name = factory.Sequence(lambda x: "Order flag %s" % x)

    class Meta:
        model = OrderFlag


class OrderFlagOrderFactory(BaseModelFactory):
    order = factory.SubFactory(OrderFactory)
    flag = factory.SubFactory(OrderFlagFactory)

    class Meta:
        model = OrderFlagOrder


class OrderLocationFactory(BaseModelFactory):
    description = factory.Sequence(lambda x: "Order location %s" % x)
    hotkey = factory.Sequence(
        lambda n: ascii_letters[n] + ascii_letters[n % len(ascii_letters)]
    )

    class Meta:
        model = OrderLocation


class BaseOrderProductFactory(BaseModelFactory):
    filled = False
    quantity = 3
    quantity_type = QUANTITY_INDIVIDUAL

    class Meta:
        model = OrderProduct


class FrontProductOrderProductFactory(BaseOrderProductFactory):
    front_product = factory.SubFactory(FrontProductFactory)
    product_type = OP_TYPE_FRONT_PRODUCT


class SpecialProductOrderProductFactory(BaseOrderProductFactory):
    product_type = OP_TYPE_SPECIAL_PRODUCT
    name = factory.Sequence(lambda x: "Special product %s" % x)
