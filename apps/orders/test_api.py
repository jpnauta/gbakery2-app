from datetime import datetime, timedelta, time, date
from decimal import Decimal
from time import sleep
from unittest import mock
from unittest.mock import patch, MagicMock

import stripe
from ddt import data, ddt, unpack
from freezegun import freeze_time
from pytz import UTC
from stripe.error import StripeError, APIConnectionError, InvalidRequestError

from main.ext.dates import TIME_ZONE_PYTZ
from main.ext.dates.utils import (
    local_tz_now,
    start_of_date,
    to_timezone,
    local_datetime,
)
from main.testing.assertions import assert_statuses_are_met
from main.testing.decorators import bypass_stripe_webhook_signature_check
from main.tests import BaseAPITestCase
from orders.api.factories import DashboardOrderRequestFactory, PublicOrderRequestFactory
from orders.factories import (
    OrderFactory,
    OrderFlagOrderFactory,
    OrderFlagFactory,
    OrderLocationFactory,
    FrontProductOrderProductFactory,
    SpecialProductOrderProductFactory,
    InvoicableOrderFactory,
    InvoicedOrderFactory,
    OnlineOrderFactory,
)
from orders.models import (
    Order,
    OP_TYPE_FRONT_PRODUCT,
    OP_TYPE_SPECIAL_PRODUCT,
    OrderFlagOrder,
    PAYMENT_STATUS_PAYMENT_PENDING,
    PAYMENT_STATUS_PAID,
    OrderProduct,
)
from products.factories import FrontProductFactory, FrontProductPriceFactory
from products.models import QUANTITY_DOZEN, QUANTITY_HALF_DOZEN, QUANTITY_INDIVIDUAL
from schedule.models import ClosingRule, ScheduleSettings
from settings import ORDER_CONFIRMATION_DASHBOARD_SENDGRID_ID


class DashboardOrderViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/orders/"
    stripe_webhook_url = "/api/public/stripe/webhooks/handler/"

    def setUp(self):
        # Delete existing orders
        Order.objects.all().delete()
        self.user = self.login_basic_user()
        super().setUp()

    def pay_order_by_credit_card(self, order: Order, source: str = None):
        if source is None:
            source = "tok_visa"

        stripe.Customer.create_source(
            order.stripe_customer_id, source=source,
        )
        stripe.Invoice.pay(order.stripe_invoice_id)

    def create_basic_invoiced_order(self, data=None, **params):
        order = InvoicableOrderFactory(**params)

        # AND
        self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), data=data
        ).json()

        return order.reload()

    def edit_and_resend_invoice(self, order, price, data=None):
        # WHEN
        order.custom_price = price
        order.save()

        # AND
        self.make_request(
            "PUT", self.get_obj_url(order.id, "void-invoice/"), data=data
        ).json()

        # AND
        self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), data=data
        ).json()

        return order.reload()

    def find_recent_stripe_event(
        self, expected_event_type: str, expected_stripe_object_id: str
    ):
        for _ in range(0, 20):  # Try several times to find the relevant event
            events = stripe.Event.list(limit=10)["data"]

            event_data = [(event, event.type, event.data.object.id) for event in events]
            for event, event_type, stripe_invoice_id in event_data:
                # Ensure the event retrieve from Stripe is the type expected
                if (
                    event_type == expected_event_type
                    and stripe_invoice_id == expected_stripe_object_id
                ):
                    return event

            sleep(1)

        self.fail(
            f"Could not find recent Stripe event matching type={expected_event_type}, "
            f"invoice_id={expected_stripe_object_id}\nfound recent events: {event_data}"
        )

    def simulate_webhook_call(self, event: stripe.Event):
        with bypass_stripe_webhook_signature_check() as m:
            self.make_request(
                "POST",
                self.stripe_webhook_url,
                data=event,
                HTTP_STRIPE_SIGNATURE="test_signature",
            ).json()

        self.assertEqual(m.call_count, 1, "Webhook validation was never called")

    def test_list__filter_by_date__is_accepted(self):
        d = datetime.today().date()
        order = OrderFactory(date=d)
        before_order = OrderFactory(date=d + timedelta(days=1))
        after_order = OrderFactory(date=d - timedelta(days=1))

        response = self.make_request(
            "GET", self.get_list_url(), query_params=dict(date=d, page_size=10)
        )
        page = response.json()

        self.assertPaginationResultsMatch(page, [order.id])

    def test_list__filter_by_picked_up__is_accepted(self):
        o1 = OrderFactory(picked_up=False)
        o2 = OrderFactory(picked_up=True)

        page = self.make_request(
            "GET", self.get_list_url(), query_params=dict(picked_up=False, page_size=10)
        ).json()

        self.assertPaginationResultsMatch(page, [o1.id])

    def test_list__filter_by_verified__is_accepted(self):
        o1 = OrderFactory(verified=False)
        o2 = OrderFactory(verified=True)

        query_params = dict(verified=False, page_size=10)
        page = self.make_request(
            "GET", self.get_list_url(), query_params=query_params
        ).json()

        self.assertPaginationResultsMatch(page, [o1.id])

    def test_list__filter_by_filled__is_accepted(self):
        o1 = OrderFactory(filled=False)
        o2 = OrderFactory(filled=True)

        page = self.make_request(
            "GET", self.get_list_url(), query_params=dict(filled=False, page_size=10)
        ).json()

        self.assertPaginationResultsMatch(page, [o1.id])

    def test_list__filter_by_time_isnull__is_accepted(self):
        o1 = OrderFactory(time=time(8, 0))
        o2 = OrderFactory(time=None)

        query_params = dict(time__isnull=False, page_size=10)
        page = self.make_request(
            "GET", self.get_list_url(), query_params=query_params
        ).json()

        self.assertPaginationResultsMatch(page, [o1.id])

    def test_list__filter_by_order_flags__is_accepted(self):
        flag = OrderFlagFactory()
        o1 = OrderFactory()
        OrderFlagOrderFactory(flag=flag, order=o1)
        o2 = OrderFactory()

        query_params = dict(order_flags=flag.id, page_size=10)
        page = self.make_request(
            "GET", self.get_list_url(), query_params=query_params
        ).json()

        self.assertPaginationResultsMatch(page, [o1.id])

    def test_list__filter_by_cancelled__is_accepted(self):
        o1 = OrderFactory()
        OrderFactory(cancelled=True)

        query_params = dict(cancelled=False, page_size=10)
        page = self.make_request(
            "GET", self.get_list_url(), query_params=query_params
        ).json()

        self.assertPaginationResultsMatch(page, [o1.id])

    def test_list__search_by_order_flag__returns_exact_match(self):
        o1 = OrderFactory()
        o2 = OrderFactory(reference_id=o1.reference_id + "0")

        query_params = dict(q=o1.reference_id, page_size=10)
        page = self.make_request(
            "GET", self.get_list_url(), query_params=query_params
        ).json()

        self.assertPaginationResultsMatch(page, [o1.id])

    def test_create__order_with_no_products__is_accepted(self):
        d = datetime.now().date()
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=DashboardOrderRequestFactory(date=d, name="Test Order", num_boxes=4),
        ).json()

        order = Order.objects.get(id=data["id"])
        self.assertEqual(order.name, "Test Order")
        self.assertEqual(order.date, d)
        self.assertEqual(order.num_boxes, 4)

    def test_create__order_with_front_product_and_special_product__is_accepted(self):
        # GIVEN
        d = datetime.now().date()
        front_product = FrontProductFactory(name="Front Product", is_starred=True)

        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=dict(
                date=d,
                name="Test Order",
                order_products=[
                    dict(
                        product_type=OP_TYPE_FRONT_PRODUCT,
                        front_product=front_product.id,
                        quantity=3,
                        quantity_type=QUANTITY_DOZEN,
                    ),
                    dict(
                        product_type=OP_TYPE_SPECIAL_PRODUCT,
                        quantity=2,
                        quantity_type=QUANTITY_DOZEN,
                        name="Special Product",
                    ),
                ],
            ),
        ).json()

        # THEN
        order = Order.objects.get(id=data["id"])
        self.assertEqual(len(data["order_products"]), 2)

        self.assertDictContainsSubset(
            dict(
                product_type=OP_TYPE_FRONT_PRODUCT,
                name="Front Product",
                description="3 doz. Front Product",
                quantity=3,
                quantity_type=QUANTITY_DOZEN,
                is_starred=True,
            ),
            data["order_products"][1],
        )

        self.assertDictContainsSubset(
            dict(
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                name="Special Product",
                description="2 doz. Special Product",
                quantity=2,
                quantity_type=QUANTITY_DOZEN,
                is_starred=True,
            ),
            data["order_products"][0],
        )

        ops = order.order_products.all()
        self.assertEqual(len(ops), 2)

        fp_op = ops.get(product_type=OP_TYPE_FRONT_PRODUCT)
        self.assertEqual(fp_op.name, "Front Product")
        self.assertEqual(fp_op.quantity, 3)
        self.assertEqual(fp_op.total_quantity, 36)

        sp_op = ops.get(product_type=OP_TYPE_SPECIAL_PRODUCT)
        self.assertEqual(sp_op.name, "Special Product")
        self.assertEqual(sp_op.quantity, 2)
        self.assertEqual(sp_op.total_quantity, 24)

    def test_create__order_product_without_type__is_rejected(self):
        # GIVEN
        d = datetime.now().date()
        front_product = FrontProductFactory(name="Front Product", is_starred=True)

        # WHEN
        response = self.make_request(
            "POST",
            self.get_list_url(),
            data=dict(
                date=d,
                name="Test Order",
                order_products=[
                    dict(
                        product_type=None,
                        front_product=front_product.id,
                        quantity=3,
                        quantity_type=QUANTITY_DOZEN,
                    ),
                ],
            ),
            permitted_codes=[400],
        ).json()

        self.assert_response_message_equals(
            response, "Product Type: This field may not be null."
        )

    def test_create__order_front_product_without_front_product__is_rejected(self):
        # GIVEN
        d = datetime.now().date()
        front_product = FrontProductFactory(name="Front Product", is_starred=True)

        # WHEN
        response = self.make_request(
            "POST",
            self.get_list_url(),
            data=dict(
                date=d,
                name="Test Order",
                order_products=[
                    dict(
                        product_type=OP_TYPE_FRONT_PRODUCT,
                        front_product=None,
                        quantity=3,
                        quantity_type=QUANTITY_DOZEN,
                    ),
                ],
            ),
            permitted_codes=[400],
        ).json()

        self.assert_response_message_equals(
            response, "Front Product: This field is required"
        )

    def test_create__order_special_product_without_name__is_rejected(self):
        # GIVEN
        d = datetime.now().date()
        front_product = FrontProductFactory(name="Front Product", is_starred=True)

        # WHEN
        response = self.make_request(
            "POST",
            self.get_list_url(),
            data=dict(
                date=d,
                name="Test Order",
                order_products=[
                    dict(
                        product_type=OP_TYPE_SPECIAL_PRODUCT,
                        name=None,
                        quantity=3,
                        quantity_type=QUANTITY_DOZEN,
                    ),
                ],
            ),
            permitted_codes=[400],
        ).json()

        self.assert_response_message_equals(
            response, "Name: This field may not be null."
        )

    def test_create__special_product_with_product_specified__special_product_is_removed(
        self,
    ):
        # GIVEN
        d = datetime.now().date()
        front_product = FrontProductFactory(name="Front Product")

        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=dict(
                date=d,
                name="Test Order",
                order_products=[
                    dict(
                        product_type=OP_TYPE_SPECIAL_PRODUCT,
                        quantity=2,
                        quantity_type=QUANTITY_DOZEN,
                        name="Special Product",
                        front_product=front_product.id,
                    ),
                ],
            ),
        ).json()

        # THEN
        order = Order.objects.get(id=data["id"])
        self.assertEqual(len(data["order_products"]), 1)

        self.assertDictContainsSubset(
            dict(product_type=OP_TYPE_SPECIAL_PRODUCT, front_product=None,),
            data["order_products"][0],
        )

    def test_update__update_properties__is_accepted(self):
        # GIVEN
        order = OrderFactory()
        fp_op = FrontProductOrderProductFactory(order=order)
        sp_op = FrontProductOrderProductFactory(order=order)

        # AND
        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        # WHEN
        data["name"] = "New Name"
        data["order_products"][0]["quantity"] = 42
        data["order_products"][1]["quantity"] = 43
        data = self.make_request("PUT", self.get_obj_url(order.id), data=data).json()

        # THEN
        self.assertDictContainsSubset(dict(id=order.id, name="New Name",), data)

        order = order.reload()
        self.assertEqual(order.name, "New Name")

        self.assertDictContainsSubset(
            dict(id=sp_op.id, quantity=42,), data["order_products"][0]
        )

        self.assertDictContainsSubset(
            dict(id=fp_op.id, quantity=43,), data["order_products"][1]
        )

    def test_update__delete_products__is_accepted(self):
        # GIVEN
        order = OrderFactory()
        fp_op = FrontProductOrderProductFactory(order=order)
        sp_op = FrontProductOrderProductFactory(order=order)

        # AND
        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        # WHEN
        data["order_products"] = []
        data = self.make_request("PUT", self.get_obj_url(order.id), data=data).json()

        # THEN
        self.assertDictContainsSubset(dict(order_products=[]), data)

        order = order.reload()
        order_products = order.order_products.all()
        self.assertEqual(len(order_products), 0)

    def test_update__add_products__is_accepted(self):
        # GIVEN
        front_product = FrontProductFactory(name="Front Product")
        order = OrderFactory()

        # AND
        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        # WHEN
        data["order_products"] = [
            dict(
                product_type=OP_TYPE_FRONT_PRODUCT,
                front_product=front_product.id,
                quantity=3,
                quantity_type=QUANTITY_DOZEN,
            ),
            dict(
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                quantity=2,
                quantity_type=QUANTITY_DOZEN,
                name="Special Product",
            ),
        ]
        data = self.make_request("PUT", self.get_obj_url(order.id), data=data).json()

        # THEN
        order = order.reload()
        self.assertEqual(len(data["order_products"]), 2)

        self.assertDictContainsSubset(
            dict(
                product_type=OP_TYPE_FRONT_PRODUCT,
                name="Front Product",
                description="3 doz. Front Product",
                quantity=3,
                quantity_type=QUANTITY_DOZEN,
            ),
            data["order_products"][1],
        )

        self.assertDictContainsSubset(
            dict(
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                name="Special Product",
                description="2 doz. Special Product",
                quantity=2,
                quantity_type=QUANTITY_DOZEN,
            ),
            data["order_products"][0],
        )

        ops = order.order_products.all()
        self.assertEqual(len(ops), 2)

        fp_op = ops.get(product_type=OP_TYPE_FRONT_PRODUCT)
        self.assertEqual(fp_op.name, "Front Product")
        self.assertEqual(fp_op.quantity, 3)
        self.assertEqual(fp_op.total_quantity, 36)

        sp_op = ops.get(product_type=OP_TYPE_SPECIAL_PRODUCT)
        self.assertEqual(sp_op.name, "Special Product")
        self.assertEqual(sp_op.quantity, 2)
        self.assertEqual(sp_op.total_quantity, 24)

    def test_delete__basic_order__is_rejected(self):
        order = OrderFactory()

        self.make_request(
            "DELETE", self.get_obj_url(order.id), permitted_codes=[405]
        ).json()

    def test_update__add_products_to_cancelled_order__is_rejected(self):
        # GIVEN
        order = OrderFactory(cancelled=True)

        # AND
        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        # WHEN
        data["order_products"] = [
            dict(
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                quantity=2,
                quantity_type=QUANTITY_DOZEN,
                name="Special Product",
            ),
        ]
        data["cancelled"] = False  # Can't un-cancel an order
        error = self.make_request(
            "PUT", self.get_obj_url(order.id), data=data, permitted_codes=[400]
        ).json()

        # THEN
        self.assertEqual(
            error["detail"],
            "Cancelled orders cannot have products! Please create a new order instead.",
        )

    def test_create__add_valid_order_flags__is_accepted(self):
        # GIVEN
        flags = OrderFlagFactory.create_batch(2)

        # WHEN
        data = DashboardOrderRequestFactory(
            name="Test", date="2000-01-01", order_flags=[f.id for f in flags],
        )
        data = self.make_request("POST", self.get_list_url(), data=data).json()

        order = Order.objects.get(id=data["id"])

        # THEN
        # Ensure OrderFlagOrder created for each flag
        for flag in flags:
            ofo = OrderFlagOrder.objects.get(flag=flag, order=order)
            self.assertIsNotNone(ofo.created)
            self.assertIsNotNone(ofo.updated)

    def test_create__duplicate_order_flags__is_rejected(self):
        # GIVEN
        flag = OrderFlagFactory()

        # WHEN
        data = DashboardOrderRequestFactory(
            name="Test", date="2000-01-01", order_flags=[flag.id, flag.id],
        )
        error = self.make_request(
            "POST", self.get_list_url(), data=data, permitted_codes=[400]
        ).json()

        self.assertEqual(
            error["detail"], "Order Flags: Duplicate entries not permitted"
        )

    def test_create__add_invalid_order_flags__is_rejected(self):
        data = DashboardOrderRequestFactory(
            name="Test", date="2000-01-01", order_flags=[99999],
        )
        error = self.make_request(
            "POST", self.get_list_url(), data=data, permitted_codes=[400]
        ).json()

        self.assertEqual(
            error["detail"], 'Order Flags: Invalid pk "99999" - object does not exist.'
        )

    def test_update__add_remove_flags__updates_db_properly(self):
        # GIVEN
        flags = OrderFlagFactory.create_batch(3)

        # AND
        data = DashboardOrderRequestFactory(
            name="Test", date="2000-01-01", order_flags=[flags[0].id, flags[1].id],
        )
        data = self.make_request("POST", self.get_list_url(), data=data).json()

        # WHEN
        data["order_flags"] = [flags[1].id, flags[2].id]
        data = self.make_request("PUT", self.get_obj_url(data["id"]), data=data).json()

        order = Order.objects.get(id=data["id"])

        # THEN
        with self.assertRaises(OrderFlagOrder.DoesNotExist):
            OrderFlagOrder.objects.get(flag=flags[0], order=order)  # Deleted
        OrderFlagOrder.objects.get(flag=flags[1], order=order)  # Updated
        OrderFlagOrder.objects.get(flag=flags[2], order=order)  # Created

    def test_create__negative_price__is_rejected(self):
        response = self.make_request(
            "POST",
            self.get_list_url(),
            data=DashboardOrderRequestFactory(custom_price=Decimal("-0.01")),
            permitted_codes=[400],
        ).json()

        self.assertDictContainsSubset(
            dict(
                detail="Custom Price: Ensure this value is greater than or equal to 0.",
            ),
            response,
        )

    def test_send_invoice__basic_order__is_accepted(self):
        # GIVEN
        order_date = start_of_date(local_tz_now() + timedelta(days=20)).date()
        email = "createdorder@glamorganbakery.com"
        order = InvoicableOrderFactory(
            name="My Created Order",
            email=email,
            calculated_price="12.20",
            date=order_date,
            time=time(15),
        )
        front_product = FrontProductFactory(name="My Front Product")
        FrontProductOrderProductFactory(
            order=order,
            quantity_type=QUANTITY_HALF_DOZEN,
            front_product=front_product,
            quantity=1,
        )
        FrontProductOrderProductFactory(
            order=order,
            quantity_type=QUANTITY_INDIVIDUAL,
            front_product=front_product,
            quantity=2,
        )
        SpecialProductOrderProductFactory(
            order=order,
            quantity_type=QUANTITY_DOZEN,
            name="My Special Product",
            quantity=3,
        )

        # WHEN
        data = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/")
        ).json()

        # THEN
        order = Order.objects.get(id=data["id"])
        self.assertIsNotNone(order.stripe_invoice_id)
        self.assertIsNotNone(order.stripe_customer_id)
        self.assertEqual(order.price, Decimal("12.20"))
        self.assertEqual(order.last_amount_invoiced, Decimal("12.20"))

        # AND
        stripe_customer = order.get_stripe_customer()
        stripe_invoice = order.get_stripe_invoice()
        self.assertEqual(stripe_customer.email, "createdorder@glamorganbakery.com")
        self.assertEqual(stripe_customer.name, "My Created Order")
        self.assertEqual(stripe_invoice.amount_due, 1220)
        self.assertEqual(stripe_invoice.account_country, "CA")
        self.assertEqual(stripe_invoice.currency, "cad")
        self.assertEqual(stripe_invoice.status, "open")
        self.assertEqual(
            stripe_invoice.metadata["order_reference_id"], order.reference_id
        )
        self.assertEqual(stripe_invoice.metadata["order_time"], "3PM")
        self.assertIn(
            f"/orders/{order.id}/edit", stripe_invoice.metadata["order_edit_url"],
        )
        stripe_order_dt = to_timezone(
            # Parse utc datetime from timestamp
            datetime.fromtimestamp(stripe_invoice.due_date, UTC),
            # Convert to MST/MDT
            TIME_ZONE_PYTZ,
        )
        self.assertEqual(stripe_order_dt.date(), order_date)
        self.assertEqual(stripe_order_dt.time(), time(15))

        stripe_invoice_lines = stripe_invoice.lines.data
        self.assertEqual(len(stripe_invoice_lines), 4)
        self.assertEqual(stripe_invoice_lines[0].description, "Total items price")
        self.assertEqual(stripe_invoice_lines[0].quantity, 1)
        self.assertEqual(stripe_invoice_lines[0].amount, 1220)
        self.assertEqual(
            stripe_invoice_lines[1].description, "My Front Product (Half Dozen)"
        )
        self.assertEqual(stripe_invoice_lines[1].quantity, 1)
        self.assertEqual(stripe_invoice_lines[1].amount, 0)
        self.assertEqual(
            stripe_invoice_lines[2].description, "My Front Product (Individual)"
        )
        self.assertEqual(stripe_invoice_lines[2].quantity, 2)
        self.assertEqual(stripe_invoice_lines[2].amount, 0)
        self.assertEqual(
            stripe_invoice_lines[3].description, "My Special Product (Dozen)"
        )
        self.assertEqual(stripe_invoice_lines[3].quantity, 3)
        self.assertEqual(stripe_invoice_lines[3].amount, 0)
        self.assertEqual(order.stripe_charge_status, "payment_pending")

    def test_create__send_invoice__email_required(self):
        # GIVEN
        order = InvoicableOrderFactory(email=None,)

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), permitted_codes=[400]
        ).json()

        self.assertDictContainsSubset(
            dict(detail="Email: This field is required",), response
        )

    def test_send_invoice__past_date_rejected(self):
        # GIVEN
        order_date = local_datetime(2020, 1, 1, 9)
        order = InvoicableOrderFactory(date=order_date, time=time(9),)

        # WHEN
        with freeze_time(order_date + timedelta(seconds=1)):
            response = self.make_request(
                "PUT",
                self.get_obj_url(order.id, "send-invoice/"),
                permitted_codes=[400],
            ).json()

        self.assertDictContainsSubset(
            dict(detail="Cannot invoice customer for an order date in the past",),
            response,
        )

    @patch.object(
        stripe.Invoice, "send_invoice", side_effect=StripeError("Unknown Message")
    )
    def test_send_invoice__unknown_stripe_error__error_is_returned(self, m):
        # GIVEN
        order = InvoicableOrderFactory()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), permitted_codes=[400]
        ).json()

        self.assertDictContainsSubset(
            dict(
                detail='Unexpected error while communicating with Stripe: "Unknown Message"',
            ),
            response,
        )

    @patch.object(
        stripe.Invoice,
        "send_invoice",
        side_effect=APIConnectionError("Cannot connect to Stripe API"),
    )
    def test_create__stripe_downtime__error_is_returned(self, m):
        # GIVEN
        order = InvoicableOrderFactory()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), permitted_codes=[400]
        ).json()

        self.assertDictContainsSubset(
            dict(
                detail='Unexpected error while communicating with Stripe: "Cannot connect to Stripe API"',
            ),
            response,
        )

    def test_send_invoice__invoice_already_sent__error_occurs(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Pending Invoice Order",
            email="pendinginvoice@glamorganbakery.com",
            custom_price="12.20",
        )

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), permitted_codes=[400]
        ).json()

        self.assertDictContainsSubset(
            dict(detail="Invoice must be voided before creating a new invoice",),
            response,
        )

    def test_send_invoice__order_marked_as_paid__is_rejected(self):
        # GIVEN
        order = InvoicableOrderFactory(marked_as_paid=True)

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Cannot send invoice for an order that is marked as paid"
        )

    def test_resend_invoice__pending_invoice_exists__original_invoice_is_voided(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Pending Invoice Order",
            email="pendinginvoice@glamorganbakery.com",
            custom_price="12.20",
        )

        # WHEN
        original_stripe_customer_id = order.stripe_customer_id
        original_stripe_invoice_id = order.stripe_invoice_id
        order = self.edit_and_resend_invoice(order, "50.20")

        # THEN
        order = order.reload()
        new_stripe_invoice = order.get_stripe_invoice()
        self.assertNotEqual(order.stripe_invoice_id, original_stripe_invoice_id)
        self.assertEqual(order.stripe_customer_id, original_stripe_customer_id)

        # AND
        self.assertEqual(new_stripe_invoice.amount_due, 5020)
        self.assertEqual(new_stripe_invoice.account_country, "CA")
        self.assertEqual(new_stripe_invoice.currency, "cad")
        self.assertEqual(new_stripe_invoice.status, "open")
        self.assertEqual(new_stripe_invoice.metadata["order_time"], "(Unknown)")

        # AND old invoice is voided
        original_stripe_invoice = stripe.Invoice.retrieve(original_stripe_invoice_id)
        self.assertEqual(original_stripe_invoice.status, "void")

    def test_resend_invoice__voided_invoice_exists__new_invoice_is_created(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Voided Invoice Order", email="voidedinvoice@glamorganbakery.com",
        )

        # AND
        stripe.Invoice.void_invoice(order.stripe_invoice_id)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("invoice.voided", order.stripe_invoice_id)
        )

        # THEN payment status is updated
        order = order.reload()
        self.assertEqual(order.stripe_charge_status, None)

        # AND WHEN
        original_stripe_customer_id = order.stripe_customer_id
        original_stripe_invoice_id = order.stripe_invoice_id
        order = self.edit_and_resend_invoice(order, "50.10")

        # THEN
        order = order.reload()
        new_stripe_invoice = order.get_stripe_invoice()
        self.assertNotEqual(order.stripe_invoice_id, original_stripe_invoice_id)
        self.assertEqual(order.stripe_customer_id, original_stripe_customer_id)

        # AND
        self.assertEqual(new_stripe_invoice.amount_due, 5010)
        self.assertEqual(new_stripe_invoice.account_country, "CA")
        self.assertEqual(new_stripe_invoice.currency, "cad")
        self.assertEqual(new_stripe_invoice.status, "open")

        # AND old invoice remains voided
        original_stripe_invoice = stripe.Invoice.retrieve(original_stripe_invoice_id)
        self.assertEqual(original_stripe_invoice.status, "void")

    def test_void_invoice__uncollectible_or_unknown_status__error_is_returned(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Uncollectible Invoice Order",
            email="uncollectibleinvoice@glamorganbakery.com",
        )

        # AND
        stripe.Invoice.mark_uncollectible(order.stripe_invoice_id)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event(
                "invoice.marked_uncollectible", order.stripe_invoice_id
            )
        )

        # THEN payment status is updated
        order = order.reload()
        self.assertEqual(order.stripe_charge_status, "paid")

        # AND WHEN
        order.calculated_price = "50.20"
        order.save()

        # AND
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "void-invoice/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(
                detail="Current status is in invalid status `uncollectible`. "
                "Please change this status manually and try again.",
            ),
            response,
        )

    def test_resend_invoice__invoice_paid__original_invoice_is_refunded(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Paid Invoice Order",
            email="paidinvoice@glamorganbakery.com",
            custom_price="13.95",
        )

        # AND customer pays for order
        self.pay_order_by_credit_card(order)
        original_stripe_customer_id = order.stripe_customer_id
        original_stripe_invoice = order.get_stripe_invoice()

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("invoice.paid", order.stripe_invoice_id)
        )

        # THEN payment status is updated
        order = order.reload()
        self.assertEqual(order.stripe_charge_status, "paid")
        self.assertEqual(order.stripe_charge_id, original_stripe_invoice.charge)

        # AND WHEN
        order.custom_price = "50.00"
        order.save()

        # AND
        self.make_request("PUT", self.get_obj_url(order.id, "refund-invoice/")).json()

        # AND
        data = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/")
        ).json()

        order = order.reload()
        new_stripe_invoice = order.get_stripe_invoice()
        self.assertNotEqual(order.stripe_invoice_id, original_stripe_invoice.id)
        self.assertEqual(order.stripe_customer_id, original_stripe_customer_id)

        # AND
        self.assertEqual(new_stripe_invoice.amount_due, 5000)
        self.assertEqual(new_stripe_invoice.account_country, "CA")
        self.assertEqual(new_stripe_invoice.currency, "cad")
        self.assertEqual(new_stripe_invoice.status, "open")
        self.assertEqual(order.stripe_charge_id, None)

        # AND old invoice is refunded
        refunds = stripe.Refund.list(
            payment_intent=original_stripe_invoice.payment_intent
        )
        self.assertEqual(len(refunds), 1)
        self.assertEqual(original_stripe_invoice.status, "paid")

    # I'm not sure if this scenario is possible, but better safe than sorry
    @patch.object(
        stripe.Refund,
        "create",
        side_effect=InvalidRequestError("Unknown refund error", "id"),
    )
    def test_refund_invoice__invoice_paid_and_cannot_be_refunded__error_message_is_displayed(
        self, m
    ):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Unrefundable Paid Invoice Order",
            email="unrefundable@glamorganbakery.com",
        )

        # AND customer pays for order
        self.pay_order_by_credit_card(order)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("invoice.paid", order.stripe_invoice_id)
        )

        # WHEN
        order = order.reload()
        order.calculated_price = "50.00"
        order.save()

        # AND
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "refund-invoice/"), permitted_codes=[400]
        ).json()

        self.assertDictContainsSubset(
            dict(
                detail='Unexpected error while communicating with Stripe: "Unknown refund error"',
            ),
            response,
        )

    # A new customer must always be created because to prevent someone using another customer's card!
    def test_resend_invoice__orders_with_same_email_exists__new_customer_is_created(
        self,
    ):
        # GIVEN
        email = "sameemailaddress@glamorganbakery.com"
        order_1 = self.create_basic_invoiced_order(email=email,)

        # WHEN
        order_2 = self.create_basic_invoiced_order(email=email,)

        # THEN
        stripe_customer_1 = order_1.get_stripe_customer()
        stripe_customer_2 = order_2.get_stripe_customer()
        self.assertEqual(stripe_customer_1.email, stripe_customer_2.email)
        self.assertNotEqual(stripe_customer_1.stripe_id, stripe_customer_2.stripe_id)

    def test_refund_invoice__disputed_purchase__error_is_returned(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Chargeback Order",
            email="chargeback@glamorganbakery.com",
            calculated_price="13.95",
        )

        # AND customer pays for order
        self.pay_order_by_credit_card(order, source="tok_createDispute")

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("invoice.paid", order.stripe_invoice_id)
        )

        # WHEN
        order = order.reload()
        order.calculated_price = "50.00"
        order.save()

        # AND
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "refund-invoice/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assertIn(
            "Unexpected error while communicating with Stripe:", response["detail"]
        )
        self.assertIn(
            "has been charged back; cannot issue a refund.", response["detail"]
        )

    def test_resend_invoice__customer_is_deleted__new_customer_is_created(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Deleted Customer Order", email="deletedcustomer@glamorganbakery.com",
        )

        # AND Stripe customer is deleted
        stripe.Customer.delete(order.stripe_customer_id)

        # WHEN
        order.calculated_price = "50.00"
        order.save()
        original_stripe_customer_id = order.stripe_customer_id
        original_stripe_invoice_id = order.stripe_invoice_id

        # AND
        self.make_request("PUT", self.get_obj_url(order.id, "void-invoice/")).json()

        # AND
        self.make_request("PUT", self.get_obj_url(order.id, "send-invoice/")).json()

        # THEN
        order = order.reload()
        stripe_customer = order.get_stripe_customer()
        stripe_invoice = order.get_stripe_invoice()
        self.assertNotEqual(stripe_customer.stripe_id, original_stripe_customer_id)
        self.assertNotEqual(stripe_invoice.stripe_id, original_stripe_invoice_id)

    def test_resend_invoice__customer_no_longer_exists__new_customer_is_created(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Deleted Customer Order", email="deletedcustomer@glamorganbakery.com",
        )

        # WHEN
        order.calculated_price = "50.00"
        order.stripe_customer_id = "fake_customer_id"
        order.save()
        original_stripe_customer_id = order.stripe_customer_id
        original_stripe_invoice_id = order.stripe_invoice_id

        # AND
        self.make_request("PUT", self.get_obj_url(order.id, "void-invoice/")).json()

        # AND
        self.make_request("PUT", self.get_obj_url(order.id, "send-invoice/")).json()

        # THEN
        order = order.reload()
        stripe_customer = order.get_stripe_customer()
        stripe_invoice = order.get_stripe_invoice()
        self.assertNotEqual(stripe_customer.stripe_id, original_stripe_customer_id)
        self.assertNotEqual(stripe_invoice.stripe_id, original_stripe_invoice_id)

    def test_cancel_order__basic_order__is_accepted(self):
        # GIVEN
        order = OrderFactory()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "cancel/")
        ).json()

        # THEN
        order = order.reload()
        self.assertTrue(order.cancelled)

    def test_cancel_order__invoiced_order__error_is_returned(self):
        # GIVEN
        order = self.create_basic_invoiced_order()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "cancel/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(detail="Stripe invoice must be voided before being cancelled",),
            response,
        )

    def test_void_order__invoice_never_created__error_is_returned(self):
        order = OrderFactory()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "void-invoice/"), permitted_codes=[400],
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(detail="Invoice has not been sent",), response
        )

    def test_refund_order__invoice_never_created__error_is_returned(self):
        order = OrderFactory()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "refund-invoice/"), permitted_codes=[400],
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(
                detail="Cannot refund an order if the customer's credit card was never charged",
            ),
            response,
        )

    def test_void_order__no_invoice_exists__error_is_returned(self):
        order = InvoicedOrderFactory(stripe_invoice_id="fake_stripe_id")

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "void-invoice/"), permitted_codes=[400],
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(
                detail="Unexpected error while communicating with Stripe: \"No such invoice: 'fake_stripe_id'\"",
            ),
            response,
        )

    def test_voidorder__no_invoice_exists__error_is_returned(self):
        order = InvoicedOrderFactory(stripe_invoice_id="fake_stripe_id")

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "void-invoice/"), permitted_codes=[400],
        ).json()

        # THEN
        self.assertDictContainsSubset(
            dict(
                detail="Unexpected error while communicating with Stripe: \"No such invoice: 'fake_stripe_id'\"",
            ),
            response,
        )

    def test_void_order__voided_order__is_accepted(self):
        # GIVEN
        order = self.create_basic_invoiced_order()
        original_stripe_invoice_id = order.stripe_invoice_id

        # WHEN
        data = self.make_request(
            "PUT", self.get_obj_url(order.id, "void-invoice/")
        ).json()

        # THEN invoice ID is cleared
        order = order.reload()
        self.assertIsNone(order.stripe_invoice_id)

        # AND invoice is voided
        original_stripe_invoice = stripe.Invoice.retrieve(original_stripe_invoice_id)
        self.assertEqual(original_stripe_invoice.status, "void")

    def test_refund_order__voided_order__is_rejected(self):
        # GIVEN
        order = self.create_basic_invoiced_order()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "refund-invoice/"), permitted_codes=[400],
        ).json()

        # THEN
        self.assert_response_message_equals(
            response,
            "Cannot refund an order if the customer's credit card was never charged",
        )

    def test_void_order__refunded_order__is_rejected(self):
        # GIVEN
        order = self.create_basic_invoiced_order()

        # AND
        self.pay_order_by_credit_card(order)

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "void-invoice/"), permitted_codes=[400],
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Cannot void an order if the customer's credit card was charged"
        )

    def test_refund_order__refunded_order__is_accepted(self):
        # GIVEN
        order = self.create_basic_invoiced_order()
        original_stripe_invoice_id = order.stripe_invoice_id

        # AND
        self.pay_order_by_credit_card(order)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("invoice.paid", order.stripe_invoice_id)
        )

        # WHEN
        data = self.make_request(
            "PUT", self.get_obj_url(order.id, "refund-invoice/")
        ).json()

        # THEN invoice ID is cleared
        order = order.reload()
        self.assertIsNone(order.stripe_invoice_id)

        # AND invoice is refunded
        original_stripe_invoice = stripe.Invoice.retrieve(original_stripe_invoice_id)
        refunds = stripe.Refund.list(
            payment_intent=original_stripe_invoice.payment_intent
        )
        self.assertEqual(len(refunds), 1)

    def test_mark_as_paid__basic_order__is_accepted(self):
        # GIVEN
        order = OrderFactory()

        # WHEN
        self.make_request("PUT", self.get_obj_url(order.id, "mark-as-paid/")).json()

        # THEN
        order = order.reload()
        self.assertEqual(order.marked_as_paid, True)
        self.assertEqual(order.purchase_status, "marked_as_paid")
        self.assertEqual(order.purchase_status_text, "Marked as Paid")
        self.assertEqual(order.purchase_status_text, "Marked as Paid")

    def test_mark_as_paid__payment_pending_stripe_order__is_rejected(self):
        # GIVEN
        order = InvoicedOrderFactory(
            stripe_charge_status=PAYMENT_STATUS_PAYMENT_PENDING
        )

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "mark-as-paid/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response,
            "Cannot mark order as paid when it has a payment pending; please go to "
            "order's Stripe invoice and mark the invoice paid instead.",
        )

        # AND order is unchanged
        order = order.reload()
        self.assertEqual(order.marked_as_paid, False)
        self.assertEqual(order.purchase_status, "payment_pending")
        self.assertEqual(order.purchase_status_text, "Payment Pending")

    def test_mark_as_paid__stripe_paid_order__is_rejected(self):
        # GIVEN
        order = InvoicedOrderFactory(stripe_charge_status=PAYMENT_STATUS_PAID)

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "mark-as-paid/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Cannot mark order as paid when it has been paid via Stripe."
        )

        # AND order is unchanged
        order = order.reload()
        self.assertEqual(order.marked_as_paid, False)
        self.assertEqual(order.purchase_status, "paid")
        self.assertEqual(order.purchase_status_text, "Paid")

    def test_edit_order__stripe_paid_order_and_marked_as_paid__is_rejected(self):
        # GIVEN
        order = InvoicedOrderFactory(stripe_charge_status=PAYMENT_STATUS_PAID)

        # AND
        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        # WHEN
        data["marked_as_paid"] = True
        response = self.make_request(
            "PUT", self.get_obj_url(order.id), data=data, permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Cannot mark order as paid when it has been paid via Stripe."
        )

        # AND order is unchanged
        order = order.reload()
        self.assertEqual(order.marked_as_paid, False)
        self.assertEqual(order.purchase_status, "paid")
        self.assertEqual(order.purchase_status_text, "Paid")

    def test_mark_as_paid__marked_as_paid_order__is_rejected(self):
        # GIVEN
        order = OrderFactory(marked_as_paid=True)

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "mark-as-paid/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Order has already been marked as paid"
        )

    def test_unmark_as_paid__paid_order__is_accepted(self):
        # GIVEN
        order = OrderFactory(marked_as_paid=True)

        # WHEN
        self.make_request("PUT", self.get_obj_url(order.id, "unmark-as-paid/")).json()

        # THEN
        order = order.reload()
        self.assertEqual(order.marked_as_paid, False)
        self.assertEqual(order.purchase_status, "unpaid")
        self.assertEqual(order.purchase_status_text, "Unpaid")

    def test_unmark_as_paid__order_not_marked_as_paid__is_rejected(self):
        # GIVEN
        order = OrderFactory(marked_as_paid=False)

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "unmark-as-paid/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Order has not been marked as paid"
        )

        # AND order is unchanged
        order = order.reload()
        self.assertEqual(order.marked_as_paid, False)
        self.assertEqual(order.purchase_status, "unpaid")
        self.assertEqual(order.purchase_status_text, "Unpaid")

    def test_get_order__basic_order__data_format(self):
        # GIVEN
        order = OrderFactory()

        # WHEN
        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        # THEN
        self.assertEqual(data["marked_as_paid"], False)
        self.assertEqual(data["purchase_status"], "unpaid")
        self.assertEqual(data["purchase_status_text"], "Unpaid")

    def test_send_invoice__mark_invoice_as_paid__is_accepted(self):
        # WHEN
        order = self.create_basic_invoiced_order(
            name="Send and Mark as Paid",
            email="sendandmarkaspaid@glamorganbakery.com",
            calculated_price="12.20",
            data=dict(mark_invoice_as_paid=True,),
        )

        # THEN
        order = order.reload()
        stripe_invoice = order.get_stripe_invoice()
        self.assertEqual(stripe_invoice.status, "paid")
        self.assertEqual(order.stripe_charge_status, "paid")

    def test_resend_invoice__mark_invoice_as_paid__is_accepted(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Resend and Mark as Paid",
            email="resendandmarkaspaid@glamorganbakery.com",
            calculated_price="12.20",
        )

        # WHEN
        order = self.edit_and_resend_invoice(
            order, "50.20", data=dict(mark_invoice_as_paid=True)
        )

        # THEN
        order = order.reload()
        stripe_invoice = order.get_stripe_invoice()
        self.assertEqual(stripe_invoice.status, "paid")
        self.assertEqual(order.stripe_charge_status, "paid")

    def test_send_invoice__calculated_price__is_accepted(self):
        # GIVEN
        front_product = FrontProductFactory(name="Front Product", is_starred=True)
        FrontProductPriceFactory(
            front_product=front_product,
            quantity_type=QUANTITY_DOZEN,
            price=Decimal("4.20"),
        )
        FrontProductPriceFactory(
            front_product=front_product,
            quantity_type=QUANTITY_HALF_DOZEN,
            price=Decimal("2.30"),
        )

        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=DashboardOrderRequestFactory(
                order_products=[
                    dict(
                        product_type=OP_TYPE_FRONT_PRODUCT,
                        front_product=front_product.id,
                        quantity=3,
                        quantity_type=QUANTITY_DOZEN,
                    ),
                    dict(
                        product_type=OP_TYPE_FRONT_PRODUCT,
                        front_product=front_product.id,
                        quantity=2,
                        quantity_type=QUANTITY_HALF_DOZEN,
                    ),
                ]
            ),
        ).json()

        # THEN
        order = Order.objects.get(id=data["id"])
        self.assertEqual(Decimal(data["price"]), Decimal("17.20"))
        self.assertEqual(order.price, Decimal("17.20"))
        self.assertEqual(order.custom_price, None)
        self.assertEqual(order.calculated_price, Decimal("17.20"))

        # AND WHEN
        data = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/")
        ).json()

        # THEN
        self.assertEqual(Decimal(data["price"]), Decimal("17.20"))

        # AND
        order = order.reload()
        stripe_invoice = order.get_stripe_invoice()
        self.assertEqual(stripe_invoice.amount_due, 1720)

    def test_create__product_does_not_have_price__calculated_price_is_not_calculated(
        self,
    ):
        # GIVEN
        front_product = FrontProductFactory(name="Front Product", is_starred=True)
        FrontProductPriceFactory(
            front_product=front_product,
            quantity_type=QUANTITY_DOZEN,
            price=Decimal("4.20"),
        )

        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=DashboardOrderRequestFactory(
                order_products=[
                    dict(
                        product_type=OP_TYPE_FRONT_PRODUCT,
                        front_product=front_product.id,
                        quantity=3,
                        quantity_type=QUANTITY_DOZEN,
                    ),
                    dict(
                        product_type=OP_TYPE_FRONT_PRODUCT,
                        front_product=front_product.id,
                        quantity=2,
                        quantity_type=QUANTITY_HALF_DOZEN,
                    ),
                ]
            ),
        ).json()

        # THEN
        order = Order.objects.get(id=data["id"])
        self.assertEqual(data["price"], None)
        self.assertEqual(order.price, None)
        self.assertEqual(order.custom_price, None)
        self.assertEqual(order.calculated_price, None)

    def test_create__input_all_product_prices__price_is_calculated(self):
        # GIVEN
        front_product = FrontProductFactory(name="Front Product", is_starred=True)
        fpp = FrontProductPriceFactory(
            front_product=front_product,
            quantity_type=QUANTITY_DOZEN,
            price=Decimal("1.10"),
        )

        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=DashboardOrderRequestFactory(
                order_products=[
                    dict(
                        product_type=OP_TYPE_FRONT_PRODUCT,
                        front_product=front_product.id,
                        quantity=1,
                        quantity_type=QUANTITY_DOZEN,
                        custom_price=Decimal("1.05"),
                    ),
                    dict(
                        product_type=OP_TYPE_SPECIAL_PRODUCT,
                        name="Custom products",
                        quantity=2,
                        quantity_type=QUANTITY_HALF_DOZEN,
                        custom_price=Decimal("0.66"),
                    ),
                ]
            ),
        ).json()

        # THEN
        order = Order.objects.get(id=data["id"])
        self.assertEqual(Decimal(data["price"]), Decimal("2.37"))
        self.assertEqual(data["custom_price"], None)
        self.assertEqual(Decimal(data["calculated_price"]), Decimal("2.37"))
        self.assertEqual(order.price, Decimal("2.37"))
        self.assertEqual(order.custom_price, None)
        self.assertEqual(order.calculated_price, Decimal("2.37"))

        # AND
        self.assertEqual(len(data["order_products"]), 2)
        op_data_0 = data["order_products"][0]
        op_0 = OrderProduct.objects.get(id=op_data_0["id"])
        self.assertEqual(Decimal(op_data_0["price"]), Decimal("0.66"))
        self.assertEqual(op_data_0["custom_price"], "0.66")
        self.assertEqual(op_data_0["calculated_price"], None)
        self.assertEqual(op_0.price, Decimal("0.66"))
        self.assertEqual(op_0.custom_price, Decimal("0.66"))
        self.assertEqual(op_0.calculated_price, None)
        self.assertEqual(op_0.front_product_price_id, None)
        op_data_1 = data["order_products"][1]
        op_1 = OrderProduct.objects.get(id=op_data_1["id"])
        self.assertEqual(Decimal(op_data_1["price"]), Decimal("1.05"))
        self.assertEqual(op_data_1["custom_price"], "1.05")
        self.assertEqual(op_data_1["calculated_price"], "1.10")
        self.assertEqual(op_1.price, Decimal("1.05"))
        self.assertEqual(op_1.custom_price, Decimal("1.05"))
        self.assertEqual(op_1.calculated_price, Decimal("1.10"))
        self.assertEqual(op_1.front_product_price_id, fpp.id)

    def test_webhook__refund_on_stripe__order_is_marked_unpaid(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Refunded Invoice Order", email="refundedinvoice@glamorganbakery.com",
        )

        # AND
        self.pay_order_by_credit_card(order)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("invoice.paid", order.stripe_invoice_id)
        )

        # AND
        order = order.reload()
        stripe.Refund.create(charge=order.stripe_charge_id)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("charge.refunded", order.stripe_charge_id)
        )

        # THEN payment status is updated
        order = order.reload()
        self.assertIsNone(order.stripe_charge_status)
        self.assertIsNone(order.stripe_invoice_id)
        self.assertIsNone(order.stripe_charge_id)
        self.assertIsNotNone(order.stripe_customer_id)

    def test_webhook__partial_refund_on_stripe__order_remains_paid(self):
        # GIVEN
        order = self.create_basic_invoiced_order(
            name="Refunded Invoice Order",
            email="refundedinvoice@glamorganbakery.com",
            custom_price="12.20",
        )

        # AND
        self.pay_order_by_credit_card(order)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("invoice.paid", order.stripe_invoice_id)
        )

        # AND
        order = order.reload()
        stripe.Refund.create(charge=order.stripe_charge_id, amount=600)

        # AND webhook is triggered by Stripe
        self.simulate_webhook_call(
            self.find_recent_stripe_event("charge.refunded", order.stripe_charge_id)
        )

        # THEN payment status is updated
        order = order.reload()
        self.assertEqual(order.stripe_charge_status, PAYMENT_STATUS_PAID)
        self.assertIsNotNone(order.stripe_invoice_id, None)
        self.assertIsNotNone(order.stripe_charge_id)
        self.assertIsNotNone(order.stripe_customer_id)

    def test_edit_order__mark_order_as_filled__is_accepted(self):
        # GIVEN
        order = OrderFactory()
        SpecialProductOrderProductFactory(order=order)
        FrontProductOrderProductFactory(order=order)

        # AND
        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        # WHEN
        data["filled"] = True
        data = self.make_request("PUT", self.get_obj_url(order.id), data=data).json()

        # THEN
        order = order.reload()
        self.assertEqual(order.filled, True)
        self.assertEqual(data["filled"], True)
        self.assertEqual(order.order_products.count(), 2)

    def test_cancel_order__online_order_not_refunded__is_rejected(self):
        # GIVEN
        order = OnlineOrderFactory()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "cancel/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Stripe charge must be refunded before being cancelled"
        )

        # AND
        order = order.reload()
        self.assertFalse(order.cancelled)

    @patch.object(stripe.Charge, "retrieve", MagicMock())
    @patch.object(stripe.Refund, "create", MagicMock())
    def test_cancel_order__online_order_refunded__is_accepted(self):
        # GIVEN
        order = OnlineOrderFactory()

        # AND
        self.make_request(
            "PUT", f"/api/dashboard/orders/{order.id}/refund-invoice/",
        ).json()

        # WHEN
        self.make_request("PUT", self.get_obj_url(order.id, "cancel/")).json()

        # THEN
        order = order.reload()
        self.assertTrue(order.cancelled)

    def test_retrieve__invoiced_order__data_format(self):
        order = InvoicedOrderFactory()

        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        self.assertDictContainsSubset(
            dict(
                id=order.id,
                stripe_invoice_link="https://dashboard.stripe.com/invoices/fake_stripe_invoice_id",
                stripe_charge_link=None,
            ),
            data,
        )

    def test_retrieve__online_order__data_format(self):
        order = OnlineOrderFactory(name="My Order")

        data = self.make_request("GET", self.get_obj_url(order.id)).json()

        self.assertDictContainsSubset(
            dict(
                id=order.id,
                stripe_invoice_link=None,
                stripe_charge_link="https://dashboard.stripe.com/charges/fake_stripe_charge_id",
            ),
            data,
        )

    def test_send_invoice__online_order_already_created__is_rejected(self):
        # GIVEN
        order = OnlineOrderFactory()

        # WHEN
        response = self.make_request(
            "PUT", self.get_obj_url(order.id, "send-invoice/"), permitted_codes=[400]
        ).json()

        # THEN
        self.assert_response_message_equals(
            response, "Charge must be refunded before creating a new invoice"
        )


class DashboardOrderProductViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/orders/products/"

    def test_permissions__all__are_enforced(self):
        self.login_basic_user()

        assert_statuses_are_met(
            [
                ["GET", self.get_list_url(), 404],
                ["GET", self.get_list_url() + "stats/", 400],
                ["GET", self.get_obj_url(9999), 404],
                ["POST", self.get_list_url(), 405],
                ["PUT", self.get_obj_url(9999), 404],
                ["DELETE", self.get_obj_url(9999), 404],
            ],
            self,
        )

    def test_stats__no_front_product_stats__is_empty(self):
        # GIVEN
        self.login_basic_user()

        # WHEN
        params = dict(order__date="2030-01-01")
        results = self.make_request(
            "GET", self.get_list_url() + "stats/", query_params=params
        ).json()

        # THEN
        self.assertEqual(results, [])

    def test_stats__front_product_stats__calculated(self):
        # GIVEN
        self.login_basic_user()

        # AND
        d = date(2031, 1, 1)  # Must be unique
        p1 = FrontProductFactory()
        p2 = FrontProductFactory(is_starred=True)
        order1 = OrderFactory(date=d, filled=False, picked_up=True, name="O1")
        order2 = OrderFactory(date=d, filled=True, picked_up=False, name="O2")

        # AND
        FrontProductOrderProductFactory(
            order=order1,
            front_product=p1,
            quantity=2,
            quantity_type=QUANTITY_HALF_DOZEN,
        )
        FrontProductOrderProductFactory(
            order=order2, front_product=p1, quantity=3,
        )
        FrontProductOrderProductFactory(
            order=order2, front_product=p2, quantity=4, quantity_type=QUANTITY_DOZEN,
        )

        # WHEN
        params = dict(order__date="2031-01-01")
        with self.assertNumQueries(2):
            results = self.make_request(
                "GET", self.get_list_url() + "stats/", query_params=params
            ).json()

        # THEN
        self.assertEqual(len(results), 2)
        self.assertDictContainsSubset(
            dict(
                id=p1.id,
                name=p1.name,
                order_name="O2",
                product_type=OP_TYPE_FRONT_PRODUCT,
                is_starred=False,
                totals=dict(
                    to_be_filled=12,
                    to_be_picked_up=3,
                    total=15,
                    quantity_types={
                        str(QUANTITY_HALF_DOZEN): dict(
                            to_be_filled=12, to_be_picked_up=0, total=12,
                        ),
                        str(QUANTITY_INDIVIDUAL): dict(
                            to_be_filled=0, to_be_picked_up=3, total=3,
                        ),
                    },
                ),
            ),
            results[0],
        )
        self.assertDictContainsSubset(
            dict(
                id=p2.id,
                name=p2.name,
                order_name="O2",
                product_type=OP_TYPE_FRONT_PRODUCT,
                is_starred=True,
                totals=dict(
                    to_be_filled=0,
                    to_be_picked_up=48,
                    total=48,
                    quantity_types={
                        str(QUANTITY_DOZEN): dict(
                            to_be_filled=0, to_be_picked_up=48, total=48,
                        ),
                    },
                ),
            ),
            results[1],
        )

    def test_stats__special_product_stats__calculated(self):
        # GIVEN
        self.login_basic_user()

        # AND
        d = date(2032, 1, 1)  # Must be unique
        order1 = OrderFactory(date=d, filled=False, picked_up=True, name="O1")
        order2 = OrderFactory(date=d, filled=True, picked_up=False, name="O2")

        # AND
        op1 = SpecialProductOrderProductFactory(
            order=order1, quantity=2, quantity_type=QUANTITY_HALF_DOZEN,
        )
        op2 = SpecialProductOrderProductFactory(order=order2, quantity=3,)
        op3 = SpecialProductOrderProductFactory(
            order=order2, quantity=4, quantity_type=QUANTITY_DOZEN,
        )

        # WHEN
        params = dict(order__date="2032-01-01")
        with self.assertNumQueries(2):
            results = self.make_request(
                "GET", self.get_list_url() + "stats/", query_params=params
            ).json()

        # THEN
        self.assertEqual(len(results), 3)
        self.assertDictContainsSubset(
            dict(
                id=None,
                name=op3.name,
                order_name="O2",
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                is_starred=True,
                totals=dict(
                    to_be_filled=0,
                    to_be_picked_up=48,
                    total=48,
                    quantity_types={
                        str(QUANTITY_DOZEN): dict(
                            to_be_filled=0, to_be_picked_up=48, total=48,
                        ),
                    },
                ),
            ),
            results[0],
        )
        self.assertDictContainsSubset(
            dict(
                id=None,
                name=op2.name,
                order_name="O2",
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                is_starred=True,
                totals=dict(
                    to_be_filled=0,
                    to_be_picked_up=3,
                    total=3,
                    quantity_types={
                        str(QUANTITY_INDIVIDUAL): dict(
                            to_be_filled=0, to_be_picked_up=3, total=3,
                        ),
                    },
                ),
            ),
            results[1],
        )
        self.assertDictContainsSubset(
            dict(
                id=None,
                name=op1.name,
                order_name=order1.name,
                product_type=OP_TYPE_SPECIAL_PRODUCT,
                totals=dict(
                    to_be_filled=12,
                    to_be_picked_up=0,
                    total=12,
                    quantity_types={
                        str(QUANTITY_HALF_DOZEN): dict(
                            to_be_filled=12, to_be_picked_up=0, total=12,
                        ),
                    },
                ),
            ),
            results[2],
        )


class DashboardOrderFlagViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/orders/flags/"

    def setUp(self):
        super().setUp()
        self.user = self.login_basic_user()

    def test_retrieve__basic_order_flag__data_format(self):
        of = OrderFlagFactory(name="Order Flag")

        data = self.make_request("GET", self.get_obj_url(of.id)).json()

        self.assertDictContainsSubset(dict(id=of.id, name="Order Flag",), data)


class DashOrderLocationViewSetTestCase(BaseAPITestCase):
    base_url = "/api/dashboard/orders/locations/"

    def setUp(self):
        super().setUp()
        self.user = self.login_basic_user()

    def test_retrieve__basic_order_location__data_format(self):
        ol = OrderLocationFactory(description="Order Location", hotkey="zz")

        data = self.make_request("GET", self.get_obj_url(ol.id)).json()

        self.assertDictContainsSubset(
            dict(id=ol.id, description="Order Location", hotkey="zz",), data
        )

    def test_move__basic_orders__is_successful(self):
        src_location = OrderLocationFactory()
        src_order_1 = OrderFactory(location=src_location, date=date(2019, 1, 1))
        src_order_2 = OrderFactory(location=src_location, date=date(2019, 1, 2))
        dest_location = OrderLocationFactory()
        dest_order = OrderFactory(location=dest_location, date=date(2019, 1, 1))
        other_location = OrderLocationFactory()
        other_location_order = OrderFactory(
            location=other_location, date=date(2019, 1, 1)
        )

        self.make_request(
            "POST",
            self.get_list_url() + "move/",
            data=dict(
                date=date(2019, 1, 1),
                src_location=src_location.id,
                dest_location=dest_location.id,
            ),
        ).json()

        self.assertEqual(src_order_1.reload().location, dest_location)
        self.assertEqual(src_order_2.reload().location, src_location)
        self.assertEqual(dest_order.reload().location, dest_location)
        self.assertEqual(other_location_order.reload().location, other_location)

    def test_move__invalid_data__returns_errors(self):
        src_location = OrderLocationFactory()
        error = self.make_request(
            "POST",
            self.get_list_url() + "move/",
            data=dict(
                date=date(2019, 1, 1), src_location=src_location.id, dest_location=9999,
            ),
            permitted_codes=[400],
        ).json()

        # THEN
        self.assertEqual(
            error["detail"], 'Dest Location: Invalid pk "9999" - object does not exist.'
        )


@ddt
class PublicOrderViewSetTestCase(BaseAPITestCase):
    base_url = "/api/public/orders/"

    def setUp(self):
        self.d = datetime.now().date() + timedelta(days=3)
        if self.d.weekday() == 6:
            self.d += timedelta(days=1)
        self.product = FrontProductFactory(name="Front Product", is_starred=True)
        self.fpp = FrontProductPriceFactory(
            front_product=self.product,
            quantity_type=QUANTITY_HALF_DOZEN,
            price=Decimal("1.23"),
        )
        ScheduleSettings.objects.all().delete()

    def create_test(self, data: dict, message: str):
        # WHEN
        response = self.make_request(
            "POST", self.get_list_url(), data=data, permitted_codes=[400],
        ).json()

        # THEN
        self.assert_response_message_equals(response, message)

    def test_validate__basic_purchase__is_accepted(self):
        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url("validate/"),
            data=dict(
                date=self.d,
                time="13:00:00",
                name="Test Order",
                phone_number="4039999999",
                email="test@example.com",
                order_products=[dict(front_product_price=self.fpp.id, quantity=3)],
            ),
        ).json()

        # THEN
        self.assertEqual(Order.objects.count(), 0)
        self.assertEqual(OrderProduct.objects.count(), 0)

        # AND
        self.assertEqual(
            data,
            dict(
                date=self.d.strftime("%Y-%m-%d"),
                time="13:00:00",
                name="Test Order",
                email="test@example.com",
                phone_number="4039999999",
                calculated_price="3.69",
                order_products=[
                    dict(
                        front_product_price=self.fpp.id,
                        quantity=3,
                        calculated_price="1.23",
                        description="3 half doz. Front Product",
                        total_calculated_price="3.69",
                    ),
                ],
            ),
        )

    @mock.patch("orders.models.send_templated_message")
    def test_create__basic_purchase__is_accepted(self, m):
        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=dict(
                date=self.d,
                time="13:00:00",
                name="Test Order",
                phone_number="4039999999",
                email="test@example.com",
                stripe_token="tok_visa",
                order_products=[dict(front_product_price=self.fpp.id, quantity=3,)],
                stripe_token_id="tok_mastercard",
            ),
        ).json()

        # THEN
        order = Order.objects.get()
        order_product = OrderProduct.objects.get()

        # AND
        self.assertEqual(
            data,
            dict(
                id=order.id,
                date=self.d.strftime("%Y-%m-%d"),
                time="13:00:00",
                name="Test Order",
                email="test@example.com",
                phone_number="4039999999",
                calculated_price="3.69",
                order_products=[
                    dict(
                        id=order_product.id,
                        front_product_price=self.fpp.id,
                        quantity=3,
                        calculated_price="1.23",
                        description="3 half doz. Front Product",
                        total_calculated_price="3.69",
                    ),
                ],
            ),
        )

        self.assertIsNotNone(order.stripe_charge_id)
        stripe_charge = order.get_stripe_charge()
        self.assertEqual(stripe_charge.amount, 369)
        self.assertEqual(stripe_charge.currency, "cad")
        self.assertEqual(stripe_charge.receipt_email, "test@example.com")
        self.assertEqual(
            dict(stripe_charge.metadata),
            dict(
                order_id=str(order.id),
                order_reference_id=order.reference_id,
                order_date=order.user_friendly_date_str,
                order_time=order.user_friendly_time_str,
                order_edit_url=order.edit_url,
            ),
        )

        self.assertEqual(order.purchase_status, "paid")
        self.assertEqual(order.stripe_charge_status, "paid")

        self.assertEqual(m.call_count, 1)
        m.assert_called_once_with(
            ORDER_CONFIRMATION_DASHBOARD_SENDGRID_ID,
            {
                "order": {
                    "id": order.id,
                    "date": self.d.strftime("%A, %b %-d, %Y"),
                    "time": "1PM",
                    "name": "Test Order",
                    "price": "3.69",
                    "order_products": [
                        {"description": "3 half doz. Front Product", "price": "1.23",}
                    ],
                }
            },
            reply_to=["glamorganbakery@gmail.com"],
            to=["test@example.com", "glamorganbakery@gmail.com"],
        )

    def test_create__invalid_token__is_rejected(self):
        # WHEN
        response = self.make_request(
            "POST",
            self.get_list_url(),
            data=PublicOrderRequestFactory(
                date=self.d,
                order_products=[dict(front_product_price=self.fpp.id, quantity=3,)],
                stripe_token_id="invalid_token",
            ),
            permitted_codes=[400],
        ).json()

        # THEN
        self.assert_response_message_equals(
            response,
            "Unexpected error while communicating with Stripe: \"No such token: 'invalid_token'\"",
        )

    def test_create__refund_invoice__is_accepted(self):
        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=PublicOrderRequestFactory(
                date=self.d,
                order_products=[dict(front_product_price=self.fpp.id, quantity=3,)],
                stripe_token_id="tok_visa",
            ),
        ).json()

        # THEN
        order = Order.objects.get()

        # AND
        self.user = self.login_basic_user()
        self.make_request(
            "PUT", f"/api/dashboard/orders/{order.id}/refund-invoice/",
        ).json()

        # THEN
        order = order.reload()
        self.assertEqual(order.purchase_status, "unpaid")
        self.assertEqual(order.stripe_charge_id, None)

    def test_create__refund_disputed__is_accepted(self):
        # WHEN
        data = self.make_request(
            "POST",
            self.get_list_url(),
            data=PublicOrderRequestFactory(
                date=self.d,
                order_products=[dict(front_product_price=self.fpp.id, quantity=3,)],
                stripe_token_id="tok_createDispute",
            ),
        ).json()

        # THEN
        order = Order.objects.get()

        # AND
        self.user = self.login_basic_user()
        response = self.make_request(
            "PUT",
            f"/api/dashboard/orders/{order.id}/refund-invoice/",
            permitted_codes=[400],
        ).json()

        # THEN
        self.assertIn(
            "Unexpected error while communicating with Stripe:", response["detail"]
        )
        self.assertIn(
            "has been charged back; cannot issue a refund.", response["detail"]
        )

    def test_create__no_name__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(name=None), "Name: This field may not be null."
        )

    def test_create__blank_name__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(name=""), "Name: This field may not be blank."
        )

    def test_create__no_phone_number__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(phone_number=None),
            "Phone Number: This field may not be null.",
        )

    def test_create__blank_phone_number__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(phone_number=""),
            "Phone Number: This field may not be blank.",
        )

    def test_create__no_email__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(email=None), "Email: This field may not be null."
        )

    def test_create__blank_email__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(email=""), "Email: This field may not be blank."
        )

    def test_create__no_date__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(date=None), "Date: This field may not be null."
        )

    def test_create__no_time__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(time=None), "Time: This field may not be null."
        )

    def test_create__null_fpp__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(order_products=[dict(quantity=1)]),
            "Front Product Price: This field is required.",
        )

    def test_create__null_quantity__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(
                order_products=[dict(front_product_price=self.fpp.id)]
            ),
            "Quantity: This field is required.",
        )

    def test_create__above_5pm__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(time="18:00"), "Time must be 5:00PM or earlier",
        )

    def test_create__before_9am__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(time="8:00",), "Time must be 9:00AM or later",
        )

    @freeze_time(local_datetime(2020, 1, 2))
    def test_create__two_days_notice__is_accepted(self):
        self.make_request(
            "POST",
            self.get_list_url(),
            data=PublicOrderRequestFactory(date="2020-01-03",),
            permitted_codes=[400],
        ).json()

    def test_create__private_product_price__is_rejected(self):
        fpp = FrontProductPriceFactory(front_product=self.product, is_public=False)

        self.create_test(
            PublicOrderRequestFactory(
                order_products=[dict(quantity=1, front_product_price=fpp.id)]
            ),
            "Product is not available for purchase",
        )

    def test_create__invalid_product_price__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(
                order_products=[dict(quantity=1, front_product_price=999)]
            ),
            'Front Product Price: Invalid pk "999" - object does not exist.',
        )

    def test_permissions__all__are_enforced(self):
        assert_statuses_are_met(
            [
                ["GET", self.get_list_url(), 405],
                ["GET", self.get_obj_url(9999), 404],
                ["POST", self.get_list_url(), 400],
                ["PUT", self.get_obj_url(9999), 404],
                ["DELETE", self.get_obj_url(9999), 404],
            ],
            self,
        )

    @freeze_time(local_datetime(2020, 12, 10))
    def test_create__sunday_order__is_rejected(self):
        self.create_test(
            PublicOrderRequestFactory(
                date=local_datetime(2020, 12, 13, 12).strftime("%Y-%m-%d"),  # Sunday
            ),
            "Cannot place an order on Sunday",
        )

    @freeze_time(local_datetime(2020, 12, 10))
    def test_create__closing_hour__is_rejected(self):
        ClosingRule.objects.create(
            start=date(2021, 12, 25), end=date(2021, 12, 27), reason="special holiday"
        )
        self.create_test(
            PublicOrderRequestFactory(
                date=local_datetime(2021, 12, 25, 9).strftime("%Y-%m-%d"),
            ),
            "Cannot place an order for this day due to special holiday",
        )
        self.create_test(
            PublicOrderRequestFactory(
                date=local_datetime(2021, 12, 27, 16).strftime("%Y-%m-%d"),
            ),
            "Cannot place an order for this day due to special holiday",
        )

    @data(
        (
            local_datetime(2020, 12, 14, 15),
            local_datetime(2020, 12, 15, 9),
            local_datetime(2020, 12, 16, 9),
            "Wednesday",
        ),  # Monday morning -> Wednesday
        (
            local_datetime(2020, 12, 14, 16),
            local_datetime(2020, 12, 16, 9),
            local_datetime(2020, 12, 17, 9),
            "Thursday",
        ),  # Monday evening -> Thursday
        (
            local_datetime(2020, 12, 15, 15),
            local_datetime(2020, 12, 16, 9),
            local_datetime(2020, 12, 17, 9),
            "Thursday",
        ),  # Tuesday morning -> Thursday
        (
            local_datetime(2020, 12, 15, 16),
            local_datetime(2020, 12, 17, 9),
            local_datetime(2020, 12, 18, 9),
            "Friday",
        ),  # Tuesday evening -> Friday
        (
            local_datetime(2020, 12, 16, 15),
            local_datetime(2020, 12, 17, 9),
            local_datetime(2020, 12, 18, 9),
            "Friday",
        ),  # Wednesday morning -> Friday
        (
            local_datetime(2020, 12, 16, 16),
            local_datetime(2020, 12, 18, 9),
            local_datetime(2020, 12, 19, 9),
            "Saturday",
        ),  # Wednesday evening -> Saturday
        (
            local_datetime(2020, 12, 17, 15),
            local_datetime(2020, 12, 18, 9),
            local_datetime(2020, 12, 19, 9),
            "Saturday",
        ),  # Thursday morning -> Saturday
        (
            local_datetime(2020, 12, 17, 16),
            local_datetime(2020, 12, 19, 9),
            local_datetime(2020, 12, 21, 9),
            "Monday",
        ),  # Thursday evening -> Monday
        (
            local_datetime(2020, 12, 18, 15),
            local_datetime(2020, 12, 20, 9),
            local_datetime(2020, 12, 21, 9),
            "Monday",
        ),  # Friday morning -> Monday
        (
            local_datetime(2020, 12, 18, 16),
            local_datetime(2020, 12, 21, 9),
            local_datetime(2020, 12, 22, 9),
            "Tuesday",
        ),  # Friday evening -> Tuesday
        (
            local_datetime(2020, 12, 19, 15),
            local_datetime(2020, 12, 21, 9),
            local_datetime(2020, 12, 22, 9),
            "Tuesday",
        ),  # Saturday morning -> Tuesday
        (
            local_datetime(2020, 12, 19, 16),
            local_datetime(2020, 12, 22, 9),
            local_datetime(2020, 12, 23, 9),
            "Wednesday",
        ),  # Saturday -> Wednesday
        (
            local_datetime(2020, 12, 20, 15),
            local_datetime(2020, 12, 22, 9),
            local_datetime(2020, 12, 23, 9),
            "Wednesday",
        ),  # Sunday morning -> Wednesday
        (
            local_datetime(2020, 12, 20, 16),
            local_datetime(2020, 12, 22, 9),
            local_datetime(2020, 12, 23, 9),
            "Wednesday",
        ),  # Sunday evening -> Wednesday
    )
    @unpack
    def test_create__two_days_notice__is_enforced(
        self, current_dt, fail_order_dt, success_order_dt, earliest_date_str
    ):
        with freeze_time(current_dt):
            response = self.make_request(
                "POST",
                self.get_list_url("validate/"),
                data=PublicOrderRequestFactory(
                    date=fail_order_dt.strftime("%Y-%m-%d"),
                    time=fail_order_dt.strftime("%H:%M"),
                ),
                permitted_codes=[400],
            ).json()

            self.assert_response_message_equals(
                response,
                f"Two business days notice are required for online orders (cut-off at 4:01PM). "
                f"The earliest available date is {earliest_date_str}.",
            )

            self.make_request(
                "POST",
                self.get_list_url("validate/"),
                data=PublicOrderRequestFactory(
                    date=success_order_dt.strftime("%Y-%m-%d"),
                    time=success_order_dt.strftime("%H:%M"),
                ),
            ).json()

    def test_custom_schedule_settings_are_enforced(self):
        settings, _ = ScheduleSettings.objects.get_or_create()
        settings.num_business_days = 1
        settings.order_cutoff_time = time(11)
        settings.save()

        current_dt = local_datetime(2020, 12, 14, 12)
        fail_order_dt = local_datetime(2020, 12, 15, 10)
        success_order_dt = local_datetime(2020, 12, 16, 9)
        earliest_date_str = "Wednesday"

        with freeze_time(current_dt):
            response = self.make_request(
                "POST",
                self.get_list_url("validate/"),
                data=PublicOrderRequestFactory(
                    date=fail_order_dt.strftime("%Y-%m-%d"),
                    time=fail_order_dt.strftime("%H:%M"),
                ),
                permitted_codes=[400],
            ).json()

            self.assert_response_message_equals(
                response,
                f"Two business days notice are required for online orders (cut-off at 11:00AM). "
                f"The earliest available date is {earliest_date_str}.",
            )

            self.make_request(
                "POST",
                self.get_list_url("validate/"),
                data=PublicOrderRequestFactory(
                    date=success_order_dt.strftime("%Y-%m-%d"),
                    time=success_order_dt.strftime("%H:%M"),
                ),
            ).json()

    def test_create__custom_earliest_time__is_enforced(self):
        settings, _ = ScheduleSettings.objects.get_or_create()
        settings.order_earliest_time = time(11)
        settings.save()

        self.create_test(
            PublicOrderRequestFactory(time="10:00",), "Time must be 11:00AM or later",
        )

    def test_create__custom_latest_time__is_enforced(self):
        settings, _ = ScheduleSettings.objects.get_or_create()
        settings.order_latest_time = time(15)
        settings.save()

        self.create_test(
            PublicOrderRequestFactory(time="16:00",), "Time must be 3:00PM or earlier",
        )
