from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response

from main.serializers.pagination import OptionalPageNumberPagination
from main.serializers.viewsets import AppModelViewSet, AppGenericViewSet
from orders.api.dashboard.filters import (
    DashboardOrderFilterSet,
    DashOrderProductFilterSet,
)
from orders.api.dashboard.serializers import (
    DashboardOrderSerializer,
    DashboardOrderFlagSerializer,
    DashboardOrderLocationSerializer,
    DashboardMoveOrderLocationsSerializer,
    DashboardSendInvoiceSerializer,
)
from orders.models import Order, OrderFlag, OrderLocation, OrderProduct


class DashboardOrderViewSet(AppModelViewSet):
    model = Order
    queryset = model.objects.all().prefetch_related(
        "order_products", "order_products__front_product"
    )
    serializer_class = DashboardOrderSerializer
    filter_class = DashboardOrderFilterSet
    search_fields = (
        "name",
        "phone_number",
        "email",
        "reference_id",
        "stripe_invoice_id",
    )
    pagination_class = OptionalPageNumberPagination
    send_invoice_serializer_class = DashboardSendInvoiceSerializer

    def get_queryset(self):
        qs = super().get_queryset()

        # Filter by order reference ID if query appears to be one
        query = self.request.query_params.get("q")
        if Order.is_order_reference_id(query):
            return qs.filter(reference_id=query)

        return qs

    @action(detail=True, methods=["PUT"])
    def cancel(self, *args, **kwargs):
        """
        Cancels the given order
        """
        # Cancel order
        order = self.get_object()
        order.cancel()

        self.show_details = True  # Send detailed info
        serializer = self.get_serializer(instance=order)
        return Response(serializer.data)

    @action(detail=True, methods=["PUT"], url_path="send-invoice")
    def send_invoice(self, request, *args, **kwargs):
        order = self.get_object()

        serializer = self.send_invoice_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        order.validate_create_stripe_invoice()
        order.create_stripe_invoice()

        if serializer.data["mark_invoice_as_paid"]:
            order.mark_invoice_as_paid()

        order.save()

        serializer = self.get_serializer(instance=order)
        return Response(serializer.data)

    @action(detail=True, methods=["PUT"], url_path="refund-invoice")
    def refund_invoice(self, *args, **kwargs):
        """
        Refunds the existing invoice. If the invoice cannot be refunded,
        an error is returned.

        This method is a pre-requisite for cancelling any invoiced order.
        """
        order = self.get_object()

        order.refund_stripe_charge()
        order.save()

        serializer = self.get_serializer(instance=order)
        return Response(serializer.data)

    @action(detail=True, methods=["PUT"], url_path="void-invoice")
    def void_invoice(self, *args, **kwargs):
        """
        Void the existing invoice. If the order must be refunded, or is
        in an invalid state, an error is returned.

        This method is a pre-requisite for cancelling any invoiced order.
        """
        order = self.get_object()

        order.void_stripe_invoice()
        order.save()

        serializer = self.get_serializer(instance=order)
        return Response(serializer.data)

    @action(detail=True, methods=["PUT"], url_path="mark-as-paid")
    def mark_as_paid(self, *args, **kwargs):
        """
        Marks the order as paid
        """
        order = self.get_object()
        order.mark_as_paid()
        order.save()

        serializer = self.get_serializer(instance=order)
        return Response(serializer.data)

    @action(detail=True, methods=["PUT"], url_path="unmark-as-paid")
    def unmark_as_paid(self, *args, **kwargs):
        """
        Reverses an order to be unpaid
        """
        order = self.get_object()
        order.unmark_as_paid()
        order.save()

        serializer = self.get_serializer(instance=order)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        # Deleting not permitted - use `cancel` instead
        raise MethodNotAllowed("DELETE")


class DashboardOrderProductViewSet(AppGenericViewSet):
    model = OrderProduct
    queryset = model.objects.all()
    stats_filter_set_class = DashOrderProductFilterSet

    @action(detail=False, methods=["GET"])
    def stats(self, request, *args, **kwargs):
        qs = self.stats_filter_set_class(
            request.query_params, queryset=self.get_queryset(), request=request
        ).qs

        totals = qs.calculate_totals()
        return Response(totals)


class DashboardOrderFlagViewSet(AppModelViewSet):
    model = OrderFlag
    queryset = model.objects.all()
    serializer_class = DashboardOrderFlagSerializer
    pagination_class = OptionalPageNumberPagination


class DashboardOrderLocationViewSet(AppModelViewSet):
    model = OrderLocation
    queryset = model.objects.all()
    serializer_class = DashboardOrderLocationSerializer
    pagination_class = OptionalPageNumberPagination
    move_serializer_class = DashboardMoveOrderLocationsSerializer

    @action(detail=False, methods=["POST"])
    def move(self, request, *args, **kwargs):
        serializer = self.move_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
