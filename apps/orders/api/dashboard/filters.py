from main.serializers.filters import AppFilterSet
from orders.models import Order, OrderProduct


class DashboardOrderFilterSet(AppFilterSet):
    class Meta:
        model = Order
        fields = {
            "date": ["exact"],
            "picked_up": ["exact"],
            "verified": ["exact"],
            "filled": ["exact"],
            "time": ["exact", "isnull"],
            "order_flags": ["exact"],
            "cancelled": ["exact"],
        }


class DashOrderProductFilterSet(AppFilterSet):
    required_fields = ["order__date"]

    class Meta:
        model = OrderProduct
        fields = {
            "order__date": ["exact"],
            "product_type": ["exact"],
        }
