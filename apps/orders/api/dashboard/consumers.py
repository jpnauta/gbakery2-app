from main.serializers.consumers import DashboardConsumer

from orders.api.dashboard.serializers import DashboardOrderSerializer
from orders.models import Order


class DashboardOrderConsumer(DashboardConsumer):
    model = Order
    group_name = "dashboard-orders"
    serializer_class = DashboardOrderSerializer
