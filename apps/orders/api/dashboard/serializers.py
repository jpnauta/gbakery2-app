from django.db import transaction
from rest_framework.fields import DateField, BooleanField
from rest_framework.relations import PrimaryKeyRelatedField

from main.serializers import (
    AppModelSerializer,
    NestedAppModelSerializer,
    M2MRelatedModelSerializer,
)
from main.serializers.fields import AppListField, CurrencySerializerField
from main.serializers.mixins import AppSerializer
from orders.models import (
    Order,
    OrderFlag,
    OrderLocation,
    OrderProduct,
    OrderFlagOrder,
    OP_TYPE_FRONT_PRODUCT,
    OP_TYPE_SPECIAL_PRODUCT,
)
from products.models import FrontProductPrice


class DashboardOrderSerializer(M2MRelatedModelSerializer):
    class OrderProductSerializer(NestedAppModelSerializer):
        class Meta:
            model = OrderProduct
            fields = (
                "id",
                "front_product",
                "quantity",
                "quantity_type",
                "filled",
                "product_type",
                "name",
                "description",
                "is_starred",
                "custom_price",
                "calculated_price",
                "price",
                "front_product_price",
            )

        price = CurrencySerializerField(read_only=True)

        def validate(self, attrs):
            attrs = super().validate(attrs)

            fpp = None
            if attrs["product_type"] == OP_TYPE_FRONT_PRODUCT:
                # Required fields for front products
                self.force_require_fields(attrs, ["front_product"])
                fpp = FrontProductPrice.objects.get_or_none(
                    front_product_id=attrs["front_product"],
                    quantity_type=attrs["quantity_type"],
                )
            elif attrs["product_type"] == OP_TYPE_SPECIAL_PRODUCT:
                # Required fields for special products
                self.force_require_fields(attrs, ["name"])

            attrs["front_product_price"] = fpp if fpp is not None else None
            attrs["calculated_price"] = fpp.price if fpp is not None else None

            return attrs

    order_products = OrderProductSerializer(many=True)
    order_flags = AppListField(
        child=PrimaryKeyRelatedField(queryset=OrderFlag.objects.all()),
        allow_duplicates=False,
        default=[],
    )
    price = CurrencySerializerField(read_only=True)

    def validate(self, attrs):
        attrs = super().validate(attrs)

        if self.instance:
            # Ensure cancelled cannot be manipulated
            if self.instance.cancelled:
                product_count = len(attrs["order_products"])
                self.instance.clean_order_cancelled(product_count)

            if attrs.get("marked_as_paid"):
                self.instance.validate_marked_as_paid()

        Order.calculate_order_price(attrs)

        return attrs

    def create(self, validated_data):
        with transaction.atomic():
            relations_to_create = self.prepare_for_m2m_create(
                OrderFlagOrder, validated_data.pop("order_flags"), "flag",
            )

            order_products = validated_data.pop("order_products")
            order = super().create(validated_data)

            self.fields["order_products"].child.create_m2m_related(
                order_products, "order", order
            )

            self.bulk_create_m2m_relations(
                order, "order", OrderFlagOrder, relations_to_create
            )

        return order

    def update(self, order, validated_data):
        from orders.api.dashboard.consumers import DashboardOrderConsumer

        with transaction.atomic():
            (
                relations_to_create,
                relations_to_update,
                relations_to_delete,
            ) = self.prepare_for_m2m_update(
                self.instance,
                OrderFlagOrder,
                order.order_flag_orders.all(),
                validated_data.pop("order_flags"),
                "flag",
            )

            order_products = validated_data.pop("order_products")
            order = super().update(order, validated_data)

            self.fields["order_products"].child.update_m2m_related(
                order_products, "order", order, order.order_products.all(),
            )

            self.bulk_delete_m2m_relations(OrderFlagOrder, relations_to_delete)
            self.bulk_create_m2m_relations(
                order, "order", OrderFlagOrder, relations_to_create
            )

        # Send a second update message now that order products are updated
        DashboardOrderConsumer.post_save_receiver(order, False)

        return order

    class Meta:
        model = Order
        fields = (
            "id",
            "name",
            "phone_number",
            "email",
            "reference_id",
            "date",
            "time",
            "comment",
            "picked_up",
            "filled",
            "in_progress",
            "verified",
            "location",
            "cannot_be_filled",
            "comment",
            "verified",
            "cancelled",
            "order_products",
            "order_flags",
            "num_boxes",
            "custom_price",
            "calculated_price",
            "price",
            "stripe_customer_id",
            "stripe_invoice_id",
            "stripe_charge_id",
            "stripe_invoice_link",
            "stripe_charge_link",
            "purchase_status",
            "purchase_status_text",
            "marked_as_paid",
            "created",
            "updated",
        )


class DashboardOrderFlagSerializer(AppModelSerializer):
    class Meta:
        model = OrderFlag
        fields = (
            "id",
            "name",
        )


class DashboardOrderLocationSerializer(AppModelSerializer):
    class Meta:
        model = OrderLocation
        fields = (
            "id",
            "description",
            "hotkey",
        )


class DashboardMoveOrderLocationsSerializer(AppSerializer):
    date = DateField()
    src_location = PrimaryKeyRelatedField(queryset=OrderLocation.objects.all())
    dest_location = PrimaryKeyRelatedField(queryset=OrderLocation.objects.all())

    class Meta:
        fields = (
            "date",
            "src_location",
            "dest_location",
        )

    def save(self):
        date = self.validated_data["date"]
        src_orders = self.validated_data["src_location"].orders.filter(date=date)
        dest_location = self.validated_data["dest_location"]
        with transaction.atomic():
            for order in src_orders:
                order.location = dest_location
                order.save()


class DashboardSendInvoiceSerializer(AppSerializer):
    mark_invoice_as_paid = BooleanField(default=False)
