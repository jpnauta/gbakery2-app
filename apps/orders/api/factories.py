from datetime import timedelta

import factory
from factory import DictFactory

from main.ext.dates.utils import local_tz_now


class DashboardOrderRequestFactory(DictFactory):
    email = "testuser@glamorganbakery.com"
    calculated_price = "12.20"
    date = factory.LazyAttribute(
        lambda x: (local_tz_now() + timedelta(days=1)).strftime("%Y-%m-%d")
    )
    name = "My Order"
    order_products = []


class PublicOrderRequestFactory(DictFactory):
    email = "testuser@glamorganbakery.com"
    phone_number = "4039199999"
    date = factory.LazyAttribute(
        lambda x: (local_tz_now() + timedelta(days=1)).strftime("%Y-%m-%d")
    )
    time = "14:00"
    name = "My Order"
    order_products = []
    stripe_token_id = "fake_token"  # Must be overridden for valid purchase
