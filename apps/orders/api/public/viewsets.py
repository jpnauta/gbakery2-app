from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response

from main.serializers.viewsets import AppPublicViewSetMixin, AppGenericViewSet
from orders.api.public.serializers import (
    PublicOrderSerializer,
    PublicValidateOrderSerializer,
)
from orders.models import Order


class PublicOrderViewSet(AppPublicViewSetMixin, CreateModelMixin, AppGenericViewSet):
    model = Order
    queryset = model.objects.all().prefetch_related("order_products")
    serializer_class = PublicOrderSerializer
    validate_serializer_class = PublicValidateOrderSerializer

    @action(detail=False, methods=["POST"], url_path="validate")
    def validate(self, request, *args, **kwargs):
        """
        Validates the order data without actually creating it. Most useful for
        generating a stripe intent.
        """
        serializer = self.validate_serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data)

    def perform_create(self, serializer):
        super().perform_create(serializer)

        # Send email to customer
        serializer.instance.send_order_confirmation_email()
