from datetime import timedelta

import stripe
from django.db import transaction
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField

from main.ext.dates.utils import local_tz_now
from main.serializers import M2MRelatedModelSerializer, NestedAppModelSerializer
from main.serializers.fields import CurrencySerializerField
from orders.models import (
    OrderProduct,
    Order,
    OP_TYPE_FRONT_PRODUCT,
    PAYMENT_STATUS_PAID,
)
from products.models import QUANTITY_TYPE_OPTIONS
from schedule.models import ScheduleSettings
from schedule.utils import get_closing_rule_for


class BasePublicOrderSerializer(M2MRelatedModelSerializer):
    """
    Serializer class to both validate the order before credit card info,
    and confirm order once the payment intent is confirmed
    """

    class OrderProductSerializer(NestedAppModelSerializer):
        class Meta:
            model = OrderProduct
            fields = (
                "id",
                "front_product_price",
                "quantity",
                "calculated_price",
                "description",
                "total_calculated_price",
            )

        calculated_price = CurrencySerializerField(read_only=True)
        total_calculated_price = CurrencySerializerField(read_only=True)

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields["front_product_price"].required = True

        def validate(self, attrs):
            attrs = super().validate(attrs)

            if not attrs["front_product_price"].is_public:
                raise ValidationError(f"Product is not available for purchase")

            fpp = attrs["front_product_price"]
            attrs["product_type"] = OP_TYPE_FRONT_PRODUCT
            attrs["front_product"] = fpp.front_product
            attrs["quantity_type"] = fpp.quantity_type

            attrs["calculated_price"] = fpp.price if fpp is not None else None
            attrs["total_calculated_price"] = (
                attrs["quantity"] * attrs["calculated_price"]
            )

            attrs[
                "description"
            ] = f'{attrs["quantity"]} {QUANTITY_TYPE_OPTIONS[attrs["quantity_type"]]["short_name_2"]} {attrs["front_product"].name}'

            return attrs

        def create(self, validated_data):
            validated_data.pop("total_calculated_price")
            return super().create(validated_data)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["phone_number"].required = True
        self.fields["phone_number"].allow_null = False
        self.fields["phone_number"].allow_blank = False
        self.fields["email"].required = True
        self.fields["email"].allow_null = False
        self.fields["email"].allow_blank = False
        self.fields["time"].required = True
        self.fields["time"].allow_null = False

    order_products = OrderProductSerializer(many=True)
    calculated_price = CurrencySerializerField(read_only=True)

    def validate(self, attrs):
        attrs = super().validate(attrs)

        settings, _ = ScheduleSettings.objects.get_or_create()

        order_latest_time = settings.order_latest_time
        if attrs["time"] > order_latest_time:
            raise ValidationError(f"Time must be {order_latest_time.strftime('%-I:%M%p')} or earlier")

        order_earliest_time = settings.order_earliest_time
        if attrs["time"] < order_earliest_time:
            raise ValidationError(f"Time must be {order_earliest_time.strftime('%-I:%M%p')} or later")

        # Enforce minimum business days
        now = local_tz_now()
        weekday = now.weekday()
        days_later = now.date() + timedelta(days=settings.num_business_days)
        order_cutoff_time = settings.order_cutoff_time
        if now.time() >= order_cutoff_time and weekday != 6:  # 4PM
            days_later += timedelta(days=1)
        if weekday >= 4:  # Friday, Saturday or Sunday
            days_later += timedelta(days=1)
        if days_later.weekday() == 6:
            days_later += timedelta(days=1)
        if attrs["date"] < days_later:
            raise ValidationError(
                f"Two business days notice are required for online orders (cut-off at "
                f"{order_cutoff_time.strftime('%-I:%M%p')}). The earliest available date "
                f"is {days_later.strftime('%A')}."
            )

        if attrs["date"].weekday() == 6:
            raise ValidationError("Cannot place an order on Sunday")

        closing_rule = get_closing_rule_for(attrs["date"])
        if closing_rule is not None:
            raise ValidationError(
                f"Cannot place an order for this day due to {closing_rule.reason}"
            )

        Order.calculate_order_price(attrs)

        return attrs


class PublicValidateOrderSerializer(BasePublicOrderSerializer):
    class Meta:
        model = Order
        fields = (
            "id",
            "name",
            "phone_number",
            "email",
            "date",
            "time",
            "order_products",
            "calculated_price",
        )


class PublicOrderSerializer(BasePublicOrderSerializer):
    stripe_token_id = CharField(required=True, allow_blank=False, write_only=True)

    class Meta:
        model = Order
        fields = (
            "id",
            "name",
            "phone_number",
            "email",
            "date",
            "time",
            "order_products",
            "calculated_price",
            "stripe_token_id",
        )

    def create(self, validated_data):
        with transaction.atomic():
            stripe_token_id = validated_data.pop("stripe_token_id")
            order_products = validated_data.pop("order_products")
            order = super().create(validated_data)

            self.fields["order_products"].child.create_m2m_related(
                order_products, "order", order
            )

            # Charge customer's credit card
            charge = stripe.Charge.create(
                amount=order.price_cents,
                currency="cad",
                description=f"Order {order.id} for {order.name}",
                source=stripe_token_id,
                receipt_email=order.email,
                metadata=order.stripe_metadata,
            )
            order.stripe_charge_id = charge.stripe_id
            order.stripe_charge_status = PAYMENT_STATUS_PAID
            order.save()

        return order
