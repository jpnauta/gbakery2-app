from __future__ import unicode_literals

from channels.testing import WebsocketCommunicator

from main.serializers.consumers import UPDATE
from main.testing.decorators import async_test
from main.tests.test_cases import BaseChannelTestCase, BaseAPITestCase
from orders.api.dashboard.consumers import DashboardOrderConsumer
from orders.factories import OrderFactory
from users.factories import UserModelFactory
from users.models import User


class VenueBasedReservationBindingTestCase(BaseChannelTestCase, BaseAPITestCase):
    user: User = None

    def setUp(self):
        super().setUp()
        self.user = self.login_basic_user()

    def get_communicator(self):
        return WebsocketCommunicator(DashboardOrderConsumer, "/dashboard/orders/")

    @async_test
    async def test_process_message__basic_order_saved__data_format(self):
        # GIVEN
        user = UserModelFactory()
        token = self.get_jwt_token(user)
        order = OrderFactory(name="Test name", phone_number="4039999999")

        # WHEN
        await self.connect()
        await self.subscribe(data=dict(token=token))
        order.name = "New name"
        order.save()
        await DashboardOrderConsumer.process_message(order, UPDATE)

        # THEN
        received = await self.receive_from()
        self.assertIn("data", received)
        self.assertEqual(received["action"], "update")
        self.assertEqual(received["data"]["name"], "New name")
        self.assertEqual(received["data"]["phone_number"], "4039999999")
        self.assertEqual(received["data"]["id"], order.id)

        await self.assert_all_received()

    @async_test
    async def test_subscribe__user_not_logged_in__not_authorized(self):
        # GIVEN
        await self.connect()

        # WHEN
        response = await self.subscribe(data=dict(), permitted_codes=[401])

        # THEN
        self.assertEqual(
            response["errors"]["detail"],
            "Authentication credentials were not provided.",
        )

        await self.assert_all_received()

    @async_test
    async def test_run_action__unknown_action__does_nothing(self):
        # GIVEN
        await self.connect()

        # WHEN
        await self.send_to(dict(action="unknown_action",))

        # THEN
        await self.assert_all_received()

    @async_test
    async def test_subscribe__basic_subscription__data_format(self):
        # GIVEN
        user = UserModelFactory()
        token = self.get_jwt_token(user)
        await self.connect()

        # WHEN
        response = await self.subscribe(data=dict(token=token))

        # THEN
        self.assertDictContainsSubset(
            dict(action="subscribe", response_status=200, errors=None, data=dict(),),
            response,
        )

        await self.assert_all_received()

    @async_test
    async def test_subscribe__basic_subscribe_with_invalid_token__is_rejected(self):
        # WHEN
        await self.connect()
        response = await self.subscribe(
            data=dict(token="invalidtoken"), permitted_codes=[401],
        )

        # THEN
        self.assertEqual(
            response["errors"]["detail"], "Incorrect authentication credentials.",
        )

        await self.assert_all_received()
