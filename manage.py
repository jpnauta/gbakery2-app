#!/usr/bin/env python
import os, sys

PROJECT_ROOT = os.path.dirname(__file__)

from django.core.management import execute_from_command_line

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

import settings

import os.path

SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))

if __name__ == "__main__":
    execute_from_command_line(sys.argv)
