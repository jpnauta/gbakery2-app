from django.urls import include, path

from django.contrib import admin

admin.autodiscover()

urlpatterns = (
    # Admin urls
    path("", include("main.admin.urls")),
    # API urls
    path("api/", include("main.api.urls")),
)
